<p align="center"><img src=Picture/22.jpg height="242" width="342"/></p>


# Unicorn
[![Version](https://img.shields.io/pypi/v/ipywidgets.svg)](https://pypi.python.org/pypi/Flask)
<!-- [![Build Status](xxx/app/rest/builds/buildType:(id:DevOps_UnicornMaster)/statusIcon)](xxx/viewType.html?buildTypeId=DevOps_UnicornMaster&tab=buildTypeStatusDiv) -->

## Server Provisioning: What and Who?
A server provisioning system takes a physical server from nothing to fully installed OS. Unicorn is a complete lifecycle management project for physical servers, was developed by the CloudAutomaion team. This tool provides power for every team to easily install physical servers.

## :unicorn: New features in V2:
1. Self-service CLI provides bulk parallel installations with a security check (Active Directory).
2. Self-service to manage a flavor of the server (style amazon).
3. K8S service waiting for API requests will call Rundeck API.

**Table of Contents:**

<!-- toc -->

- [Components Project](#components-project)
- [Server Site](#server-site)
    - [WorkFlow](#flow-chart)
    - [Kubernetes](#kubernetes-pods)
    - [Portworx](#Portworx)
    - [Installs Server Criterions](#installs-criterions)
        - [Stock Parameters](#choose-devices-in-stock-by-the-following-parameters)
        - [Errors](#errors)
        - [Multi Installing](#multi-installing)
        - [System Variables](#variables)
        - [Profiles](#profiles)
        - [Flavors](#falvors)
- [Client](#client)
    - [Usage](#usage)
    - [Examples](#examples)
    
- [DataCenter Hardware Usage And Cost Report](https://datastudio.google.com/u/0/reporting/1uSK-JOS48IwCO_sk6wFEhRP7oJG4XKm1/page/pgNw)
<!--- [Graffana Dashboard](https://grafana.int.liveperson.net/dashboard/db/bare-metal-installations?orgId=1&from=now-2d&to=now&var-DataCenter=All&var-Owner=All&var-User=All&var-Rack=All&var-interval=2d)
<!-- tocstop -->

#### Components Project
- Opendcim
- K8s
- Cobbler
- IPAM
- Ansible
- Python
- Rundeck
- Portworx
- Kibana
- BigQuery
- DataStudio

> OpenDCIM: Provide complete physical inventory of our data centers (hardware info and warranty).

> K8S: We created kubernets pod that provides extra features for OpenDCIM.

> Cobbler: Cobbler is a Linux provisioning server.

> IPAM: IP Address Management.

> Ansible:  Automation engine that automates butt provisioning, configuration management, application deployment, intra-service orchestration

> Python: Programing language - Python 3

> Rundeck: Rundeck is open source software that helps you automate routine operational procedures in data center or cloud environments. Rundeck provides a number of features that will alleviate time-consuming grunt work and make it easy for you to scale up your automation efforts and create self service for others.

> Portworx: A Persistent Volume (PV) is useful to know how Kubernetes manages storage resources. Kubernetes has a matching primitive for each of the traditional storage operational activities - provisioning (Storage Class)

> BigQuery: is a RESTful web service that enables interactive analysis of massive datasets working in conjunction with Google Storage. It is a serverless Platform as a Service that may be used complementarily with MapReduce
This cli tool provides all the features you need to install the server.

> DataStudio: ist eine Software der Google LLC zur Verwaltung und Visualisierung von Massendaten. Im Gegensatz zu Google Analytics können hier benutzerdefinierte und interaktive Berichte und Dashboards erstellt werden. Die Software richtet sich mit einer einfacheren Bedienung an unerfahrenere Anwender.

Old way:

- Find the server (To understand whether there is enough memory, how many disks there are, whether there is a warranty yet.)
- Service pack.
- Create a configuration file.


# Server Site
**A flask application receiving api requests.**

### Flow Chart
![flow](Picture/Unicornflow.png)
![flow](Picture/2019-09-15.png)

### Kubernetes pods
##### Prod VIP:
- Datacenter_1:
    - replicas: 3
    - VIP: VIP_1.liveperson.net.

##### Beckup VIP:
- Datacenter2:
    - replicas: 0
    - VIP: VIP_2.liveperson.net.

### Portworx - persistent storage
> /dev/pxd/pxd756717569813926837         976M         6.1M         903M         1%         /var/unicorn_states
**Direactory:**
   
     → Datacenter
          → state - Saved jobs details per user (Jobs that have expired after 24 hours are deleted)

           → request - Saved the request details by UUID.

           → tools - Saved the flavor capacity - daily updated

     → workflow (Build automatically by ***

        → Prod

            → <jobName>.yaml
                    .
                    .
                    . 

        → Telaviv

            → <jobName>.yaml
                    .
                    .
                    . 

> Sync all pods per DC

### Installs Criterions

#### Choose devices in Stock by the following parameters
- Health OK
- Cpu cores number >= CpuCores by flavor
- Total memory >= TotalMemory by flavor
- Disk Size (equal)
- Total Disks Number( Dosent get Mix servers - HDD/SSD )
- Arista Switch Rack

##### Errors
- No capacity in stock  - No Devices in Stock (DCIM)
- No devices in stock!- No Devices in Stock (DCIM & Spider)
- No match devices in stock! - Doesn't found devices in Stock are matched flavor
- No free stock of servers per your request, please contact to DC Eng. team

#### Multi Installing
- Rack balance
- Hostname number are sequence(xxpx-xxx**200**,xxpx-xxx**201**)
- When cluster installs and specific number hostname use --min

#### Variables
> [Variables Location](https://gitlab.com/oris1002/myunicorn/-/blob/master/files/variables/variables.yaml)
- Limit number of stack devices per datacenter
- Convention Name per datacenter and segment
- Rundeck URL


#### Profile
> [Profiles Location](https://gitlab.com/oris1002/myunicorn/-/tree/master/profiles)

**Installs according to parameters in the yaml file**

- Profiles Example:
```
    {

      Image: centos7.5_k8s_gold,
      Owner: kube_clusters,
      Hostname_connvention: knc
    }
```


#### Flavors
> [Flavors Location](https://gitlab.com/oris1002/myunicorn/-/tree/master/flavors)

***Gets device and configurations RAID according to parameters in the yaml file***
- Flavors Example:
```
{
  TotalDisks: 6,
  DiskSize: 960,
  Num_Of_Boot_Disks: "6",
  Boot_Disk_Raid: "0",
  Data_Disk_Raid: "0",
  TotalMemory: 128,
  CpuCores: 20,
  is_ssd: True,
  Price: 6500
}
```


***
## Client

#### Usage
```
Options:
    --version               show program's version number and exit
    -h, --help              show this help message and exit

=============================================== Modes =======================================================
All:
    show                    Show property list (images | profiles | flavors | dc)

Physical:
    create                  Create a new physical server
    clone                   Clone an existing physical server into a new one
    delete                  Delete an existing server
    retry                   Retry installation
    firmware_update         update specific serial

OpenStack:
    create_openstack        Create a new instance
    clone_openstack         Clone an existing instance into a new one
    delete_openstack        Delete an existing instance
    live_migration          Live migration

Vmware:
    create_vmware           Create a new instance
    delete_vmware           Delete an existing instance

See more:
    lpunicorn <mode> --help
```


#### Examples
```
lpunicorn show --help
lpunicorn create --help
lpunicorn retry --help
lpunicorn delete --help

lpunicorn show --dc
lpunicorn show --flavors # show all flavors
lpunicorn show --flavors m1.large # show flavors details
lpunicorn show --profile # show all profiles
lpunicorn show --profile openstack # # show profile details

lpunicorn create --dc lon --profile openstack --user USER --hostname slpr-tst999
lpunicorn create --dc lon --profile openstack --user USER
lpunicorn create --dc lon --profile openstack --user USER --serial SERIAL --reinstall

lpunicorn retry --job_id 51085 --user USER

lpunicorn delete --dc lon --serial SERIAL --user USER --hostname slpr-tst999
```
![Example](Picture/Screen_Shot_2020-10-27_at_17.57.32.png)
> Active Directory Verify authentication in CREATE/DELETE mode


***
:unicorn:
By Ori Shinsholker

Bugs: oris@liveperson.com
