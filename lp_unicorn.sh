#!/bin/sh
# run python script.
source /usr/bin/preflight.sh &
/usr/bin/python3.6 /lp_unicorn/unicorn/files/core/reports/BigQuery/save_data.py &
/usr/bin/python3.6 /lp_unicorn/unicorn/files/core/git_clone_repos.py &
/usr/bin/python3.6 /lp_unicorn/unicorn/files/core/app.py
#/usr/bin/python3.6 /lp_unicorn/unicorn/files/core/state/update_jobs.py
