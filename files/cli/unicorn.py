#!/bin/python3.6
# PYTHON_ARGCOMPLETE_OK
import os
import getpass
import re
import argcomplete
import sys, select
import argparse
import sys
import getpass
import datetime
import time
import subprocess
import tabulate
import requests
import requests.packages.urllib3
import json
import logging
import time
import curses
from goto import with_goto
from logging.handlers import RotatingFileHandler

requests.packages.urllib3.disable_warnings()

# if getpass.getuser() != 'sysadmin':
# 	print("Error: unicorn can be run only from full sysadmin shell")
# 	exit(1)
# eval "$(register-python-argcomplete /home/sysadmin/unicorn/py_2_bin/dist/new_cli)"
# activate-global-python-argcomplete

global logger
logger = None
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/cli', '')
sys.path.append(home_dir)
VERSION = '3.12'

SHOW = False
CREATE = False
global unicorn_url, unicorn_url_dev
unicorn_url = 'URL'
# unicorn_url = 'http://ca.unicorn.int.liveperson.net/'
unicorn_url_dev = 'URL_DEV'

# unicorn_url = 'http://127.0.0.1:8080/'
unicorn = '''
                                                                                   /
                                                                              ,.. /
 __  __                                                                     ,'   ';
/\ \/\ \          __                                             ,,.__    _,' /';  .
\ \ \ \ \    ___ /\_\    ___    ___   _ __   ___                :','  ~~~~    '. '~
 \ \ \ \ \ /' _ `\/\ \  /'___\ / __`\/\`'__/' _ `\             :' (   )         ):,
  \ \ \_\ \/\ \/\ \ \ \/\ \__//\ \L\ \ \ \//\ \/\ \            '. '. .=----=..-~ .;'
   \ \_____\ \_\ \_\ \_\ \____\ \____/\ \_\\ \_\ \_\            '  ;' ::    ':. '"
    \/_____/\/_/\/_/\/_/\/____/\/___/  \/_/ \/_/\/_/              (:  ':     ;)
                                                                   \\  '"   ./
                                                                    '"      '"
'''


def complete_subparser_action(prefix, **kwargs):
    internal_actions = ['foo', 'bar']
    if prefix and any(action.startswith(prefix) for action in internal_actions):
        return internal_actions
    else:
        external_actions = subprocess.check_output("compgen -c " + prefix, shell=True, executable='/bin/bash')
        external_actions = [a.decode() for a in external_actions.splitlines()]
        return internal_actions + external_actions


def init_args():
    global parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, add_help=False)
    parser.prog = 'lpunicorn'
    parser.usage = 'lpunicorn { show | create | clone | retry | delete | create_openstack  | clone_openstack | delete_openstack | create_vmware | delete_vmware  } [--help]'

    subparsers = parser.add_subparsers()
    subparsers.completer = complete_subparser_action

    sb_show = subparsers.add_parser('show', add_help=False)
    sb_create = subparsers.add_parser('create', add_help=False)
    sb_clone = subparsers.add_parser('clone', add_help=False)
    sb_delete = subparsers.add_parser('delete', add_help=False)
    sb_retry = subparsers.add_parser('retry', add_help=False)
    sb_create_vmware = subparsers.add_parser('create_vmware', add_help=False)
    sb_delete_vm = subparsers.add_parser('delete_vmware', add_help=False)
    sb_create_openstack = subparsers.add_parser('create_openstack', add_help=False)
    sb_clone_openstack = subparsers.add_parser('clone_openstack', add_help=False)
    sb_delete_openstack = subparsers.add_parser('delete_openstack', add_help=False)
    sb_firmware_update = subparsers.add_parser('firmware_update', add_help=False)

    parser.add_argument("-h", "--help", action="store_true")
    parser.add_argument("-v", "--version", action="store_true")

    #### retry job ####
    sb_retry.add_argument("-h", "--help", action="store_true")
    sb_retry.add_argument("--job_id", metavar='job_id', nargs='?')
    sb_retry.add_argument("--user", metavar='user', nargs='?')
    sb_retry.add_argument("--image", metavar='image', nargs='?')
    sb_retry.add_argument("--flavor", metavar='image', nargs='?')
    sb_retry.add_argument("--lprole", metavar='lprole', nargs='?')
    sb_retry.add_argument("--lpcluster", metavar='lpcluster', nargs='?')
    sb_retry.add_argument("--no_arista", action="store_true")
    sb_retry.add_argument("--reinstall", action="store_true")
    sb_retry.add_argument("--skip_yum_update", action="store_true")
    sb_retry.add_argument("--skip_server_firmware", action="store_true")
    sb_retry.add_argument("--force_poweroff", action="store_true")
    sb_retry.add_argument("--hba_mode", action="store_true")
    sb_retry.add_argument("--sda_only", action="store_true")
    #### retry job ####

    #### Show ####
    sb_show.add_argument("-h", "--help", action="store_true")
    sb_show.add_argument("--dc", nargs='?')
    sb_show.add_argument("--profiles", nargs='*')
    sb_show.add_argument("--flavors", nargs='*')
    sb_show.add_argument("--flavor_capacity", nargs='*')
    sb_show.add_argument("--jobs", action="store_true")
    sb_show.add_argument("--images", action="store_true")
    # sb_show.add_argument("--devices", action="store_true")  # show devices list
    sb_show.add_argument("--flavors_openstack", action="store_true")
    sb_show.add_argument("--images_openstack", action="store_true")
    sb_show.add_argument("--projects_openstack", action="store_true")
    # sb_show.add_argument("--fields", metavar='fields', nargs='*')  # split by comma
    # sb_show.add_argument("--DF", metavar='deviceField', nargs='*')  # DF - Device fields #split by comma device filter
    sb_show.add_argument("--user", metavar='user', nargs='?')
    #### Show ####

    #### Create Physical ####
    sb_create.add_argument("-h", "--help", action="store_true")
    sb_create.add_argument("-f", "--force", action="store_true")
    sb_create.add_argument("--profile", metavar='profile', nargs='?')
    sb_create.add_argument("--user", metavar='user', nargs='?')
    sb_create.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_create.add_argument("-s", "--segment", metavar='segment', nargs='?', default='shared',
                           help=argparse.SUPPRESS)  # alpha / shared / smb / ent
    # sb_create.add_argument("--sub_profile", metavar='sub_profile', nargs='?')
    sb_create.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1,
                           help=argparse.SUPPRESS)
    sb_create.add_argument("--image", metavar='image', nargs='?')
    sb_create.add_argument("--flavor", metavar='image', nargs='?')
    sb_create.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_create.add_argument("--serial", metavar='serial', nargs='?')
    sb_create.add_argument("--min", metavar='min', default=100, nargs='?', type=int)
    sb_create.add_argument("--lprole", metavar='lprole', nargs='?')
    sb_create.add_argument("--lpcluster", metavar='lpcluster', nargs='?')
    sb_create.add_argument("--no_arista", action="store_true")
    sb_create.add_argument("--hw_type", metavar='hw_type', nargs='?')
    sb_create.add_argument("--reinstall", action="store_true")
    sb_create.add_argument("--skip_yum_update", action="store_true")
    sb_create.add_argument("--skip_server_firmware", action="store_true")
    sb_create.add_argument("--server_firmware", action="store_true",
                           help=argparse.SUPPRESS)
    sb_create.add_argument("--puppet_env", action="store_true")
    sb_create.add_argument("--boot_disk", action="store_true")
    sb_create.add_argument("--hba_mode", action="store_true")
    sb_create.add_argument("--force_poweroff", action="store_true")
    sb_create.add_argument("--sda_only", action="store_true")
    #### Create Physical ####

    #### Create VM ####
    sb_create_vmware.add_argument("-h", "--help", action="store_true")
    sb_create_vmware.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_create_vmware.add_argument("--user", metavar='user', nargs='?')
    sb_create_vmware.add_argument("--image", metavar='image', nargs='?')
    sb_create_vmware.add_argument("--hostname", metavar='hostnmae', nargs='?')

    sb_create_vmware.add_argument("--skip_yum_update", action="store_true")
    sb_create_vmware.add_argument("--skip_dns_check", action="store_true")
    sb_create_vmware.add_argument("-annotations", "--annotations", metavar='description', nargs=1,
                                  help=argparse.SUPPRESS)
    sb_create_vmware.add_argument("-s", "--segment", metavar='segment', nargs='?', default='shared',
                                  help=argparse.SUPPRESS)  # alpha / shared / smb / ent
    sb_create_vmware.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1,
                                  help=argparse.SUPPRESS)
    sb_create_vmware.add_argument("--disk_size", metavar='disk_size', nargs='?', type=int, default=60,
                                  help=argparse.SUPPRESS)
    sb_create_vmware.add_argument("--disk_type", metavar='disk_type', nargs='?', type=str, default="thick",
                                  help=argparse.SUPPRESS)
    sb_create_vmware.add_argument("--memory", metavar='memory', nargs='?', type=int, default=4,
                                  help=argparse.SUPPRESS)
    sb_create_vmware.add_argument("--cpus", metavar='cpus', nargs='?', type=int, default=4,
                                  help=argparse.SUPPRESS)
    #### Create VM ####

    #### Create Openstack Instace ####
    sb_create_openstack.add_argument("-h", "--help", action="store_true")
    sb_create_openstack.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_create_openstack.add_argument("--user", metavar='user', nargs='?')
    sb_create_openstack.add_argument("--image", metavar='image', nargs='?')
    sb_create_openstack.add_argument("--project", metavar='project', nargs='?')
    sb_create_openstack.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_create_openstack.add_argument("--flavor", metavar='image', nargs='?')
    sb_create_openstack.add_argument("--lprole", metavar='lprole', nargs='?')
    sb_create_openstack.add_argument("--f5_clone_server", metavar='f5_clone_server', nargs='?')
    sb_create_openstack.add_argument("--lpcluster", metavar='lpcluster', nargs='?')
    sb_create_openstack.add_argument("-s", "--segment", metavar='segment', nargs='?', default='shared',
                                     help=argparse.SUPPRESS)  # alpha / shared / smb / ent
    sb_create_openstack.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1,
                                     help=argparse.SUPPRESS)
    #### Create Openstack Instace ####

    #### Delete Openstack Instace ####
    sb_delete_openstack.add_argument("-h", "--help", action="store_true")
    sb_delete_openstack.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_delete_openstack.add_argument("--user", metavar='user', nargs='?')
    sb_delete_openstack.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_delete_openstack.add_argument("--force", action="store_true")
    #### Delete Openstack Instace ####

    #### clone Openstack Instace ####
    sb_clone_openstack.add_argument("-h", "--help", action="store_true")
    sb_clone_openstack.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_clone_openstack.add_argument("--user", metavar='user', nargs='?')
    sb_clone_openstack.add_argument("--image", metavar='image', nargs='?')
    sb_clone_openstack.add_argument("--flavor", metavar='image', nargs='?')
    sb_clone_openstack.add_argument("--cloneserver", metavar='hostnmae', nargs='?')
    sb_clone_openstack.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_clone_openstack.add_argument("--lprole", metavar='lprole', nargs='?')
    sb_clone_openstack.add_argument("--lpcluster", metavar='lpcluster', nargs='?')
    sb_clone_openstack.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1,
                                    help=argparse.SUPPRESS)
    sb_clone_openstack.add_argument("-s", "--segment", metavar='segment', nargs='?', default='shared',
                                    help=argparse.SUPPRESS)  # alpha / shared / smb / ent
    #### clone Openstack Instace ####

    #### Delete VM ####
    # --dc \t   datacenter \t  eg. va / ca/ uk / nl / sy / me (default the DC of current server)
    sb_delete.add_argument("-h", "--help", action="store_true")
    sb_delete.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_delete.add_argument("--serial", metavar='serial', nargs='?')
    sb_delete.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_delete.add_argument("--user", metavar='user', nargs='?')
    sb_delete_vm.add_argument("-h", "--help", action="store_true")
    sb_delete_vm.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_delete_vm.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_delete_vm.add_argument("--user", metavar='user', nargs='?')
    #### Delete VM ####


    #### Firmware ####
    # --dc \t   datacenter \t  eg. va / ca/ uk / nl / sy / me (default the DC of current server)
    sb_firmware_update.add_argument("-h", "--help", action="store_true")
    sb_firmware_update.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_firmware_update.add_argument("--serial", metavar='serial', nargs='?')
    sb_firmware_update.add_argument("--user", metavar='user', nargs='?')
    sb_firmware_update.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1,
                                    help=argparse.SUPPRESS)
    #### Firmware ####


    #### Clone Physical ####
    sb_clone.add_argument("-h", "--help", action="store_true")
    sb_clone.add_argument("-d", "--dc", metavar='datacenter', nargs='?')
    sb_clone.add_argument("--hostname", metavar='hostnmae', nargs='?')
    sb_clone.add_argument("--cloneserver", metavar='cloneserver', nargs='?')
    sb_clone.add_argument("--user", metavar='user', nargs='?')
    sb_clone.add_argument("-f", "--force", action="store_true")
    sb_clone.add_argument("--profile", metavar='profile', nargs='?')
    sb_clone.add_argument("-s", "--segment", metavar='segment', nargs='?',
                          default='shared')  # alpha / shared / smb / ent
    sb_clone.add_argument("--quantity_servers", metavar='quantity_servers', nargs='?', type=int, default=1)
    sb_clone.add_argument("--image", metavar='image', nargs='?')
    sb_clone.add_argument("--flavor", metavar='image', nargs='?')
    sb_clone.add_argument("--serial", metavar='serial', nargs='?')
    sb_clone.add_argument("--min", metavar='min', nargs='?', type=int)
    sb_clone.add_argument("--lprole", metavar='lprole', nargs='?')
    sb_clone.add_argument("--lpcluster", metavar='lpcluster', nargs='?')
    sb_clone.add_argument("--no_arista", action="store_true")
    sb_clone.add_argument("--hw_type", metavar='hw_type', nargs='?')
    sb_clone.add_argument("--reinstall", action="store_true")
    sb_clone.add_argument("--skip_yum_update", action="store_true")
    sb_clone.add_argument("--skip_server_firmware", action="store_true")
    sb_clone.add_argument("--server_firmware", action="store_true")
    sb_clone.add_argument("--force_poweroff", action="store_true")
    sb_clone.add_argument("--sda_only", action="store_true")
    sb_clone.add_argument("--hba_mode", action="store_true")
    #### Clone Physical ####
    argcomplete.autocomplete(parser)

    global args
    args = parser.parse_args()
    return parser, args


def print_help_main(message=''):
    print(unicorn + '''
Options:
    --version               show program's version number and exit
    -h, --help              show this help message and exit

=============================================== Modes =======================================================
All:
    show                    Show property list (images | profiles | flavors | dc)

Physical:
    create                  Create a new physical server
    clone                   Clone an existing physical server into a new one
    delete                  Delete an existing server
    retry                   Retry installation
    firmware_update         update specific serial 

OpenStack:
    create_openstack        Create a new instance
    clone_openstack         Clone an existing instance into a new one
    delete_openstack        Delete an existing instance

Vmware:
    create_vmware           Create a new instance
    delete_vmware           Delete an existing instance

See more:
    lpunicorn <mode> --help

======================================== Required Properties =================================================

    --user USER             Your LPDOMMAIN user
    --dc                    Show datacenter list
    --profile PROFILE       Specify a profile
    --flavor                Specify a flavor
    --image                 Override the image is chosen (profile yaml)
    --lprole LPROLE         Tag your server(s) with lprole
    --lpcluster LPCLUSTER   Tag your server(s) with lpcluster

========================================= Optional Properties ================================================

    --hostname HOSTNAME     Specify a hostname (delimited by comma for multi inserts)
    --segment SEGMENT       Specify a segment (default: shared) (alpha | shared | smb | ent | uk)
    --quantity_servers      Specify how many servers to create (default: 1)
    --serial SERIAL         Supply a serial number (delimited by comma for multi inserts)
    --reinstall             Reinstall server
    --sda_only              sda only
    --skip_yum_update       No yum update
    --skip_server_firmware  Skip server firmware (by default)
    --server_firmware       Server firware (don't skip)
    --min NUMBER            Minimum number (Hostname)

============================================ Useful commands =================================================

    lpunicorn show --dc
    lpunicorn show --flavors # show all flavors
    lpunicorn show --flavors m1.large # show flavors details
    lpunicorn show --flavor_capacity # Show flavors list and capacity servers per DC
    lpunicorn show --flavor_capacity [flavor_name] --dc <dc> # Specific flavor per DC
    lpunicorn show --profiles # show all profiles
    lpunicorn show --profile openstack  # show profile details
    lpunicorn show --images # show all profiles
    lpunicorn show --user USER --jobs  # show your jobs

================================================ Examples ====================================================

    lpunicorn create --dc lon --profile openstack --flavor  m2.large --user USER --hw_type hp --lprole LPROLE --lpcluster LPCLUSTER
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --hostname rapr-tst998,rapr-tst999 --force_poweroff --skip_server_firmware --skip_yum_update --reinstall --q 2
    lpunicorn delete --dc oak --user USER --hostname ropr-tst999 --serial SERIAL
    lpunicorn retry --job_id 51085 --user USER
	''')
    print(Colors.red + message + Style.reset)


def print_help_show(message=''):
    print(unicorn + '''

========================================= Optional Properties ================================================

    --help                    Show this help message and exit
    --dc                      Show datacenter list
    //physical
    --flavors                 Show flavors list
    --flavor_capacity         Show flavors list and capacity servers per DC
    --images                  Show image list
    --profile [ profile ]     Show profiles list / show profile details
    //openstack
    --images_openstack        Show image list
    --projects_openstack      Show project list
    --flavors_openstack       Show flavors list Openstack
                              Specs: https://confluence.liveperson.com/display/cloudops/Flavors
    --user USERNAME           Your AD user name

================================================ Examples ====================================================

    lpunicorn show --dc
    lpunicorn show --images
    lpunicorn show --user USER --jobs
    lpunicorn show --flavors # show all flavors
    lpunicorn show --flavors m1.large # show flavors details
    lpunicorn show --profile # show all profiles
    lpunicorn show --profile openstack # # show profile details
    lpunicorn show --flavor_capacity # show all flavors capacity of all DCs
    lpunicorn show --flavor_capacity [flavor_name] # Specific flavor name
    lpunicorn show --flavor_capacity [flavor_name] --dc <dc> # Specific flavor per DC

    #openstack
    lpunicorn show --flavors_openstack  # show flavors openstack list
    lpunicorn show --images_openstack  # show images openstack list
    lpunicorn show --projects_openstack # # show project details

    ''')
    print(Colors.red + message + Style.reset)


# print(message)


def print_help_create(message=''):
    print(unicorn + '''

======================================== Required Properties =================================================

    --user USER             Your LPDOMMAIN user
    --dc                    Show datacenter list
    --profile PROFILE       Specify a profile
    --flavor                Specify a flavor
    --image                 Override the image is chosen (profile yaml)
    --lprole LPROLE         Tag your server(s) with lprole
    --lpcluster LPCLUSTER   Tag your server(s) with lpcluster

========================================= Optional Properties ================================================

    --hostname HOSTNAME       Specify a hostname (delimited by comma for multi inserts)
    --segment SEGMENT         Specify a segment (default: shared) (alpha | shared | smb | ent | uk)
    --quantity_servers        Specify how many servers to create (default: 1)
    --serial SERIAL           Supply a serial number (delimited by comma for multi inserts)
    --reinstall               Reinstall server
    --no_arista               The device doesn't exist on a rack with arista switch
    --sda_only                sda only
    --skip_yum_update         No yum update
    --force_poweroff          Force power off the server
    --skip_server_firmware    Skip server firmware (by default)
    --server_firmware         Server firware (don't skip)
    --min NUMBER              Minimum number (Hostname)
    --hw_type TYPE            Choose either Dell \ Hp. (default: no preferences)

================================================ Examples ====================================================

    lpunicorn create --dc oak --profile openstack --flavor  m2.large --user USER --hostname ropr-tst999
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --segment alpha --hostname rapr-tst999 --serial SERIAL --reinstall
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --hostname rapr-tst999 --force_poweroff --skip_server_firmware --skip_yum_update
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --hostname rapr-tst999,rapr-tst777 --q 2 --lprole LPROLE --lpcluster LPCLUSTER
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --hostname rapr-tst999,rapr-tst777 --serial SERIALA,SERIALB --q 2
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --serial SERIALA,SERIALB --q 2 --hw_type Hp
    lpunicorn create --dc ams --profile openstack --flavor  m2.large --user USER --min 200 --q 2 # Create 2 servers rapr-tst200 - 201
    lpunicorn create --dc oak --profile kubernetes_mng --flavor  m2.large --user USER --hostname ropr-tst999 --puppet_env=infra
    lpunicorn create --dc oak --profile kubernetes_mng --flavor  m2.large --user USER --hostname ropr-tst999 --hba_mode
        ''')
    print(Colors.red + message + Style.reset)


# print(message)
def print_help_create_openstack_instacne(message=''):
    print(unicorn + '''

======================================== Required Properties =================================================

    --user USER             Your LPDOMMAIN user
    --dc DATACENTER			Show datacenter list
    --profile PROFILE       Specify a profile
    --lprole LPROLE         Tag your server(s) with lprole
    --lpcluster LPCLUSTER   Tag your server(s) with lpcluster
    --hostname HOSTNAME       Specify a hostname (delimited by comma for multi inserts)

========================================= Optional Properties ================================================

    --project project         Specify a profile (by default: Defualt)
    --segment SEGMENT         Specify a segment (default: shared) (alpha | shared | smb | ent | uk)
    --quantity_servers        Specify how many servers to create (default: 1)
    --image                   Specify an image (by default: centos7_gold)
    --flavor                  Specify a flavor - for flavor list: lpunicorn show --flavor_openstack
    --min NUMBER              Minimum number (Hostname)

================================================ Examples ====================================================

    lpunicorn create_openstack --dc tlv --flavor  m2.large --user USER --hostname roor-tst
    lpunicorn create_openstack --dc tlv --project default --flavor  m2.large --user USER --hostname roor-tst999
        ''')
    print(Colors.red + message + Style.reset)


# print(Colors.lightblue + Style.bold + Style.blink + "Supported only to TelAviv farm\n" + Style.reset)


def print_help_create_vmware(message=''):
    print(unicorn + '''

positional the following arguments:
    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --hostname HOSTNAME       Specify a hostname (delimited by comma for multi inserts)
    --annotations ANNOTATIONS Annotations about your server(s). Description should contain more then ten characters

========================================= Optional Properties ================================================

    --user USER               Your LPDOMMAIN user
    --image Image             Specify a Image - (default: CentOS_VMWARE_gold)
    --segment SEGMENT         Specify a segment (default: shared) (alpha | shared | smb | ent | uk)
    --cpus CPUS               Specify a CPUs (default: 4) Enforced from Allowed Values:[ 2,4,8,16,32 ]
    --quantity_servers        Specify how many servers to create (default: 1)
    --memory MEMORY           Specify a Memory (default: 4GB) Enforced from Allowed Values:[ 2,4,8,16,32 ] GB
    --disk_size DISK_SIZE     Specify a Disk Size
    --disk_type DISK_TYPE     Specify a Disk Type (default: Thick) Enforced from Allowed Values: [ Thick,Thin ]
    --skip_yum_update         No yum update
    --skip_DNS_Check          No DNS check
    --lprole LPROLE           Tag your server(s) with lprole
    --lpcluster LPCLUSTER     Tag your server(s) with lpcluster

================================================ Examples ====================================================

    lpunicorn create_vmware --dc oak --user USER --hostname rovr-tst999 --annotations "my testing"
        ''')
    print(Colors.red + message + Style.reset)


# print(message)

def print_help_clone(message=''):
    print(unicorn + '''

positional the following arguments:
    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --cloneserver SERVERNAME  Specify a hostname server to clone

========================================= Optional Properties ================================================

    --user USER               Your LPDOMMAIN user
    --hostname HOSTNAME       Specify a hostname (delimited by comma for multi inserts)
    --segment SEGMENT         Specify a segment (default: shared) (alpha | shared | smb | ent | uk)
    --quantity_servers        Specify how many servers to create (default: 1)
    --serial SERIAL           Supply a serial number (delimited by comma for multi inserts)
    --reinstall               Reinstall server
    --no_arista               The device doesn't exist on a rack with arista switch
    --sda_only                sda only
    --image                   Override the image is chosen (profile yaml)
    --skip_yum_update         No yum update
    --force_poweroff          Force power off the server
    --server_firmware         Server firware (don't skip)
    --min NUMBER              Minimum number (Hostname)
    --hw_type TYPE            Choose either Dell \ Hp. (default: no preferences)


================================================ Examples ====================================================

    lpunicorn clone --dc ams --cloneserver rapr-tst999 --user USER
    lpunicorn clone --dc ams --cloneserver rapr-tst999 --user USER--segment alpha --hostname rapr-tst999 --serial SERIAL --reinstall
        ''')
    print(Colors.red + message + Style.reset)


def print_help_clone_openstack(message=''):
    print(unicorn + '''

positional the following arguments:
    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --cloneserver SERVERNAME  Specify a hostname server to clone
    --hostname HOSTNAME       Specify a hostname (delimited by comma for multi inserts)
    --user USER               Your LPDOMMAIN user

========================================= Optional Properties ================================================

    --quantity_servers        Specify how many servers to create (default: 1)
    --lprole LPROLE           Tag your server(s) with lprole
    --lpcluster LPCLUSTER     Tag your server(s) with lpcluster

================================================ Examples ====================================================

    lpunicorn clone_openstack --dc tlv --cloneserver rapr-tst999 --user USER --hostname qtor-tst
    lpunicorn clone_openstack --dc tlv --cloneserver rapr-tst999 --user USER --hostname qtor-tst102
    lpunicorn clone_openstack --dc tlv --cloneserver rapr-tst999 --user USER --hostname qtor-tst102 --lprole LPROLE --lpcluster LPCLUSTER
    lpunicorn clone_openstack --dc tlv --cloneserver rapr-tst999 --user USER --hostname qtor-tst102,qtor-tst103
        ''')
    print(Colors.red + message + Style.reset)


# print(Colors.lightblue + Style.bold + Style.blink + "Supported only to TelAviv farm\n" + Style.reset)


# print(message)


def print_help_retry(message=''):
    print(unicorn + '''

positional the following arguments:
    --help                    Show this help message and exit
    --job_id                  Specify a job ID (delimited by comma for multi inserts)

========================================= Optional Properties ================================================

    --user USER               Your LPDOMMAIN user
    --image                   Override the image is chosen
    --reinstall               Reinstall server
    --no_arista               The device doesn't exist on a rack with arista switch
    --sda_only                sda only
    --skip_yum_update         No yum update
    --force_poweroff          Force power off the server
    --skip_server_firmware    Skip server firware
    --lprole LPROLE           Tag your server(s) with lprole
    --lpcluster LPCLUSTER     Tag your server(s) with lpcluster

================================================ Examples ====================================================

    lpunicorn retry --job_id 51085 --user USER
    lpunicorn retry --job_id 51085 --user USER --image centos7.5_gold
    lpunicorn retry --job_id 51085,51086 --user USER

        ''')
    print(Colors.red + message + Style.reset)


# print(message)


def print_help_delete(message=''):
    print(unicorn + '''

========================================= Optional Properties ================================================

    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --hostname HOSTNAME       Specify a hostname
    --serial SERIAL           Supply a serial number
    --user USER               Your LPDOMMAIN user

================================================ Examples ====================================================

    lpunicorn delete --dc oak --user USER --hostname ropr-tst999 --serial SERIAL
    lpunicorn delete --dc oak --user USER --hostname ropr-tst999,ropr-tst666 --serial SERIALA,SERIALB
        ''')
    print(Colors.red + message + Style.reset)


# print(message)


def print_help_delete_vm(message=''):
    print(unicorn + '''

========================================= Optional Properties ================================================

    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --hostname HOSTNAME       Specify a hostname
    --user USER               Your LPDOMMAIN user

================================================ Examples ====================================================

     lpunicorn delete_vm --dc oak --user USER --hostname rovr-tst999
     lpunicorn delete_vm --dc oak --user USER --hostname rovr-tst999,ravr-tst666
        ''')
    print(Colors.red + message + Style.reset)


def print_help_delete_openstack(message=''):
    print(unicorn + '''

========================================= Optional Properties ================================================

    --help                    Show this help message and exit
    --dc DATACENTER           Specify a datacenter (Regex)
    --hostname HOSTNAME       Specify a hostname
    --user USER               Your LPDOMMAIN user

optional arguments:
	--foce                    force delete

================================================ Examples ====================================================

     lpunicorn delete_openstack --dc tlv --user USER --hostname roor-tst999
     lpunicorn delete_openstack --dc tlv --user USER --hostname roor-tst999,roor-tst666
        ''')
    print(Colors.red + message + Style.reset)



def print_help_firmware_update(message=''):
    pass

# print(Colors.lightblue + Style.bold + Style.blink + "Supported only to TelAviv farm\n" + Style.reset)


# print(message)
def get_dc():
    # Check on which DC i'm running, in order to run locally
    p = subprocess.Popen('facter -p datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8').strip()
    # dict_fields['datacenter'] = dc_name.title()
    return dc_name.title()


def verify_flags():
    logger.info(sys.argv)
    global SHOW, CREATE, CREATE_VM, CREATE_OPENSTACK_INSTANCE, DELETE, DELETE_VM, DELETE_OPENSTACK_INSTANCE, RETRY, CLONE, CLONE_OPENSTACK_INSTANCE, ENV, FIRMWARE_UPDATE

    SHOW = True if 'show' in sys.argv else False
    FIRMWARE_UPDATE = True if 'firmware_update' in sys.argv else False
    CREATE = True if 'create' in sys.argv else False
    CREATE_VM = True if 'create_vmware' in sys.argv else False
    CREATE_OPENSTACK_INSTANCE = True if 'create_openstack' in sys.argv else False
    DELETE = True if 'delete' in sys.argv and 'delete_vm' not in sys.argv else False
    DELETE_VM = True if 'delete_vm' in sys.argv else False
    DELETE_OPENSTACK_INSTANCE = True if 'delete_openstack' in sys.argv else False
    RETRY = True if 'retry' in sys.argv else False
    CLONE = True if 'clone' in sys.argv else False
    CLONE_OPENSTACK_INSTANCE = True if 'clone_openstack' in sys.argv else False
    if 'Telaviv' in get_dc():
        global unicorn_url
        unicorn_url = unicorn_url_dev
        ENV = 'DEV'
    else:
        ENV = 'prod'

    logger.info('new session => {}'.format(vars(args)))

    if args.version:
        print('unicorn ' + VERSION)
        sys.exit(0)
    if len(sys.argv) <= 2 or args.help:
        if SHOW:
            print_help_show()
        elif CREATE:
            print_help_create()
        elif CREATE_VM:
            print_help_create_vmware()
        elif CREATE_OPENSTACK_INSTANCE:
            print_help_create_openstack_instacne()
        elif DELETE:
            print_help_delete()
        elif DELETE_VM:
            print_help_delete_vm()
        elif DELETE_OPENSTACK_INSTANCE:
            print_help_delete_openstack()
        elif RETRY:
            print_help_retry()
        elif CLONE:
            print_help_clone()
        elif CLONE_OPENSTACK_INSTANCE:
            print_help_clone_openstack()
        else:
            # print(unicorn)
            print_help_main()
        # parser.print_help()
        sys.exit(0)

    if args.dc:
        if args.dc != []:
            dcs = ['datacenter_1','datacenter_2','datacenter_3','datacenter_4']

            dc = [x for x in dcs if args.dc.lower() in x]
            if not dc:
                logger.error('Error: Invalid dataenter, inserted => ' + args.dc)

            if args.dc.lower() in ['telaviv', 'tlv']:
                logger.info("DC => {}".format(args.dc))
                args.dc = 'TelAviv'
                ENV = 'DEV'
                # global unicorn_url
                unicorn_url = unicorn_url_dev
                if not (CREATE_OPENSTACK_INSTANCE or DELETE_OPENSTACK_INSTANCE or CLONE_OPENSTACK_INSTANCE):
                    logger.error('** {} mode isn\'t supported in {} DC'.format(sys.argv[1], args.dc))
                    print_help_main(
                        Colors.red + '** {} mode isn\'t supported in {} datacenter ***'.format(sys.argv[1],
                                                                                               args.dc) + Style.reset)
                    sys.exit(1)
            else:
                logger.info('Datacenter => ' + dc[0].title())
                args.dc = dc[0].title()

    if CLONE:
        if not args.cloneserver or not args.dc:
            logger.error('clone without --cloneserver or --dc')
            print_help_clone(
                Colors.lightcyan + '** For clone, must insert --cloneserver <clone server name> and --dc <datacenter> **' + Style.reset)
            sys.exit(1)

        args.flavor, args.profile, args.lprole, args.lpcluster = (get_facter_from_zoom(args.cloneserver))
        CREATE = True

    if CLONE_OPENSTACK_INSTANCE:
        if not args.cloneserver or not args.dc or not args.hostname:
            logger.error('clone without --cloneserver or --dc or --hostname')
            print_help_clone_openstack(
                Colors.lightcyan + '** For clone, must insert --cloneserver <clone server name> and --dc <datacenter> and --hostname <hostname> **' + Style.reset)
            sys.exit(1)
        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)
        return

    if FIRMWARE_UPDATE:
        if not args.serial or not args.dc:
            logger.error('firmware update without --serial or --dc')
            print_help_firmware_update(
                Colors.lightcyan + '** For clone, must insert --serial <serial number> and --dc <datacenter> **' + Style.reset)
            sys.exit(1)
        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)
        return
    # args.flavor, args.profile, args.lprole, args.lpcluster = (get_facter_from_zoom(args.cloneserver))
    # CREATE = True
    if RETRY:
        if not args.job_id:
            logger.error('retry without --job_id')
            print_help_retry(Colors.lightcyan + '** For retry job, must insert --job_id <job_id> **' + Style.reset)
            sys.exit(1)
        else:
            verify_ad_auth()
            print(Colors.pink + 'Authentication successed :)' + Style.reset)
            return

    if SHOW:
        logger.info('SHOW MODE')
        # if not args.dc and not args.profile and not args.os:
        if set(['dc', 'profiles', 'images']) & set(sys.argv):
            logger.error('dc/profiles/images no inserted')
            # parser.epilog ='** You must choose dc/profile/os **'
            print_help_show(Colors.lightcyan + '** You must choose dc/profile/images **' + Style.reset)
            # parser.print_help()
            sys.exit(1)

        if args.jobs and not args.user:
            logger.error('show jobs, without user')
            print_help_show(Colors.lightcyan + '** You must insert user name (--user) **' + Style.reset)
            # parser.print_help()
            sys.exit(1)
            if args.dc:
                if args.dc == []:
                    logger.error('show jobs, without dc value ')
                    print_help_show(Colors.lightcyan + '** You must insert DC name (--dc DC) **' + Style.reset)
                    # parser.print_help()
                    sys.exit(1)

    if CREATE:

        logger.info('CREATE MODE')
        if not args.dc:
            logger.error('Dc dosent inserted')
            print_help_create('** You must enter a DC **')
            sys.exit(1)
        if args.segment == 'alpha' and args.dc not in ('Datacenter_1', 'Datacenter_2'):
            logger.error('Alpha segment exist only in Datacenter_1, Datacenter_2 datacentes')
            print_help_create(
                Colors.lightcyan + '** Alpha segment exist only in Datacenter_1, Datacenter_2 datacente **' + Style.reset)
            sys.exit(1)
        if not args.lprole or not args.lpcluster:
            logger.error('lprole/lpcluster dosent inserted')
            print_help_create(Colors.red + '** You must insert lprole and lpcluster **' + Style.reset)
            sys.exit(1)

        if args.hostname:

            if type(args.hostname) is list:
                args.hostname = args.hostname[0]

            elif len(set(args.hostname.split(','))) != args.quantity_servers:
                logger.error('less hostname as quantity servers. {} - hostname, {} - quantity'.format(
                    len(set(args.hostname.split(','))), args.quantity_servers))
                print(Colors.red + 'Error: Please equals hostnames as quantity servers' + Style.reset)
                sys.exit(1)

        if not args.profile:
            logger.error('profile no inserted')
            print_help_create(Colors.lightcyan + '** You must enter a profile **' + Style.reset)
            # parser.print_help()
            sys.exit(1)

        if not args.flavor:
            logger.error('flavor no inserted')
            print_help_create(Colors.lightcyan + '** You must enter a flavor **' + Style.reset)
            # parser.print_help()
            sys.exit(1)

        if args.serial:
            if len(set(args.serial.split(','))) != args.quantity_servers:
                logger.error('less serialsNo. as quantity servers {} - serials, {} - quantity'.format(
                    len(set(args.serial.split(','))), args.quantity_servers))
                print('** Please insert equals serialsNo. as quantity servers **')
                sys.exit(1)
            if not args.force_poweroff:
                logger.error('inserted serials withput --force_poweroff')
                print(Colors.red + 'Error: You must use --force_poweroff when you selected serials' + Style.reset)
                sys.exit(1)

        if args.reinstall:
            if not args.serial:
                logger.error('Reinstall, serial no inserted')
                print_help_create(Colors.lightcyan + '** You must insert serial number **' + Style.reset)
                sys.exit(1)
        if args.hw_type:
            if args.hw_type.lower() not in ['dell', 'hp']:
                logger.error('Invalide hw_type')
                print_help_create(Colors.lightcyan + '** Invalid hw_type. should be dell \ hp **' + Style.reset)
                sys.exit(1)

        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)

    if CREATE_VM:

        logger.info('CREATE VM MODE')
        if not args.dc:
            logger.error('Dc dosent inserted')
            print_help_create_vmware('** You must enter a DC **')
            sys.exit(1)
        if args.hostname == 'alpha' and args.dc not in ('Datacenter_1', 'Datacenter_2'):
            logger.error('Alpha segment exist only in Datacenter_1, Datacenter_2 datacente')
            print_help_create_vmware(
                Colors.lightcyan + '** Alpha segment exist only in Datacenter_1, Datacenter_2 datacente **' + Style.reset)
            sys.exit(1)

        if not args.hostname:
            logger.error('Hostname dosent inserted')
            print_help_create_vmware('** You must enter a hostnmae **')
            sys.exit(1)

        if not args.annotations:
            logger.error('Annotations dosent inserted')
            print_help_create_vmware('** You must enter an annotations **')
            sys.exit(1)
        elif len(args.annotations[0]) < 10:
            logger.error('Annotations length less 10 chararters')
            print_help_create_vmware('** Annotations should contain more then 10 characters **')
            sys.exit(1)
        args.annotations = args.annotations[0]

        if args.cpus not in [2, 4, 8, 16, 32]:
            logger.error('Invalid cpus value')
            print_help_create_vmware('** Invalid cpus value. Should be [ 2,4,8,16,32 ]')
            sys.exit(1)
        if args.memory not in [2, 4, 8, 16, 32]:
            logger.error('Invalid memory value')
            print_help_create_vmware('** Invalid memory value. Should be [ 2,4,8,16,32 ]')
            sys.exit(1)
        args.memory = args.memory * 1024
        # if not args.lprole or not args.lpcluster:
        # 	logger.error('lprole/lpcluster dosent inserted')
        # 	print_help_create_vmware(Colors.red + '** You must insert lprole and lpcluster **' + Style.reset)
        # 	sys.exit(1)

        if args.hostname:
            # print(args.hostname)
            if type(args.hostname) is list:
                args.hostname = args.hostname[0]

            if len(set(args.hostname.split(','))) != args.quantity_servers:
                logger.error('less hostname as quantity servers. {} - hostname, {} - quantity'.format(
                    len(set(args.hostname.split(','))), args.quantity_servers))
                print(Colors.red + 'Error: Please equals hostnames as quantity servers' + Style.reset)
                sys.exit(1)
        # args.hostname = args.hostname.split(',')

        # if not args.profile:
        # 	logger.error('profile no inserted')
        # 	print_help_create(Colors.lightcyan + '** You must enter a profile **' + Style.reset)
        # 	# parser.print_help()
        # 	exit(1)

        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)

    if CREATE_OPENSTACK_INSTANCE:
        logger.info('CREATE OPENSTACK INSTANCE MODE')
        # global ENV
        if args.dc == 'tlv':
            ENV = 'DEV'

        if not args.user:
            logger.error('user dosent inserted')
            print_help_create_openstack_instacne('** You must enter a User **')
            sys.exit(1)

        if not args.dc:
            logger.error('Dc dosent inserted')
            print_help_create_openstack_instacne('** You must enter a DC **')
            sys.exit(1)
        if args.hostname == 'alpha' and args.dc not in ('Datacenter_1', 'Datacenter_2'):
            logger.error('Alpha segment exist only in Datacenter_1, Datacenter_2 datacente')
            print_help_create_openstack_instacne(
                Colors.lightcyan + '** Alpha segment exist only in Datacenter_1, Datacenter_2 datacente **' + Style.reset)
            sys.exit(1)

        if not args.hostname:
            logger.error('Hostname dosent inserted')
            print_help_create_openstack_instacne('** You must enter a hostnmae **')
            sys.exit(1)

        if args.hostname:
            # print(args.hostname)
            if len(set(args.hostname.split(','))) == 1:
                for name in set(args.hostname.split(',')):
                    regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}[0-9]{3}"
                    # print('regex' + regex)
                    # print(name)
                    if not re.match(regex, name):  # qtor-xxx999
                        regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}"
                        if not re.match(regex, name):  # qtor-xxx
                            logger.error('Invalid convention name - {}'.format(name))
                            print(Colors.red + 'Error: Invalid convention name - {}'.format(name) + Style.reset)
                            sys.exit(1)
            elif len(set(args.hostname.split(','))) != args.quantity_servers:
                logger.error('less hostname as quantity servers. {} - hostname, {} - quantity'.format(
                    len(set(args.hostname.split(','))), args.quantity_servers))
                print(Colors.red + 'Error: Please equals hostnames as quantity servers' + Style.reset)
                sys.exit(1)

            if type(args.hostname) is list:
                args.hostname = args.hostname[0]

        if ENV == 'DEV':
            # global unicorn_url
            unicorn_url = unicorn_url_dev
        else:
            verify_ad_auth()
            print(Colors.pink + 'Authentication successed :)' + Style.reset)

    if DELETE_OPENSTACK_INSTANCE:
        logger.info('CREATE OPENSTACK INSTANCE MODE')
        # global ENV
        if args.dc == 'tlv':
            ENV = 'DEV'

        if not args.user:
            logger.error('user dosent inserted')
            print_help_delete_openstack('** You must enter a User **')
            sys.exit(1)

        if not args.dc:
            logger.error('Dc dosent inserted')
            print_help_delete_openstack('** You must enter a DC **')
            sys.exit(1)
        if args.hostname == 'alpha' and args.dc not in ('Datacenter_1', 'Datacenter_2'):
            logger.error('Alpha segment exist only in Datacenter_1, Datacenter_2 datacente')
            print_help_delete_openstack(
                Colors.lightcyan + '** Alpha segment exist only in Datacenter_1, Datacenter_2 datacente **' + Style.reset)
            sys.exit(1)

        if not args.hostname:
            logger.error('Hostname dosent inserted')
            print_help_delete_openstack('** You must enter a hostnmae **')
            sys.exit(1)

        if ENV == 'DEV':
            unicorn_url = unicorn_url_dev
        else:
            verify_ad_auth()
            print(Colors.pink + 'Authentication successed :)' + Style.reset)

    if DELETE:
        if not (args.serial and args.hostname and args.user and args.dc):
            print_help_delete(
                Colors.lightcyan + "You must insert: --dc <DC> --serial <SERIAL> --hostname <HOSTNAME> --user <USER>" + Style.reset)
            sys.exit(1)
        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)

    if DELETE_VM:
        if not (args.hostname and args.user and args.dc):
            print_help_delete_vm(
                Colors.lightcyan + "You must insert: --dc <DC> --hostname <HOSTNAME> --user <USER>" + Style.reset)
            sys.exit(1)
        verify_ad_auth()
        print(Colors.pink + 'Authentication successed :)' + Style.reset)


@with_goto
def send_request_api():
    data = vars(args)

    # get requests
    if SHOW:

        if args.jobs or (args.user and len(sys.argv) == 4):
            show_status_mode()
        if args.dc == []:
            response = get_request('datacenter')
            print('\nDatacenters list:\n\t')
            print('\t' + '\n\t'.join(set(response['dc'])))
            sys.exit(0)

        # show dcs list
        if args.images:
            response = get_request('image')
            print(Style.bold + Style.underline + '\nImage list:\n' + Style.reset)
            for_display = [x for x in response['image'] if x.endswith('gold')]
            # print('\n'.join(set(response['os'])))
            print('\n\t'.join(set(for_display)))

        if args.profiles:
            # show profile values
            result = get_request('profile/{}'.format(args.profiles[0]))
            if result == 'null':
                print(Colors.red + "Error: {} profile dosent exist".format(args.profiles[0]) + Style.reset)
            else:
                print('\n{} profile values:\n\t'.format(args.profiles[0]))
                if result.get('profile', None):
                    r = result.get('profile')
                else:
                    r = result.get(args.profiles[0], '')

                print(json.dumps(result, indent=4, sort_keys=True))
                print(json.dumps(r, indent=4, sort_keys=True))

        # shoe profile list
        elif args.profiles == []:
            response = get_request('profile')
            print('\nprofile list:\n')
            print('\t' + '\n\t'.join(set(response['profile'])))

        if args.flavors:
            # show profile values
            result = get_request('flavor/{}'.format(args.flavors[0]))
            if result == 'null':
                print(Colors.red + "Error: {} flavor dosent exist".format(args.flavors[0]) + Style.reset)
            else:
                print(Colors.red + '\nError: {} flavor values:\n\t'.format(args.flavors[0]) + Style.reset)
                if result.get('flavor', None):
                    r = result.get('flavor')
                else:
                    r = result.get(args.flavors[0], '')
                print(json.dumps(r, indent=4, sort_keys=True))

        # shoe profile list
        elif args.flavors == []:
            response = get_request('flavor')
            print('\nflavors list:\n')
            lst_data = []
            lst_flavors = sorted(response['flavor'], key=lambda k: k['TotalMemory'])
            for flavor in lst_flavors:
                lst_data.append(
                    (flavor.get('Flavor Name', ''), flavor.get('DiskSize', ''), flavor.get('TotalDisks', ''),
                     flavor.get('TotalMemory'), flavor.get('CpuCores', '20'), flavor.get('Num_Of_Boot_Disks'),
                     flavor.get('Boot_Disk_Raid'),
                     flavor.get('Data_Disk_Raid'), flavor.get('is_ssd', 'False'), flavor.get('is_nvme', 'False'),
                     str(flavor.get('Price', '')) + '$'))

            print(tabulate.tabulate(lst_data,
                                    headers=['Flavor Name', 'DiskSize', 'TotalDisks', 'TotalMemory', 'Cpu_Cores',
                                             'Num_Of_Boot_Disks', 'Boot_Disk_Raid', 'Data_Disk_Raid',
                                             'Is SSD', 'Is NVme', 'Price'], tablefmt="grid"))

        elif args.flavor_capacity == [] or args.flavor_capacity:
            response = get_request('flavor_cpacity')
            response = response['flavor']

            print('\nflavors list:\n')
            lst_data = []
            print(args.flavor_capacity)
            lst_flavors = sorted(response, key=lambda k: k['TotalMemory'])
            for flavor in lst_flavors:
                # lst_data.append(
                if args.flavor_capacity != []:
                    if args.flavor_capacity[0] != flavor.get('Flavor_Name', ''):
                        continue
                l = [flavor.get('Flavor_Name', ''),
                     flavor.get('DiskSize', ''),
                     flavor.get('TotalDisks', ''),
                     flavor.get('TotalMemory'),
                     flavor.get('CpuCores', '20'),
                     flavor.get('Num_Of_Boot_Disks'),
                     flavor.get('Boot_Disk_Raid'),
                     flavor.get('Data_Disk_Raid'),
                     flavor.get('is_ssd', 'False'),
                     flavor.get('is_nvme', 'False'),
                     '$' + str(flavor.get('Price_int', '')),
                     ]
                # )

                if args.dc:
                    dc = args.dc
                    l.append(str(flavor.get(dc, '0')))

                else:
                    dcs = ['Datacenter_1', 'Datacenter_2', 'Datacenter_3', 'Datacenter_4', 'Datacenter_5', 'Datacenter_6']

                    for dc in dcs:
                        l.append(str(flavor.get(dc, '0')))

                lst_data.append(l)

            headers = ['Flavor Name', 'DiskSize', 'TotalDisks', 'TotalMemory', 'Cpu_Cores',
                       'Num_Of_Boot_Disks', 'Boot_Disk_Raid', 'Data_Disk_Raid',
                       'Is SSD', 'Is NVme', 'Price']
            if args.dc:
                dc = args.dc
                headers.append(dc)

            else:
                dcs = ['Datacenter_1', 'Datacenter_2', 'Datacenter_3', 'Datacenter_4', 'Datacenter_5', 'Datacenter_6']

                for dc in dcs:
                    headers.append(dc)

            print(tabulate.tabulate(lst_data,
                                    headers=headers, tablefmt="grid"))

        elif args.flavors_openstack:
            response = get_request('openstack/flavors', 'dev')
            print('\nflavor list:\n')
            print('\t' + '\n\t'.join(set(response['flavor'])))
            sys.exit(0)
        elif args.images_openstack:
            response = get_request('openstack/images', 'dev')
            print('\nimage list:\n')
            print('\t' + '\n\t'.join(set(response['images'])))
            sys.exit(0)
        elif args.projects_openstack:
            response = get_request('openstack/projects', 'dev')
            print('\nproject list:\n')
            print('\t' + '\n\t'.join(set(response['projects'])))
            sys.exit(0)

    if RETRY:
        headers = {
            'Content-Type': "application/json",
            'Authorization': 'Bearer ' + args.access_token,
            'cache-control': "no-cache",
        }
        import requests
        try:

            data['Command'] = ' '.join(sys.argv)
            if args.reinstall:
                data['Reinstall_Server'] = "Yes"
                del data['reinstall']

            if args.skip_yum_update:
                data['Skip_Yum_Update'] = "Yes"
                del data['skip_yum_update']

            if args.skip_server_firmware:
                data['Skip_Server_Firmware'] = "Yes"
                del data['skip_server_firmware']
            data['Skip_Server_Firmware'] = "Yes"

            if args.force_poweroff:
                data['Force_PowerOFF'] = "Yes"
                del data['force_poweroff']

            if args.image:
                data['Image'] = args.image
                del data['image']

            if args.no_arista:
                data['Arista_Switch'] = "No"
                del data['no_arista']

            if args.sda_only:
                data['Sda_Only'] = "Yes"
                del data['sda_only']

            if args.hba_mode:
                data['Configure_RAID'] = "No"
                del data['hba_mode']

            payload = "{{{}}}".format(
                ','.join(['"{}":"{}"'.format(k, v) for k, v in data.items() if v]))

            result = requests.post(unicorn_url + 'retry', data=payload, headers=headers).json()
            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)
            # print(result['Error'])
            else:
                print(result.get('message', ''))
                my_uuid = result['uuid']
                del result['uuid']
                # print(sorted(result['tasks'][0]))
                # header = sorted(result['tasks'][0])
                header = ['DataCenter', 'Owner', 'Rack', 'SerialNo', 'hostname', 'Image']

                # rows = [x.values() for x in result['tasks']]
                rows = []
                for t in result['tasks']:
                    values = []
                    if type(t) is dict:
                        for key in header:
                            if key == 'Image':
                                values.append(t['OS'])
                                continue
                            values.append(t[key])
                        rows.append(values)
                print(tabulate.tabulate(rows, header, tablefmt="grid"))
                try:
                    print('Do you want to continue? (y or n) ', )
                    i, o, e = select.select([sys.stdin], [], [], 120)

                    if (i):
                        # print("You said", sys.stdin.readline().strip())
                        if sys.stdin.readline().strip().lower() in ['y', 'yes']:

                            result = requests.post(unicorn_url + 'install', data="{{\"uuid\":\"{}\"}}".format(my_uuid),
                                                   headers=headers).json()
                            if 'Error' in result.keys():
                                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                                sys.exit(1)

                        else:
                            cancell(my_uuid)
                    else:
                        cancell(my_uuid)
                except KeyboardInterrupt:
                    cancell(my_uuid)
                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue

                    data.append((j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('hostname'),
                                 j.get('job', {}).get('options').get('Owner'),
                                 j.get('job', {}).get('options').get('SerialNo'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were initialised for execution...\n')
                print(tabulate.tabulate(data, ['Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile', 'Serial',
                                               'Permalink'], tablefmt="grid"))
        # print(json.dumps(result['tasks'], indent=4, sort_keys=True))

        except Exception as e:
            print(e)
            sys.exit(1)

    # post request
    if CREATE:
        data['Command'] = ' '.join(sys.argv)
        if args.reinstall:
            data['Reinstall_Server'] = "Yes"
            del data['reinstall']

        if args.skip_yum_update:
            data['Skip_Yum_Update'] = "Yes"
            del data['skip_yum_update']

        if args.skip_server_firmware:
            data['Skip_Server_Firmware'] = "Yes"
            del data['skip_server_firmware']

        data['Skip_Server_Firmware'] = "Yes"

        if args.server_firmware:
            data['Skip_Server_Firmware'] = "No"
            del data['skip_server_firmware']

        if args.force_poweroff:
            data['Force_PowerOFF'] = "Yes"
            del data['force_poweroff']
        if args.image:
            data['Image'] = args.image
            del data['image']

        if args.no_arista:
            data['Arista_Switch'] = "No"
            del data['no_arista']

        if args.sda_only:
            data['Sda_Only'] = "Yes"
            del data['sda_only']
        if args.hw_type:
            data['hw_type'] = args.hw_type.title()

        if args.hba_mode:
            data['Configure_RAID'] = "No"
            del data['hba_mode']

        if not args.serial:
            data['Force_PowerOFF'] = "Yes"
        # if not args.flavor:
        #     data['flavor'] = args.flavor

        try:

            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            import requests
            payload = "{{{}}}".format(
                ','.join(['"{}":"{}"'.format(k, v) for k, v in data.items()]))  # convert to string the data
            
            logger.info("data = {}".format(payload))
            
            print('Validating parameters and looking for devices...')
            result = requests.post(unicorn_url + 'create', data=payload, headers=headers).json()

            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)
            # print(result['Error'])
            else:
                print(result.get('message', ''))
                my_uuid = result['uuid']
                del result['uuid']
                # print(sorted(result['tasks'][0]))
                header = sorted(result['tasks'][0])
                header = ['DataCenter', 'Owner', 'Rack', 'SerialNo', 'hostname', 'Image']

                # rows = [x.values() for x in result['tasks']]
                rows = []
                tmp_rack = result['tasks'][0]['Rack']
                if_same_rack = True
                for t in result['tasks']:
                    if tmp_rack != t['Rack']:
                        if_same_rack = False
                    if type(t) is dict:
                        values = []
                        for key in header:
                            if key == 'Image':
                                values.append(t['OS'])
                                continue
                            values.append(t[key])
                        # values = [t['DataCenter'],t['Owner'],t['Rack'],t['SerialNo'],t['hostname'],t['OS']]
                        rows.append(values)

                if if_same_rack and len(result['tasks']) != 1:
                    print(
                        Style.bold + Colors.yellow + 'Warrning: The installation will be executed on a single rack due to capacity limitation' + Style.reset)
                header = ['DataCenter', 'Owner', 'Rack', 'SerialNo', 'hostname', 'Image','boot_disk']
                print(tabulate.tabulate(rows, header, tablefmt="grid"))

                try:
                    print('Do you want to continue? (y or n) ', )
                    i, o, e = select.select([sys.stdin], [], [], 120)

                    if (i):
                        if sys.stdin.readline().strip().lower() in ['y', 'yes']:
                            # if input('Do you want to continue? ').lower() in ['y', 'yes']:

                            result = requests.post(unicorn_url + 'install', data="{{\"uuid\":\"{}\"}}".format(my_uuid),
                                                   headers=headers).json()
                            if result.get('Error', None):
                                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                                sys.exit(1)
                        else:
                            cancell(my_uuid)
                    else:
                        cancell(my_uuid)
                except KeyboardInterrupt:
                    cancell(my_uuid)

                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue
                    if j.get('message', None):
                        print(Colors.red + j.get('message', None) + Style.reset)
                        continue

                    data.append((j.get('id', ''),
                                 j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('hostname'),
                                 j.get('job', {}).get('options').get('Owner'),
                                 j.get('job', {}).get('options').get('SerialNo'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were initialised for execution...\n')
                print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile',
                                               'Serial',
                                               'Permalink'], tablefmt="grid"))
        # print(json.dumps(result['tasks'], indent=4, sort_keys=True))

        except Exception as e:
            print(e)
            sys.exit(1)

    # post request
    if CREATE_VM:
        data['Command'] = ' '.join(sys.argv)

        if args.skip_yum_update:
            data['skip_Yum_Update'] = "Yes"
            del data['skip_yum_update']

        if args.skip_dns_check:
            data['skip_DNS_Check'] = "Yes"
            del data['skip_dns_check']

        if args.image:
            data['profile'] = args.image
            del data['image']

        if args.annotations:
            data['Annotations'] = data['annotations']
            del data['annotations']

        try:

            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            import requests
            payload = "{{{}}}".format(
                ','.join(['"{}":"{}"'.format(k, v) for k, v in data.items()]))  # convert to string the data

            print('Validating parameters and looking for devices...')
            result = requests.post(unicorn_url + 'create_vmware', data=payload, headers=headers).json()
            
            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)
            # print(result['Error'])
            else:
                print(result.get('message', ''))
                my_uuid = result['uuid']
                del result['uuid']
                # print(sorted(result['tasks'][0]))
                header = sorted(result['tasks'][0])
                header = ['DataCenter', 'Hostname', 'Profile']

                rows = []
                for t in result['tasks']:
                    values = [t['DataCenter'], t['Hostname'], t['Profile']]
                    rows.append(values)

                header = ['DataCenter', 'Hostname', 'Profile']
                print(tabulate.tabulate(rows, header, tablefmt="grid"))

                try:
                    print('Do you want to continue? (y or n) ', )
                    i, o, e = select.select([sys.stdin], [], [], 120)

                    if (i):
                        if sys.stdin.readline().strip().lower() in ['y', 'yes']:
                            # if input('Do you want to continue? ').lower() in ['y', 'yes']:

                            result = requests.post(unicorn_url + 'install',
                                                   data="{{\"uuid\":\"{}\",\"mode\":\"vm\"}}".format(my_uuid),
                                                   headers=headers).json()
                            if result.get('Error', None):
                                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                                sys.exit(1)
                        else:
                            cancell(my_uuid)
                    else:
                        cancell(my_uuid)
                except KeyboardInterrupt:
                    cancell(my_uuid)

                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue
                    if j.get('message', None):
                        print(Colors.red + j.get('message', None) + Style.reset)
                        continue

                    data.append((j.get('id', ''),
                                 j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('hostname'),
                                 j.get('job', {}).get('options').get('Owner'),
                                 j.get('job', {}).get('options').get('SerialNo'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were initialised for execution...\n')
                print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile',
                                               'Serial',
                                               'Permalink'], tablefmt="grid"))
        # print(json.dumps(result['tasks'], indent=4, sort_keys=True))

        except Exception as e:
            print(e)
            sys.exit(1)

    # post request
    if CREATE_OPENSTACK_INSTANCE:
        data['Command'] = ' '.join(sys.argv)
        try:
            headers = {
                'Content-Type': "application/json",
                'cache-control': "no-cache",
                'Authorization': 'Bearer ' + args.access_token,

            }
            import requests
            payload = "{{{}}}".format(
                ','.join(['"{}":"{}"'.format(k, v) for k, v in data.items()]))  # convert to string the data

            print('Validating parameters and looking for devices...')

            result = requests.post(unicorn_url + 'create_openstack', data=payload, headers=headers).json()

            if result.get('Error', None):
                if 'Security group and network doesnt define' in result.get('Error'):
                    print(
                        "Security group and network doesnt define on the new cluster, starting installation on the old cluster")
                    goto.old_cluster_installation
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)
            # print(result['Error'])
            else:
                print(result.get('message', ''))
                my_uuid = result['uuid']
                del result['uuid']
                # print(sorted(result['tasks'][0]))
                header = sorted(result['tasks'][0])
                header = ['DataCenter', 'Hostname', 'Flavor']

                # rows = [x.values() for x in result['tasks']]
                rows = []
                for t in result['tasks']:
                    values = [t['DataCenter'], t['HostName'], t['Flavor'],t['f5_clone_server']]
                    rows.append(values)

                header = ['DataCenter', 'HostName', 'Flavor','f5_clone_server']
                print(tabulate.tabulate(rows, header, tablefmt="grid"))

                try:
                    print('Do you want to continue? (y or n) ', )
                    i, o, e = select.select([sys.stdin], [], [], 120)

                    if (i):
                        if sys.stdin.readline().strip().lower() in ['y', 'yes']:
                            # if input('Do you want to continue? ').lower() in ['y', 'yes']:
                            if ENV == 'DEV':
                                result = requests.post(unicorn_url + 'install_tlv',
                                                       data="{{\"uuid\":\"{}\",\"mode\":\"install_openstack\"}}".format(
                                                           my_uuid),
                                                       headers=headers).json()
                            else:
                                result = requests.post(unicorn_url + 'install',
                                                       data="{{\"uuid\":\"{}\",\"mode\":\"install_openstack\"}}".format(
                                                           my_uuid),
                                                       headers=headers).json()

                                print("{{\"uuid\":\"{}\",\"mode\":\"install_openstack\"}}".format(
                                    my_uuid))
                            if result.get('Error', None):
                                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                                sys.exit(1)
                        else:
                            # cancell(my_uuid)
                            print('You Cancelled this request')
                            sys.exit(0)
                    else:
                        print('You Cancelled this request')
                        sys.exit(0)
                except KeyboardInterrupt:
                    print('You Cancelled this request')
                    sys.exit(0)

                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue
                    if j.get('message', None):
                        print(Colors.red + j.get('message', None) + Style.reset)
                        continue

                    data.append((j.get('id', ''),
                                 j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('HostName'),
                                 j.get('job', {}).get('options').get('Flavor'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were initialised for execution...\n')
                print(
                    tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname',
                                             'Flavor',
                                             'Permalink'], tablefmt="grid"))
            # print(json.dumps(result['tasks'], indent=4, sort_keys=True))

        except Exception as e:
            print(e)
            sys.exit(1)

    # post request
    if DELETE:
        rows = []
        print('Servers to decomission:\n')
        for i, server in enumerate(args.hostname.split(',')):
            rows.append((args.dc, server, args.serial.split(',')[i]))

        print(tabulate.tabulate(rows, ['Datacenter', 'Hostname', 'Serial'], tablefmt="grid"))

        input_client = input('Do you want to continue? (y or n) ')
        if input_client.lower() in ['y', 'yes']:

            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            import requests

            # result = requests.post(unicorn_url + 'delete', data=json.dump(data),headers=headers).json()
            result = requests.post(unicorn_url + 'delete',
                                   data="{{\"user\":\"{}\",\"DataCenter\":\"{}\",\"hostname\":\"{}\",\"serial\":\"{}\"}}".format(
                                       args.user, args.dc, args.hostname, args.serial), headers=headers).json()
            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)

            data = []
            logger.info(result['tasks'])
            for j in result['tasks']:

                if j.get('error', None):
                    data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                    continue

                data.append(((j.get('id', '')),
                             j.get('status', ''),
                             j.get("date-started", {}).get('date', ''),
                             j.get('job', {}).get('options').get('DataCenter'),
                             j.get('job', {}).get('options').get('HostName'),
                             j.get('job', {}).get('options').get('SerialNo'),
                             j.get('permalink')
                             ))
            print('\nAll jobs were sending to execution...\n')
            print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Serial',
                                           'Permalink'], tablefmt="grid"))

        else:
            print('You Cancelled this request')
            sys.exit(0)

    if DELETE_VM:
        rows = []
        print('Servers to decomission:\n')
        for i, server in enumerate(args.hostname.split(',')):
            rows.append((args.dc, server))

        print(tabulate.tabulate(rows, ['Datacenter', 'Hostname'], tablefmt="grid"))

        input_client = input('Do you want to continue? (y or n) ')
        if input_client.lower() in ['y', 'yes']:

            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            import requests

            # result = requests.post(unicorn_url + 'delete', data=json.dump(data),headers=headers).json()
            result = requests.post(unicorn_url + 'delete_vm',
                                   data="{{\"user\":\"{}\",\"DataCenter\":\"{}\",\"hostname\":\"{}\"}}".format(
                                       args.user, args.dc, args.hostname), headers=headers).json()
            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)

            data = []
            logger.info(result['tasks'])
            for j in result['tasks']:

                if j.get('error', None):
                    data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                    continue

                data.append(((j.get('id', '')),
                             j.get('status', ''),
                             j.get("date-started", {}).get('date', ''),
                             j.get('job', {}).get('options').get('DataCenter'),
                             j.get('job', {}).get('options').get('HostName'),
                             j.get('permalink')
                             ))
            print('\nAll jobs were sending to execution...\n')
            print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname',
                                           'Permalink'], tablefmt="grid"))

        else:
            print('You Cancelled this request')
            sys.exit(0)

    if DELETE_OPENSTACK_INSTANCE:
        if args.dc == 'Virginia':
            lpnova_command = "lpnova --delete -D {} -U {} ".format(args.dc.lower(), args.user)
            if args.hostname:
                hosts = args.hostname.split(',')
                for i in hosts:
                    lpnova_command += " -H {}".format(i)
            token = subprocess.Popen('sudo ssh svvr-mng77 -C cat /lpnova/liveperson/util/settings/.token',
                                     shell=True, stdout=subprocess.PIPE)
            token, err = token.communicate()
            token = token.decode('utf-8')
            token = token.strip('\n')
            logger.info(lpnova_command)
            lpnova_command += "  --token={}".format(token)
            process = subprocess.Popen('sudo ssh svvr-mng77 -C {}'.format(lpnova_command), shell=True)
            stdoutdata, stderrdata = process.communicate()
            print(process.returncode)
        else:
            rows = []
            print('Servers to decomission:\n')
            print(args.dc)  # todo: remove
            for i, server in enumerate(args.hostname.split(',')):
                rows.append((args.dc, server))

            print(tabulate.tabulate(rows, ['Datacenter', 'Hostname'], tablefmt="grid"))

            input_client = input('Do you want to continue? (y or n) ')
            if input_client.lower() in ['y', 'yes']:

                headers = {
                    'Content-Type': "application/json",
                    'cache-control': "no-cache",
                }
                import requests

                # result = requests.post(unicorn_url + 'delete', data=json.dump(data),headers=headers).json()
                if args.force:
                    result = requests.post(unicorn_url + 'delete_openstack',
                                           data="{{\"user\":\"{}\",\"DataCenter\":\"{}\",\"hostname\":\"{}\",\"focedelete\":\"FORCE\"}}".format(
                                               args.user, args.dc, args.hostname), headers=headers).json()
                else:
                    result = requests.post(unicorn_url + 'delete_openstack_tlv',
                                           data="{{\"user\":\"{}\",\"DataCenter\":\"{}\",\"hostname\":\"{}\"}}".format(
                                               args.user, args.dc, args.hostname), headers=headers).json()
                if result.get('Error', None):
                    print(Colors.red + "Error: " + result['Error'] + Style.reset)
                    sys.exit(1)

                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue

                    data.append(((j.get('id', '')),
                                 j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('HostName'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were sending to execution...\n')
                print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname',
                                               'Permalink'], tablefmt="grid"))

            else:
                print('You Cancelled this request')
                sys.exit(0)

    if CLONE_OPENSTACK_INSTANCE:
        # post request
        data['Command'] = ' '.join(sys.argv)
        try:
            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            if (not args.lprole and not args.lpcluster) or not args.flavor:
                flavor, lprole, lpcluster = get_facter_from_zoom(data.get('cloneserver'),True)
                data['flavor'] = flavor if not args.flavor else data['flavor']
                data['lprole'] = flavor if not args.flavor else data['lprole']
                data['lpcluster'] = flavor if not args.flavor else data['lpcluster']

            import requests
            payload = "{{{}}}".format(
                ','.join(['"{}":"{}"'.format(k, v) for k, v in data.items()]))  # convert to string the data

            print('Validating parameters and looking for devices...')
            logger.info('url => {}'.format(unicorn_url))
            result = requests.post(unicorn_url + 'create_clone_openstack', data=payload, headers=headers).json()

            if result.get('Error', None):
                if 'Security group and network doesnt define' in result.get('Error'):
                    print(
                        "Security group and network doesnt define on the new cluster, starting installation on the old cluster")
                    goto.old_cluster_clone
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)
            # print(result['Error'])
            else:
                print(result.get('message', ''))
                my_uuid = result['uuid']
                del result['uuid']
                # print(sorted(result['tasks'][0]))
                header = sorted(result['tasks'][0])
                header = ['DataCenter', 'CloneServer', 'Hostname']

                # rows = [x.values() for x in result['tasks']]
                rows = []
                for t in result['tasks']:
                    values = [t['DataCenter'], t['HostNameCloneFrom'], t['HostName']]
                    rows.append(values)

                print(tabulate.tabulate(rows, header, tablefmt="grid"))

                try:
                    print('Do you want to continue? (y or n) ', )
                    i, o, e = select.select([sys.stdin], [], [], 120)

                    if (i):
                        if sys.stdin.readline().strip().lower() in ['y', 'yes']:
                            # if input('Do you want to continue? ').lower() in ['y', 'yes']:

                            result = requests.post(unicorn_url + 'install_tlv',
                                                   data="{{\"uuid\":\"{}\",\"mode\":\"clone_openstack\"}}".format(
                                                       my_uuid),
                                                   headers=headers).json()
                            if result.get('Error', None):
                                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                                sys.exit(1)
                        else:
                            # cancell(my_uuid)
                            print('You Cancelled this request')
                            sys.exit(0)
                    else:
                        # cancell(my_uuid)
                        print('You Cancelled this request')
                        sys.exit(0)
                except KeyboardInterrupt:
                    # cancell(my_uuid)
                    print('You Cancelled this request')
                    sys.exit(0)

                data = []
                logger.info(result['tasks'])
                for j in result['tasks']:

                    if j.get('error', None):
                        data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                        continue
                    if j.get('message', None):
                        print(Colors.red + j.get('message', None) + Style.reset)
                        continue

                    data.append((j.get('id', ''),
                                 j.get('status', ''),
                                 j.get("date-started", {}).get('date', ''),
                                 j.get('job', {}).get('options').get('DataCenter'),
                                 j.get('job', {}).get('options').get('HostNameCloneFrom'),
                                 j.get('job', {}).get('options').get('HostName'),
                                 j.get('permalink')
                                 ))
                print('\nAll jobs were initialised for execution...\n')
                print(
                    tabulate.tabulate(data,
                                      ['JobID', 'Status', 'Date Started', 'Datacenter', 'Clone Server', 'Hostname',
                                       'Permalink'], tablefmt="grid"))
            # print(json.dumps(result['tasks'], indent=4, sort_keys=True))

        except Exception as e:
            print(e)
            sys.exit(1)

    if FIRMWARE_UPDATE:
        rows = []
        print('Servers to update:\n')
        for i, serial in enumerate(args.serial.split(',')):
            rows.append((args.dc, serial))

        print(tabulate.tabulate(rows, ['Datacenter', 'Serial'], tablefmt="grid"))
        print(Colors.lightcyan + 'Please make sure the server is powered off' + Style.reset)

        input_client = input('Do you want to continue? (y or n) ')
        if input_client.lower() in ['y', 'yes']:

            headers = {
                'Content-Type': "application/json",
                'Authorization': 'Bearer ' + args.access_token,
                'cache-control': "no-cache",
            }
            import requests

            # result = requests.post(unicorn_url + 'delete', data=json.dump(data),headers=headers).json()
            result = requests.post(unicorn_url + 'firmware_update',
                                   data="{{\"user\":\"{}\",\"DataCenter\":\"{}\",\"serial\":\"{}\"}}".format(
                                       args.user, args.dc, args.serial), headers=headers).json()
            if result.get('Error', None):
                print(Colors.red + "Error: " + result['Error'] + Style.reset)
                sys.exit(1)

            data = []
            logger.info(result['tasks'])
            for j in result['tasks']:

                if j.get('error', None):
                    data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                    continue

                data.append(((j.get('id', '')),
                             j.get('status', ''),
                             j.get("date-started", {}).get('date', ''),
                             j.get('job', {}).get('options').get('DataCenter'),
                             j.get('job', {}).get('options').get('SerialNo'),
                             j.get('permalink')
                             ))
            print('\nAll jobs were sending to execution...\n')
            print(tabulate.tabulate(data, ['JobID', 'Status', 'Date Started', 'Datacenter', 'Serial',
                                           'Permalink'], tablefmt="grid"))

        else:
            print('You Cancelled this request')
            sys.exit(0)

def cancell(my_uuid):
    print(Colors.red + 'Error: Timeout Your request Cancelled' + Style.reset)

    headers = {
        'Content-Type': "application/json",
        'Authorization': 'Bearer ' + args.access_token,
        'cache-control': "no-cache",
    }
    result = requests.post(unicorn_url + 'cancel', data="{{\"uuid\":\"{}\"}}".format(my_uuid),
                           headers=headers).json()
    if result.get('Error', None):
        print(Colors.red + "Error: " + result['Error'] + Style.reset)
    print('You Cancelled this request')
    sys.exit(0)


def show_status_mode():
    response = requests.request("GET", unicorn_url + 'show_jobs/{}'.format(args.user))
    if response.status_code != 200:
        print(Colors.red + 'Error: The service is down, please be in tuch with CloudOps team' + Style.reset)
        sys.exit(1)
    result = json.loads(response.text)
    data = []
    for j in result['jobs']:
        if type(j) is list:
            j = j[0]
        # print(j)

        data.append((j.get('id'), j.get('job', {}).get('name'), j.get('status'), j.get('date-started', {}).get('date'),
                     j.get('job', {}).get('options', {}).get('DataCenter'),
                     j.get('job', {}).get('options', {}).get('hostname',
                                                             j.get('job', {}).get('options', {}).get('HostName')),
                     j.get('job', {}).get('options', {}).get('Owner'),
                     j.get('job', {}).get('options', {}).get('SerialNo'), j.get('permalink'),
                     j.get('step'), '[{fill}{space}] {step}\{total}'.format(fill='#' * j.get('step_index'),
                                                                            space=' ' * (j.get('total_step') - j.get(
                                                                                'step_index')),
                                                                            step=j.get('step_index'),
                                                                            total=j.get('total_step'))
                     ))

    # print("{}:".format(j.get('id')))
    # pbar = tqdm(range(j.get('total_step')))
    # step = j.get('step_index')
    # pbar.update(step)
    # pbar.refresh()
    # print('-----------------------------------------------------------------')

    print(tabulate.tabulate(data,
                            ['JobID', 'Job Name', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile',
                             'Serial',
                             'Permalink', 'Step', 'Bar'],
                            tablefmt="grid"))


def show_status_mode_old():
    response = requests.request("GET", unicorn_url + 'status_jobs/{}'.format(args.user))
    if response.status_code != 200:
        print(Colors.red + 'Error: The service is down, please be in tuch with CloudOps team' + Style.reset)
        sys.exit(1)
    result = json.loads(response.text)
    data = []
    for j in result['jobs']:
        if type(j) is list:
            j = j[0]
        # print(j)
        data.append((j.get('id', ''), j.get('status', ''), j.get('date-started', {}).get('date', ''),
                     j.get('job', {}).get('options', {}).get('DataCenter', ''),
                     j.get('job', {}).get('options', {}).get('hostname', ''),
                     j.get('job', {}).get('options', {}).get('Owner', ''),
                     j.get('job', {}).get('options', {}).get('SerialNo', ''), j.get('permalink', '')))
    print(tabulate.tabulate(data,
                            ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile', 'Serial',
                             'Permalink'],
                            tablefmt="grid"))


def show_status_mode_old():
    import time
    import curses
    count = 1
    stdscr = curses.initscr()
    while count > 0:
        # print(count)
        try:
            stdscr.clear()
            logger.info('Request for {}'.format(args.user))
            # result = get_request('status_jobs/{}'.format(args.user))

            response = requests.request("GET", unicorn_url + 'status_jobs/{}'.format(args.user))
            result = json.loads(response.text)
            if result.get('Error', None):
                logger.error(result.get('Error'))
                print(Colors.red + result.get('Error') + Style.reset)
                # print(result.get('Error'))
                os.system('stty sane')
                sys.exit(1)
            else:
                logger.info(result['jobs'])
                result = result['jobs']

            data = []
            for j in result:
                if type(j) is list:
                    j = j[0]
                if j.get('error', None):
                    data.append(('Error: {}'.format(j.get('message')), '', '', '', '', '', ''))
                    continue
                # if str(datetime.datetime.today().strftime('%Y-%m-%d')) not in j.get('date-started',{}).get('date',''):
                #     continue

                data.append((j.get('id', ''), j.get('status', ''), j.get('date-started', {}).get('date', ''),
                             j.get('job', {}).get('options', {}).get('DataCenter', ''),
                             j.get('job', {}).get('options', {}).get('hostname', ''),
                             j.get('job', {}).get('options', {}).get('Owner', ''),
                             j.get('job', {}).get('options', {}).get('SerialNo', ''), j.get('permalink', '')))

            stdscr.addstr(0, 0,
                          tabulate.tabulate(data,
                                            ['JobID', 'Status', 'Date Started', 'Datacenter', 'Hostname', 'Profile',
                                             'Serial',
                                             'Permalink'],
                                            tablefmt="grid") + '\nLast Update: {}\nPress Ctrl-C to exit\n'.format(
                              datetime.datetime.now()))
            stdscr.refresh()
            # sys.stdout.flush()
            time.sleep(10)
            print('\n')
        except Exception as e:
            logger.error(e)
            os.system('stty sane')
            sys.exit(1)

    os.system('stty sane')


def verify_ad_auth():
    if not args.user:
        user = input("Enter your LPDOMAIN username: ")
        args.user = user
    else:
        user = args.user
    password = getpass.getpass("Enter your LPDOMAIN password: ")

    # Authentication
    headers = {'Content-Type': 'application/json'}
    data = {"username": user, "password": password}
    print(unicorn_url)  # todo: remove
    api = Api(unicorn_url, headers=headers)

    result = api.post_data('authentication', data=json.dumps(data))
    if result:
        if 'Error' not in result.keys():
            parser.add_argument('--access_token')
            args.access_token = result.get('access_token')
            return True
        else:
            print(Colors.red + result.get('Error') + Style.reset)
            # print(result.get('Error'))
            sys.exit(1)
    else:
        print(Colors.red + 'Error: The service is down, please be in tuch with CloudOps team' + Style.reset)
        # print('Error: Somthing was worng')
        sys.exit(1)


def get_request(option, env='prod'):
    if env == 'dev':
        global unicorn_url
        unicorn_url = unicorn_url_dev
    api = Api(unicorn_url)
    response = api.get_Response_base(option)
    if 'Error' in response.keys():
        [print(k + ": " + v) for k, v in response.items()]
        sys.exit(1)
    return response


def create_rotating_log(path):
    """
    Creates a rotating log
    """
    global logger
    logger = logging.getLogger("Rotating Log")
    logger.setLevel(logging.INFO)
    # add a rotating handler

    handler = RotatingFileHandler(path, maxBytes=1000000000, mode='a',
                                  backupCount=5)
    formatter = logging.Formatter(
        '%(asctime)s program_name [%(process)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def get_facter_from_zoom(hostname, openstack=False):
    command = 'zoom -g -H {} -f flavor,owner,lprole,lpcluster -j'.format(hostname)
    try:
        output = os.popen(command).read()
        if output != '[\n\n]\n' and output != '':
            jOutput = json.loads(output)  # convert the string to hash
            for node in jOutput:
                if '_id' in node:
                    flavor = node.get('facts', {}).get('flavor', '')
                    owner = node.get('facts', {}).get('owner', '')
                    lprole = node.get('facts', {}).get('lprole', '')
                    lpcluster = node.get('facts', {}).get('lpcluster', '')
        if not flavor and not owner and not openstack:
            logger.error('hostname {}: Name or service not known'.format(hostname))
            print(
                Colors.red + 'hostname {}: Name or service not known, or there aren\'t the facts for this host (flavor, owner..)'.format(
                    hostname) + Style.reset)
            sys.exit(1)
        if openstack:
            return flavor, lprole, lpcluster
        return flavor, owner, lprole, lpcluster
    # return False

    except IOError as e:
        print(red + str(e) + end)
        logging.error(str(e))


class Api:
    def __init__(self, base, user=None, pwd=None, headers={"Accept": "application/json"}):
        self.__base = base
        self.__user = user
        self.__pwd = pwd
        self.__headers = headers

    # self.headers = {"Accept": "application/json"}
    # self.__proxies = self.__get_proxy()

    def Get_Response(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, auth=(self.__user, self.__pwd), headers=self.__headers, verify=False)
            #   ,proxies=self.__proxies)
            if response.status_code != 200:
                logger.error(response.json()['error']['message'])
                return False  # sys.exit(1)

        except requests.exceptions.RequestException as e:
            logger.error(e)
            print('Error: Unable to connect with api')
            return False  # sys.exit(1)

        return response.json()

    def get_Response_base(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, headers=self.__headers, verify=False)
            #   ,proxies=self.__proxies)
            if response.status_code != 200:
                return json.loads(response.text)
            return response.json()

        except requests.exceptions.RequestException as e:
            logger.error(e)
            return {'Error': 'Unable to connect with api'}

        except Exception as e:
            logger.error(e)
            return {'Error': 'Unable to connect with api'}
        return response.json()

    def post_data(self, option, data):
        url = ''.join([self.__base, option])
        logger.info(url)
        try:
            response = requests.post(url, auth=(self.__user, self.__pwd), headers=self.__headers, data=str(data))
            # ,proxies=self.__proxies).json()
            # if 'error' in response.keys():
            if response.status_code != 201:
                response = response.json()
                logger.error(response)
                return response
            # sys.exit(1)

            return response.json()

        except requests.exceptions.RequestException as e:
            logger.error(e)
            # print('Error: Unable to connect with api')
            return False
        # sys.exit(1)

        except Exception as e:
            logger.error(e)
            return False

        return response


class Style:
    '''Colors class:
    reset all colors with colors.reset
    two subclasses fg for foreground and bg for background.
    use as colors.subclass.colorname.
    i.e. colors.fg.red or colors.bg.green
    also, the generic bold, disable, underline, reverse, strikethrough,
    and invisible work with the main class
    i.e. colors.bold
    '''
    reset = '\033[0m'
    bold = '\033[01m'
    disable = '\033[02m'
    underline = '\033[04m'
    reverse = '\033[07m'
    strikethrough = '\033[09m'
    invisible = '\033[08m'
    blink = '\33[5m'


class Colors:
    black = '\033[30m'
    red = '\033[31m'
    green = '\033[32m'
    orange = '\033[33m'
    blue = '\033[34m'
    purple = '\033[35m'
    cyan = '\033[36m'
    lightgrey = '\033[37m'
    darkgrey = '\033[90m'
    lightred = '\033[91m'
    lightgreen = '\033[92m'
    yellow = '\033[93m'
    lightblue = '\033[94m'
    pink = '\033[95m'
    lightcyan = '\033[96m'


if __name__ == "__main__":
    try:
        # log_file = "/home/sysadmin/unicorn/logs/unicorn.log"
        # log_file = "/home/sysadmin/unicorn_all/logs/unicorn.log"
        log_file = "unicorn.log"
        # log_file = "/root/unicorn/unicorn.log"
        create_rotating_log(log_file)
        init_args()
        logger.info('command => {}'.format(' '.join(sys.argv)))
        verify_flags()
        send_request_api()
        sys.exit(0)
    except KeyboardInterrupt:
        print('\nYou missing an awsome experiences with fairies and unicorn(s)  ')
        os.system('stty sane')
        sys.exit(1)
