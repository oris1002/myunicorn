import os
import sys
import time
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/workflow', '') + '/core'
root_dir = os.path.dirname(os.path.realpath(__file__)).replace('/workflow', '')
sys.path.append(home_dir)
from rundeck_api import Rundeck_api
from core.utils import read_yaml,get_dc
def get_lst_steps(jobid):
	lst_steps = list()
	rundeck = Rundeck_api()
	result = rundeck.get_job_workflow(jobid)
	if result:
		for step in result.get('workflow',[[]]):
			name = step.get('jobref',{'name': step.get('description')}).get('name')
			if name:
				lst_steps.append(name)
		return lst_steps
	return False
	
import yaml
def build_steps_4_jobs():
	data = read_yaml((root_dir + '/variables/variables.yaml'))
	
	tlv = data['tlv_rundeck']['jobs_id']
	prod = data['rundeck']['jobs_id']
	# base_dir = os.path.realpath(__file__).replace(os.path.basename(__file__), '')
	base_dir = '/var/unicorn_states/workflow'
	
	if not os.path.isdir(base_dir):
		os.mkdir(base_dir)
		
	print('base_dir = {}'.format(base_dir))
	#create dir
	if not os.path.isdir('{}/Telaviv'.format(base_dir)):
		os.mkdir('{}/Telaviv'.format(base_dir))
	if not os.path.isdir('{}/Prod'.format(base_dir)):
		os.mkdir('{}/Prod'.format(base_dir))
		
	if get_dc() =='Tlv':
		print("DC = TLV")
		for job,jobid in tlv.items():
			if jobid != 'xxx':
				print('job -- {} --'.format(job))
				with open('{}/Telaviv/{}.yaml'.format(base_dir,job),'w') as f:
					result = get_lst_steps(jobid)
					if result:
						f.write(yaml.dump(result))
	else:
		print("DC = Prod")
		for job,jobid in prod.items():
			if jobid != 'xxx':
				print('job -- {} --'.format(job))
				with open('{}/Prod/{}.yaml'.format(base_dir,job),'w') as f:
					result = get_lst_steps(jobid)
					if result:
						f.write(yaml.dump(result))


while True:
    try:
        build_steps_4_jobs()
        time.sleep(86400) # once a day
    except Exception as e:
        print(e)
        exit(1)
