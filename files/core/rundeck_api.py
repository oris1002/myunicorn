#!/usr/bin/env python3

import requests
import subprocess
import urllib3
from api import Api
urllib3.disable_warnings()
import logger
import json
from utils import read_yaml,get_dc
import os
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')

# def get_dc():
#     # Check on which DC i'm running, in order to run locally
#     print('Get DC')
#     p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
#     dc_name, err = p.communicate()
#     dc_name = dc_name.decode('utf-8')
#     dc_name = dc_name.split('\n')[1].split('=')[1]
#     # dict_fields['datacenter'] = dc_name.title()
#     return dc_name.title()
#     print('Env DC =>' + str(dc_name.title()))


class Rundeck_api():
    def __init__(self):
        self._get_vars()
        # self.base_url = "https://va-rundeck.int.liveperson.net/api/21"
        self.__headers = {
            'Accept': "application/json",
            'Content-Type': "application/json",
            'X-Rundeck-Auth-Token': self._token,
            'cache-control': "no-cache",
        }

    def _get_vars(self):
        self._dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
        if type(self._dict_fields) is not dict:
            return False
        
        if get_dc() == 'Tlv':
            self._token = self._dict_fields['tlv_rundeck']['rundeck_token']
            self.base_url = "https://" + self._dict_fields['tlv_rundeck']['rundeck_vip'] + '/api/34'
            self._jobs_Ids = self._dict_fields['tlv_rundeck']['jobs_id']
            return True
        else:
            self._token = self._dict_fields['rundeck']['rundeck_token']
            self.base_url = "https://" + self._dict_fields['rundeck']['rundeck_vip'] +'/api/34'
            self._jobs_Ids = self._dict_fields['rundeck']['jobs_id']
        print("rundeck url = > {}".format(self.base_url))
        return True

    def _run_job(self,job_id,options,user):
        # run rundeck job by job id
        action = 'executions'
        url = "{}/job/{}/{}".format(self.base_url,job_id,action)
        # payload = ""
        
        logger.logging.info('URL => ' + url)
        payload = json.dumps({"options":options,"asUser":user})
        
        response = requests.request("POST", url, data=payload, headers=self.__headers)
        # todo: add check failed
        response = response.json()
        logger.logging.info(response.get('message', response.get('text')))
        return response
    
    def run_job(self,mode,options,user):
        # job_id = self._dict_fields['rundeck']['jobs_id'][mode]
        job_id = self._jobs_Ids[mode]
        
        return self._run_job(job_id,options,user)

        
    def get_job_workflow(self,jobid):
        url = '{}/job/{}/workflow'.format(self.base_url,jobid)
        print(url)
        response = requests.request("GET", url, headers=self.__headers)
        if response.status_code == 200:
            response = response.json()
            logger.logging.info(response.get('message', response))
            return response
        else:
            return False
        
    def check_job_status(self,job_id):
        action = 'execution'
        url = "{}/{}/{}".format(self.base_url,action,job_id)
        response = requests.request("GET", url, headers=self.__headers)
        # todo: add check failed
        if response.status_code == 200:
            response = response.json()
            logger.logging.info(response.get('message', response))
            return response
        else:
            return False
    # def get_running(self,jobID):
    #     url = 'https://va-rundeck.int.liveperson.net/api/21/job/{}/executions'.format(jobID)
    
    def get_output(self,job_id):
        action = 'execution'
        url = "{}/{}/{}/output".format(self.base_url,action,job_id)
        response = requests.request("GET", url, headers=self.__headers)
        # todo: add check failed
        if response.status_code == 200:
            response = response.json()
            logger.logging.info(response.get('message', response))
            return response
        else:
            return False
    def get_current_step(self,joid):
        
        mainJobId = self.check_job_status(joid)['job']['id']
        print('mainJobId = {}'.format(mainJobId))
        #get the name main job (install / clone....) by id
        job_name = (list(self._jobs_Ids.keys())[list(self._jobs_Ids.values()).index(mainJobId)])
        print('job_name = {}'.format(job_name))

        output = self.get_output(joid)
        i = -1
        while not output['entries'][i].get('stepctx'):
            i -=1
        stepctx = output['entries'][i].get('stepctx').split('/')[0]
        stepctx = int("".join(filter(lambda d: str.isdigit(d) or d == '.', stepctx)))
        print('stepctx = {}'.format(stepctx))

        base_dir = '/var/unicorn_states/workflow'
        if get_dc() == 'Tlv':
            dc = 'Telaviv'
        else:
            dc = 'Prod'
        steps = read_yaml('{}/{}/{}.yaml'.format(base_dir,dc,job_name))
        if int(stepctx)-1 > len(steps):
            return False
        return steps[int(stepctx)-1],len(steps),int(stepctx)
