import ldap
import sys
from flask import current_app as app


def verify_auth(username, password):
    """
       LDAP authentication
       """
    LDAP_SERVER = "ldap://domain.com/"
    LDAP_BASE = "DC=domain,DC=com"
    LDAP_USERNAME = '%s@domain.com' % username
    LDAP_PASSWORD = password
    GROUP = "CN=dnser,CN=Users,DC=domain,DC=com"



    try:
        l = ldap.initialize(LDAP_SERVER)
        search_filter = '(&(objectCategory=person)(objectClass=user)(memberOf:1.2.840.113556.1.4.1941:=%s))' % GROUP
        l.set_option(ldap.OPT_NETWORK_TIMEOUT, 5.0)
        l.set_option(ldap.OPT_REFERRALS, 0)
        l.simple_bind_s(LDAP_USERNAME, LDAP_PASSWORD)
        results = l.search_ext_s(LDAP_BASE, ldap.SCOPE_SUBTREE, search_filter)
        values = ','.join(str(v) for v in results)
        if LDAP_USERNAME in values:
            app.logger.info("Info: User {} successfully authenticated".format(username))
            return True
        else:
            app.logger.error("User {} authenticated".format(username))
            return False

    except ldap.NO_SUCH_OBJECT as e:
        # sys.stderr.write('%sn' % str(e))
        app.logger.error(e)
        return False
    except ldap.INVALID_CREDENTIALS as e:
        # sys.stderr.write('%sn' % str(e))
        app.logger.error(e)
        return False
    except Exception as e:  # some other error occurred
        # sys.stderr.write('%sn' % str(e))
        app.logger.error(e)
        return False
