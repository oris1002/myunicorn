import os
import sys
import subprocess
import random

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core', '')
sys.path.append(home_dir)
import validation as validation
from server_search import looking_4_devices
from rundeck_api import Rundeck_api
from flask import current_app as app
from dcim_api import DcimApi
import json
import uuid
import re
import requests
from utils import read_yaml
from get_functions import *

# read the variables
dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
'''
All POST functions
dict_fields - json of variable from yaml
dict_params - json of all flags from cli
'''


def get_dc():
    # Check on which DC i'm running, in order to run locally
    print('Get DC')
    p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    dict_fields['datacenter'] = dc_name.title()
    print('Env DC =>' + str(dc_name.title()))


def validation_all(data):
    app.logger.info('Check validation parameters')
    app.logger.info(str(data))
    try:
        # convert to right type
        # convert from string to boolean
        # convert string to digit
        for i in data:
            if data[i] in ['True', 'False']:
                data[i] = json.loads(data[i].lower())
            elif data[i] == 'None':
                data[i] = None
            elif type(data[i]) is not list:
                if data[i].isdigit():
                    data[i] = int(data[i])

        # if supply serial(s)
        if data.get('serial', None):
            if len(data.get('serial').split(',')) < data[
                'quantity_servers']:  # if not supply less then quantity servers
                app.logger.info('Error: Not enough serials were entered')
                return {'Error': 'Not enough serials were entered'}
            else:
                serials = data.get('serial').split(',')  # split the serials (,)
                app.logger.info('Serials inserted =>' + str(serials))
                data['serial'] = [s.upper() for s in serials]  # upper all inputs

        if validation.check_datacente(data['dc']):
            if data.get('hostname', None):  # if inserted hostnames
                if len(data.get('hostname').split(',')) != data[
                    'quantity_servers']:  # if not supply less then quantity servers
                    app.logger.error('Not enough hosnames were entered')
                    return {'Error': 'Not enough hosnames were entered'}
                else:
                    data['hostname'] = data.get('hostname').split(',')  # split the hostnames are inserted
                    for hostname in data['hostname']:
                        if data.get('segment') != 'alpha':
                            segment_symbol_input = hostname.split('-')[0][-1]  # the letter before - in name
                            tmp_hostname = hostname
                        else:  # if segment is alpha
                            if data['dc'] not in ['Virginia', 'Oakland']:
                                return {'Error': 'Alpha segment exist only in Virginia, Oakland datacenters'}

                            segment_symbol_input = hostname[0]  # the first letter alpha name convvention
                            segment_symbol_by_dc = {'Virginia': 's', 'Oakland': 'r'}
                            tmp_hostname = hostname.replace('a', segment_symbol_by_dc[data['dc']])

                        if not validation.validation_hostname(data['dc'], tmp_hostname, data.get('mode',
                                                                                                 'physical')):  # check valid hostname by dc
                            app.logger.error('{} hostname wrong convention hostname'.format(hostname))
                            return {'Error': 'wrong convention hostname'}
                        if validation.check_segment(dict_fields, data.get(
                                'segment')) != segment_symbol_input:  # check valid by segment
                            app.logger.error('{} hostname wrong convention hostname in segment symbol'.format(hostname))
                            return {'Error': 'wrong convention hostname in segment symbol'}

                        if not check_hostname(hostname) and data.get('Reinstall_Server',
                                                                     'No') != 'Yes':  # check if hostname is free
                            app.logger.error('{} hostname is already exist'.format(hostname))
                            return {'Error': '{} hostname is already exist'.format(hostname)}
            else:
                # create new hostnames
                global segment_symbol
                segment_symbol = validation.check_segment(dict_fields, data['segment'])
                if not segment_symbol:
                    app.logger.error('Invalid Segment')
                    return {'Error': 'Invalid Segment'}

            # check if profile and flavor are exist
            path = validation.check_profile_exist(data['profile'])
            flavor_path = validation.check_flavor_exist(data['flavor'])

            if path and flavor_path:
                # check validation and return json with all params
                dict_params = validation.validation_yaml(path, data['profile'], flavor_path)
                dict_params['flavor_name'] = data['flavor']
                app.logger.info('parameters of {} profile : {}'.format(data['profile'], dict_params))
                if data.get('Image', None):
                    dict_params['Image'] = data['Image']
                    app.logger.info('Overwrite Image from {} to {}'.format(dict_params['Image'], data['Image']))
            else:
                app.logger.error('No yaml file for this profile/flavor')
                return {'Error': 'No yaml file for this profile/flavor'}

            # if data.get('Image', None):
            #    dict_fields['Image'] = data.get('Image')

            # check if image is exist
            if 'nvme' in dict_params['flavor_name'].lower():
                if 'centos' in dict_params['Image'].lower():
                    dict_params['Image'] = 'centos7.5_nvme_gold'
                if 'ubuntu' in dict_params['Image'].lower():
                    dict_params['Image'] = 'ubuntu18.04.2_nvme_gold'

            if validation.validation_image(dict_params['Image']):
                if 'Owner' not in dict_params.keys():
                    dict_params['Owner'] = data['Owner']
                    app.logger.error('Please add Owner parameter in Yaml')
                    return {'Error': 'Please add Owner parameter in Yaml'}
            else:
                app.logger.error('{} Invalid image'.format(dict_params['Image']))
                return {'Error': '{} Invalid image'.format(dict_params['Image'])}

            return dict_params

        else:
            app.logger.error('{} Invalid Datacenter'.format(data['dc']))
            return {'Error': '{} Invalid Datacenter'.format(data['dc'])}

    except Exception as e:
        app.logger.error(e)
        return {'Error': e}


import socket


def check_hostname(hostname):
    # check if hostname is free
    try:
        ip = socket.gethostbyname(hostname)
        return False
    except socket.error:
        return True
    except Exception as e:
        print(e)
        return True


def get_sequence_number(arr, q):
    '''
    Search a sequence of flowing number in array of q length
    :param arr: array of numbers
    :param q: quntatiy of numbers sequence
    :return: return the first number in sequence
    '''
    app.logger.info('Search a sequence of folwing number in ' + str(q) + ' length')
    if q == 1:
        return arr[0]
    count = 1
    start = arr[0]
    for i in range(len(arr) - 1):
        if arr[i] + 1 == arr[i + 1]:
            count += 1
        else:
            start = arr[i + 1]
            count = 1
        if count == q:
            return start

    app.logger.info('The first number is: ' + str(arr[0]))
    return arr[0]


def get_name_conv(dc, data, quantity, min=100, name_conv=None):
    app.logger.info(
        'Statr function get_name_conv by {} {} name convv {}'.format(dc, quantity, data['Hostname_connvention']))

    get_dc()
    if not name_conv:
        from server_search import hostname_conv
        name_conv = hostname_conv + segment_symbol
    else:
        # gets qtor-xxx (TLV)
        regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]"
        # regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]([0-9]{3})"
        print(type(data['hostname']), type(name_conv))
        if not re.match(regex, name_conv):
            app.logger.error('Wrong convvention name - ' + name_conv)
            return {'Error': 'Wrong convvention name - ' + name_conv}

    print("name_conv={}".format(name_conv))

    # + '-' +  data['Hostname_connvention']

    if data['segment'] == 'alpha':
        if dc not in ['Virginia', 'Oakland']:
            return {'Error': 'Alpha segment exist only in Virginia, Oakland datacenters'}

        name_conv_by_dc = {'Virginia': 'avpr', 'Oakland': 'aopr'}
        name_conv = name_conv_by_dc[dc]

    app.logger.info('Convvention name is ' + name_conv)
    lst_name = list()
    lst_number_range = list(range(min, 999))
    # min is a low number to hostnames
    # if not min:
    url = "http://{}/puppet/rest_nodes?filter={{'facts.datacenter':'{}'}}".format(
        dict_fields['mongodb']['mongo_web'][dict_fields['datacenter']], dc)
    print("try connect to: {}".format(url))
    if dc != 'TelAviv':
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Basic Y2xpZW50OmpYUWdUaXJzMVZkTjc2ZTlaMm5p",
            'cache-control': "no-cache",
        }

        response = requests.request("GET", url, headers=headers)
        j = json.loads(response.text)

        pattern = r'([0-9]{3})'
        print(type(j))
        if '_embedded' in j.keys():
            if type(j['_embedded']) is dict:
                if 'rh:doc' in j['_embedded'].keys():
                    for i in j['_embedded']['rh:doc']:
                        if data['Hostname_connvention'] in i['_id']:
                            match = re.search(pattern, i['_id'])
                            if match:
                                lst_name.append(match.group(1))
            else:  ## TLV
                for i in j['_embedded']:
                    if data['Hostname_connvention'] in i.get('node', ''):
                        match = re.search(pattern, i.get('node', ''))
                        if match:
                            lst_name.append(match.group(1))

    lst_name = sorted(lst_name)
    app.logger.info('exist hosts: ' + str(lst_name))

    dcim = DcimApi()
    lst_names_dcim_sorce = dcim.get_hostnames_by_dc(dc)
    lst_names_dcim = list()
    for name in lst_names_dcim_sorce:
        if data['Hostname_connvention'] in name:
            match = re.search(pattern, name)
            if match:
                lst_names_dcim.append(int(match.group(1)))

    app.logger.info('exist hosts - DCIM: ' + str(lst_names_dcim))

    lst_name = list(
        set(lst_number_range) - set(lst_name) - set(lst_names_dcim))  # remove the number exist from list (100-999)
    app.logger.info('lst_name => {}'.format(lst_name))

    hostnames = list()
    if lst_name:
        start_number = get_sequence_number(lst_name, quantity)
        if start_number:
            start_number -= 1
        else:
            app.logger.error("not found range in {} length".format(quantity))
            return {'Error': "not found range in {} length".format(quantity)}
    else:
        app.logger.warning('No found servers like hostnames')
        if min:
            start_number = int(min) - 1
            app.logger.info(start_number)
        else:
            start_number = 100
        zero_count = 2  # default

    # add_nulls = lambda number, zero_count: "{0:0{1}d}".format(number, zero_count) # add zero

    i = 0
    while i < quantity:
        start_number += 1
        name = name_conv + "-" + data['Hostname_connvention'] + str(start_number)
        if check_hostname(name):
            hostnames.append(name)
            i += 1

    app.logger.info('Hostnames: {}'.format(hostnames))
    return hostnames


from state.save_sessions import save_state, Save_state


def create(data):
    app.logger.info('Startin Create function')
    dict_params = validation_all(data)
    app.logger.info('Passed all validation')
    if 'Error' in dict_params.keys():
        app.logger.error(dict_params)
        return dict_params

    dcim = DcimApi()

    ##### if inserted serials #####
    if not data['serial']:
        # print('looking for devices...')
        app.logger.info('Lokking for devices')
        lst_devices, group_server = looking_4_devices(dict_fields, (data['dc']).title(), dict_params['DiskSize'],
                                                      dict_params['TotalDisks'],
                                                      quantity_servers=int(data['quantity_servers']),
                                                      total_memory=dict_params.get('TotalMemory', 128),
                                                      cpu_cores=dict_params.get('CpuCores', 20), force=data['force'],
                                                      is_ssd=dict_params.get('is_ssd', False),
                                                      is_nvme=dict_params.get('is_nvme', False),
                                                      hw_type=data.get('hw_type', None))
        if type(lst_devices) is dict:  # if error
            app.logger.error(lst_devices)
            return lst_devices

    elif len(set(data['serial'])) != int(data['quantity_servers']):
        app.logger.error('Serials were entered not equal to quantity servers')
        return {'Error': 'Serials were entered not equal to quantity servers'}
    else:
        lst_devices = []

        for serial in set(data['serial']):
            rack = dcim.get_rack_device(serial, (data['dc']).title())
            # price = dcim.get_price_device(serial)

            if type(rack) is dict:
                return rack

            # if type(price) is dict:
            #     return price

            lst_devices.append((serial, rack))

        app.logger.info('Serials are Unicorn choosed: {}'.format(lst_devices))
        group_server = 1

    if not data.get('hostname', None):
        app.logger.info('checking for free hostnames')
        from server_search import global_variables
        global_variables(data['dc'])
        dict_params['segment'] = data['segment']
        data['hostname'] = get_name_conv(data['dc'].title(), dict_params, data['quantity_servers'],
                                         data.get('min', 100))
        # number_server = get_number_to_hostname(name_conv)

    servers = []

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    ## send to rundeck
    for i, device in enumerate(lst_devices):  # index and serial
        serial, rack = device
        if data.get('hostname', None):
            hostname = data['hostname'][i]  # get by index

        options_server = {
            'SerialNo': serial, 'hostname': hostname, 'DataCenter': data['dc'].title(), 'OS': dict_params['Image'],
            'Owner': dict_params['Owner'], 'Num_Of_Boot_Disks': dict_params['Num_Of_Boot_Disks'],
            "Sda_Only": data.get("Sda_Only", "No"),
            'Lprole': data.get('lprole', ''), 'Lpcluster': data.get('lpcluster', ''),
            'Boot_Disk_Raid': dict_params['Boot_Disk_Raid'], 'Reinstall_Server': data.get('Reinstall_Server', 'No'),
            'Skip_Server_Firmware': data.get('Skip_Server_Firmware', 'No'),
            'Puppet_env': data.get('puppet_env', ''),'Num_Of_Boot_Disks':data.get('boot_disk',''),
            'Force_PowerOFF': data.get('Force_PowerOFF', 'No'), 'Configure_RAID': data.get('Configure_RAID', 'Yes'),
            'Data_Disk_Raid': dict_params['Data_Disk_Raid'], 'Rack': rack,
            'Skip_Yum_Update': data.get('Skip_Yum_Update', 'No'), 'Price': str(dict_params['Price']),
            'Arista_Switch': data.get('Arista_Switch', 'Yes'),
            'Flavor_name': dict_params['flavor_name'],
            'Command': data.get('Command', '')
        }
        # saver the serial and hostname
        app.logger.info('lock serial => ' + str(serial))
        dcim.update_owner_and_hostname(serial=serial, owner='InProgress', hostname=hostname)
        # dcim.update_owner(serial=serial,owner ='InProgress')
        # dcim.set_hostname(serial=serial,hostname = hostname)
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        options_server['username'] = data['user']
        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    if group_server != 1:
        return {'tasks': servers, 'message': 'get devices in priority 2', 'uuid': my_uuid}
    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def retry(job_id, user, override_parameter=None):
    app.logger.info('Startin runagain function')

    servers = []

    for id in job_id.split(','):
        rundeck = Rundeck_api()
        response = rundeck.check_job_status(id)
        if not response:
            app.logger.error('The job {} doesnt exist'.format(id))
            return {'Error': 'The job {} doesnt exist'.format(id)}

        if response['status'] == 'succeeded':
            app.logger.error('The job is succeeded jobid: {}'.format(id))
            return {'Error': 'The job is succeeded jobid: {}'.format(id)}

        options_server = response['job']['options']
        if override_parameter:
            for parmeter, value in override_parameter.items():
                options_server[parmeter] = value
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        options_server['username'] = user
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        my_uuid = str(uuid.uuid4())
        app.logger.info('Creating new UUID: {}'.format(my_uuid))
        # app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def canceled_request(my_uuid):
    d = Save_state(my_uuid, 'request')
    app.logger.info('Get details by uuid => ' + str(uuid))
    dcim = DcimApi()
    jobs = d.read_info()
    for job_config in jobs['jobs']:
        print(json.dumps(job_config))
        serial = job_config.get('SerialNo')
        app.logger.info('unlock serial {} => ' + str(serial))
        # r = dcim.update_owner(serial=serial,owner ='Stock')
        # r = dcim.set_hostname(serial=serial,hostname = 'stock')
        dcim.update_owner_and_hostname(serial=serial, owner='Stock', hostname='stock')
    return {'Canceled': True}


def delete(data):
    rundeck = Rundeck_api()
    app.logger.info('Get details by uuid => ' + str(uuid))
    username = data['user']
    servers = []
    hostname = data['hostname'].split(',')
    for i, serial in enumerate(data['serial'].split(',')):
        job_config = {}
        job_config['HostName'] = hostname[i]
        job_config['SerialNo'] = serial
        job_config['DataCenter'] = data['DataCenter']
        job = rundeck.run_job('deco', job_config, username)
        # job = rundeck.run_delete_server(job_config, username)
        save_state('{}_{}'.format(job['id'], username), job, 'state')
        servers.append(job)

    return {'tasks': servers}


def delete_vm(data):
    # deco_vm
    rundeck = Rundeck_api()
    app.logger.info('Get details by uuid => ' + str(uuid))
    username = data['user']
    servers = []
    # hostname = data['hostname'].split(',')
    for i, hostname in enumerate(data['hostname'].split(',')):
        job_config = {}
        job_config['HostName'] = hostname
        job_config['DataCenter'] = data['DataCenter']
        job = rundeck.run_job('deco_vm', job_config, username)
        # job = rundeck.run_delete_server(job_config, username)
        save_state('{}_{}'.format(job['id'], username), job, 'state')
        servers.append(job)

    return {'tasks': servers}


def instaling(mode, my_uuid):
    rundeck = Rundeck_api()
    d = Save_state(my_uuid, 'request')
    app.logger.info('Get details by uuid => ' + str(uuid))

    options_server = {}
    servers = []
    jobs = d.read_info()
    for job_config in jobs['jobs']:
        print(json.dumps(job_config))
        username = job_config['username']
        del job_config['username']
        print(mode, job_config)
        job = rundeck.run_job(mode, job_config, username)
        print(job)
        if job.get('error', None):
            return {'Error': job}
        save_state('{}_{}'.format(job['id'], username), job, 'state')
        # save_state(username, job,'state')

        servers.append(job)

    # if group_server != 1:
    #     return {'tasks': servers, 'message': 'get devices in priority 2'}

    return {'tasks': servers}


def validation_vm(data):
    return data


def create_vm(data):
    app.logger.info('Startin Create VM function')
    dict_params = validation_vm(data)
    app.logger.info('Passed all validation Vm')
    if 'Error' in dict_params.keys():
        app.logger.error(dict_params)
        return dict_params

    servers = []

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    ## send to rundeck
    for i in range(int(data.get('quantity_servers', 1))):  # index and serial

        if data.get('hostname', None):
            hostname = data['hostname'].split(',')[i]  # get by index

        options_server = {
            'Hostname': hostname, 'DataCenter': data['dc'].title(), 'CPUs': data.get('cpus', '4'),
            "Memory_Size_MB": data.get("memory", '4096'),
            # 'Lprole': data.get('lprole', ''), 'Lpcluster': data.get('lpcluster', ''),
            'Disk_Size_GB': data.get('disk_size', "60"),
            'Disk_Type': data.get('disk_type', 'thick'),
            'VLAN': data.get('vlan', '22'),
            'Annotations': data.get('Annotations', str(data['user']) + "installed"),
            'Skip_Yum_Update': data.get('skip_Yum_Update', 'No'),
            'Profile': data.get('profile', 'CentOS_VMWARE_gold'),
            'Skip_DNS_Check': data.get('skip_DNS_Check', 'No'),
            'Command': data.get('Command', '')
        }
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        options_server['username'] = data['user']
        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def validation_openstack_instance(data):
    lst_images = get_openstack_images()
    lst_projects = get_openstack_projects()
    lst_flavors = get_openstack_flavors()

    for i in data:
        if data[i] in ['True', 'False']:
            data[i] = json.loads(data[i].lower())
        elif data[i] == 'None':
            data[i] = None

    data, name_conv = validation.validation_convention_hostnmae(data)

    if 'Error' in data.keys():
        return data
    print('###$$$$')
    print(data['hostname'])

    if not data.get('hostname', None):
        if data.get('dc') == 'TelAviv':
            data['hostname'] = get_name_conv('TelAviv', data, int(data.get('quantity_servers', 1)), min=100,
                                             name_conv=name_conv)
        else:
            data['hostname'] = get_name_conv(data.get('dc'), data, int(data.get('quantity_servers', 1)), min=100,
                                             name_conv=name_conv)

    if type(data['hostname']) is dict:
        if 'Error' in data['hostname'].keys():
            return data['hostname']

    for hostname in data['hostname'].split(','):  # --hostname raor-ori || raor-ori100 || raor-tcd100,raor-tcd102
        # for hostname in data['hostname']:
        if not validation.validation_hostname(data['dc'], hostname,
                                              data.get('mode', 'openstack')):  # check valid hostname by dc
            app.logger.error('{} hostname wrong convention hostname'.format(hostname))
            return {'Error': 'wrong convention hostname'}

    network_details = get_security_group_networkname(data.get('lprole', ''))
    if not network_details or (network_details.get('supported') == False):
        app.logger.error('Security group and network doesnt define to this lprole - {}'.format(data.get('lprole', '')))
        return {'Error': 'Security group and network doesnt define to this lprole - {}'.format(data.get('lprole', ''))}

    print("ODODODODODODODODO")
    print(data['hostname'])
    if ("centos7_gold" if data.get('image', 'centos7_gold') is None else data.get('image')) not in lst_images:
        app.logger.error('{} Invalid image.'.format(data.get('image', 'centos7_gold')))
        return {'Error': '{} Invalid image.'.format(data.get('image', 'centos7_gold'))}

    if ("default" if data.get('project', 'default') is None else data.get('project')) not in lst_projects:
        app.logger.error('{} Invalid project.'.format(data.get('project', 'default')))
        return {'Error': '{} Invalid project.'.format(data.get('project', 'default'))}

    if ("m1.large" if data.get('flavor', 'm1.large') is None else data.get('flavor')) not in lst_flavors:
        app.logger.error('{} Invalid flavor.'.format(data.get('flavor', 'm1.large')))
        return {'Error': '{} Invalid flavor.'.format(data.get('flavor', 'm1.large'))}

    return data


def create_openstack_instance_tlv(data):
    app.logger.info('Startin Create openstack_instance function')
    dict_params = validation_openstack_instance(data)
    print(dict_fields)
    if 'Error' in dict_params.keys():
        app.logger.error(dict_params)
        return dict_params

    app.logger.info('Passed all validation openstack_instance')
    servers = []

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    ## send to rundeck
    for i in range(int(data.get('quantity_servers', 1))):  # index and serial
        # if data.get('hostname', None):
        #     hostname = data['hostname'].split(',')[i]  # get by index

        hostname = data['hostname'][i]
        options_server = {
            'HostName': hostname, 'DataCenter': data['dc'],
            "Flavor": 'm1.large' if data.get("flavor", 'm1.large') is None else data.get("flavor"),
            'Lprole': data.get('lprole', ''), 'Lpcluster': data.get('lpcluster', ''),
            'Image': "centos7_gold" if data.get('image', "centos7_gold") is None else data.get('image'),
            'Project': 'default' if data.get('project', 'default') is None else data.get('project'),
            'Command': data.get('Command', '')
        }
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        options_server['username'] = data['user']
        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def create_openstack_instance(data):
    app.logger.info('Startin Create openstack_instance function')

    dict_params = validation_openstack_instance(data)
    if 'Error' in dict_params.keys():
        app.logger.error(dict_params)
        return dict_params

    app.logger.info('Passed all validation openstack_instance')
    servers = list()

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    ## send to rundeck

    network_details = get_security_group_networkname(data.get('lprole', ''))
    security_group = ' --security-group '.join(network_details.get('security_group'))  # ['app-layer','web-layer']
    # app-layer --security-group web-layer ....
    networkname = network_details.get('network')
    data['hostname'] = data['hostname'].split(',')
    for i in range(int(data.get('quantity_servers', 1))):  # index and serial
        options_server = dict()
        # if data.get('hostname', None):
        #     hostname = data['hostname'].split(',')[i]  # get by index
        hostname = data['hostname'][i]
        options_server = {
            'Release': data.get('release', 'Rocky'), 'SecurityGroupName': security_group, 'NetworkName': networkname,
            'HostName': hostname, 'DataCenter': data['dc'],
            "Flavor": 'm1.large' if data.get("flavor", 'm1.large') is None else data.get("flavor"),
            'Lprole': data.get('lprole', ''), 'Lpcluster': data.get('lpcluster', ''),'f5_clone_server':data.get('f5_clone_server',''),
            'Image': "centos7_gold" if data.get('image', "centos7_gold") is None else data.get('image'),
            'Command': data.get('Command', '')
        }
        options_server['username'] = data['user']
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))

        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        state = save_state(my_uuid, options_server, 'request')

    app.logger.info('{{uuid : {} }}'.format(my_uuid))
    app.logger.debug('{{jobs list : {} }}'.format(servers[0]))
    # state = save_state(my_uuid, servers[0], 'request')
    print('Saved!')

    if type(state) is dict:
        return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def create_clone_openstack(data):
    print(data)
    app.logger.info('Startin Create openstack_instance function')
    # dict_params = validation_openstack_instance(data)

    ##valid
    for i in data:
        if data[i] in ['True', 'False']:
            data[i] = json.loads(data[i].lower())
        elif data[i] == 'None':
            data[i] = None
    if len(set(data['hostname'].split(','))) == 1:
        for name in set(data['hostname'].split(',')):
            regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}[0-9]{3}"
            print('regex' + regex)
            print(name)
            if not re.match(regex, name):  # qtor-xxx999
                regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}"
                if not re.match(regex, name):  # qtor-xxx
                    app.logger.error('{} Invalid convention name'.format(name))
                    return {'Error': '{} Invalid convention name. Should be like qtor-xxx'.format(name)}
                    sys.exit(1)
                # qtor-xxx
                data['Hostname_connvention'] = data['hostname'].split('-')[1]
                name_conv = data['hostname'].split('-')[0]
                data['hostname'] = None
            else:
                data['hostname'] = data['hostname'].split(',')

    if not data.get('hostname', None):
        data['hostname'] = get_name_conv(data['dc'], data, int(data.get('quantity_servers', 1)), min=100,
                                         name_conv=name_conv)
    if type(data['hostname']) is dict:
        if 'Error' in data['hostname'].keys():
            return data['hostname']
    ## valid
    # print(dict_fields)
    # if 'Error' in dict_params.keys():
    #     app.logger.error(dict_params)
    #     return dict_params

    app.logger.info('Passed all validation openstack_instance')
    servers = []

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    network_details = get_security_group_networkname(data.get('lprole', ''))
    if not network_details:
        return {'Error': 'Security group and network doesnt define to this lprole - {}'.format(data.get('lprole', ''))}

    security_group = ','.join(network_details.get('security_group'))
    networkname = network_details.get('network')
    ## send to rundeck
    for i in range(int(data.get('quantity_servers', 1))):  # index and serial
        # if data.get('hostname', None):
        #     hostname = data['hostname'].split(',')[i]  # get by index

        hostname = data['hostname'][i]
        options_server = {
            'HostName': hostname, 'DataCenter': data['dc'], 'SecurityGroupName': security_group,
            'NetworkName': networkname,
            'HostNameCloneFrom': data['cloneserver'],
            'Lprole': data.get('lprole', ''), 'Lpcluster': data.get('lpcluster', ''),
            'Command': data.get('Command', '')
        }
        options_server['username'] = data['user']
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        # options_server['username'] = data['user']
        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def create_clone_openstack_tlv(data):
    print(data)
    app.logger.info('Startin Create openstack_instance function')
    # dict_params = validation_openstack_instance(data)

    ##valid
    for i in data:
        if data[i] in ['True', 'False']:
            data[i] = json.loads(data[i].lower())
        elif data[i] == 'None':
            data[i] = None
    if len(set(data['hostname'].split(','))) == 1:
        for name in set(data['hostname'].split(',')):
            regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}[0-9]{3}"
            print('regex' + regex)
            print(name)
            if not re.match(regex, name):  # qtor-xxx999
                regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}"
                if not re.match(regex, name):  # qtor-xxx
                    app.logger.error('{} Invalid convention name'.format(name))
                    return {'Error': '{} Invalid convention name. Should be like qtor-xxx'.format(name)}
                    sys.exit(1)
                # qtor-xxx
                data['Hostname_connvention'] = data['hostname'].split('-')[1]
                name_conv = data['hostname'].split('-')[0]
                data['hostname'] = None
            else:
                data['hostname'] = data['hostname'].split(',')

    print(data['hostname'])

    if not data.get('hostname', None):
        data['hostname'] = get_name_conv('TelAviv', data, int(data.get('quantity_servers', 1)), min=100,
                                         name_conv=name_conv)
    if type(data['hostname']) is dict:
        if 'Error' in data['hostname'].keys():
            return data['hostname']
    ## valid
    # print(dict_fields)
    # if 'Error' in dict_params.keys():
    #     app.logger.error(dict_params)
    #     return dict_params

    app.logger.info('Passed all validation openstack_instance')
    servers = []

    my_uuid = str(uuid.uuid4())
    app.logger.info('Creating new UUID: {}'.format(my_uuid))
    ## send to rundeck
    for i in range(int(data.get('quantity_servers', 1))):  # index and serial
        # if data.get('hostname', None):
        #     hostname = data['hostname'].split(',')[i]  # get by index

        print(data['hostname'])
        hostname = data['hostname'][i]
        options_server = {
            'HostName': hostname, 'DataCenter': data['dc'],
            'HostNameCloneFrom': data['cloneserver'],
            'Lprole': data.get('lprole', ''), 'Lpcluster': data('lpcluster', ''),
            'Command': data.get('Command', '')
        }
        servers.append(options_server)

        # print('user: {} data=> {}  uuid => {}'.format(data['user'],options_server,my_uuid))
        options_server['username'] = data['user']
        app.logger.info(data)
        app.logger.info(options_server)
        # print(json.dumps(options_server))
        app.logger.info('{{uuid : {} }}'.format(my_uuid))

        state = save_state(my_uuid, options_server, 'request')
        if type(state) is dict:
            return state

        del options_server['username']

    app.logger.info('End Create function')
    return {'tasks': servers, 'uuid': my_uuid}


def delete_openstack_instance(data):
    rundeck = Rundeck_api()
    app.logger.info('Get details by uuid => ' + str(uuid))
    username = data['user']
    servers = []
    hostname = data['hostname'].split(',')
    for i in range(int(data.get('quantity_servers', 1))):
        job_config = {}
        job_config['HostName'] = hostname[i]
        job_config['ConfirmationDelete'] = data.get('Confirmation', 'Yes')
        job_config['DataCenter'] = data['DataCenter']
        job = rundeck.run_job('deco_openstack', job_config, username)
        # job = rundeck.run_delete_server(job_config, username)
        save_state('{}_{}'.format(job['id'], username), job, 'state')
        servers.append(job)

    return {'tasks': servers}

def server_firmware_update(data):
    dcim = DcimApi()
    rundeck = Rundeck_api()
    username = data['user']
    servers = []
    serials = data['serial'].split(',')
    for i in serials:
        if type(dcim._get_device_id(i)) is not str:
            print("Error: Serial doesn't exist {}".format(i))
            return {'Error':"This Serial doesn't exist"}
    for i in range(int(data.get('quantity_servers', 1))):
        job_config = {}
        job_config['Serial'] = serials[i]
        job_config['DataCenter'] = data['DataCenter']
        job = rundeck.run_job('firmware_update', job_config, username)
        save_state('{}_{}'.format(job['id'], username), job, 'state')
        servers.append(job)
    return {'tasks': servers}


