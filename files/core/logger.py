__author__ = 'oris'

import os
import datetime
import logging

def init_logger(dir_name=None,app_name=None):
    # filename = '{dir}/{app}_{date}.log'.format(dir=dir_name,app=app_name,date=datetime.datetime.today().strftime('%Y-%m-%d'))
    # if not os.path.exists(dir_name):
    #     os.mkdir(dir_name)
    # logging.basicConfig(filename=filename, level=logging.INFO,format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
    logging.basicConfig(level=logging.INFO,format='%(asctime)s [%(levelname)s]: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')
    return logging

