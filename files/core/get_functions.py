import os
import sys

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core', '')
root_dir = os.path.dirname(os.path.realpath(__file__)).replace('/files/core', '')
sys.path.append(home_dir)
from dcim_api import DcimApi
from rundeck_api import Rundeck_api
import validation
import glob
import utils
from flask import current_app as app
from server_search import get_match_devices, global_variables
from state.save_sessions import Save_state, save_flavors

'''
All GET function
'''

''' get dc https://lpbma...../dc'''


def get_dc():
	app.logger.info("Start function - get_dc")
	dcim = DcimApi()
	app.logger.info("End function - get_dc")
	return dcim.get_dcs_name()


# def get_profiles():
#     app.logger.info("Start function - get_profile")
#     dcim = DcimApi()
#     app.logger.info("End function - get_profile")
#     return dcim.get_owner_name()


def get_profiles():
	app.logger.info("Start function - get_profile")
	if not os.path.exists("/tmp/{}".format('unicorn')):
		utils.git_no_print('/tmp', 'unicorn')
	# utils.git_no_print('/tmp', 'unicorn')
	lst_profiles = [x.split('/')[4].replace('.yaml', '') for x in
	                glob.glob("/tmp/unicorn/profiles/*.yaml")]
	app.logger.info('End function: get_flavors')
	return lst_profiles


''' get profile by name https://unicron...../profile/<profile_name> '''


def get_profile_by_name(profile_name):
	app.logger.info("Start function - get_profile by name")
	# dcim = DcimApi()
	# response = dcim.get_owner_name()
	# app.logger.info('Get profile form DCIM')
	#
	# if type(response) is dict: # error
	#     app.logger.error(response['error'])
	#     return response
	
	app.logger.info("Success connect to DCIM")
	lst_profiles = get_profiles()
	# lst_profiles = [x.lower() for x in dcim.get_owner_name()]
	app.logger.info("Get list of files under /profiles")
	
	if profile_name not in lst_profiles:
		app.logger.error('<{}> profile dosent exist'.format(profile_name))
		return {'Error': 'This profile dosent exist'}
	
	path = validation.check_profile_exist(profile_name)
	app.logger.info('Path file => ' + path)
	
	if path:
		profile_detals = utils.read_yaml(path)
	
	else:
		app.logger.error("yaml dosent exit for <{}> profile".format(profile_name))
		return {'Error': 'yaml dosent exit for this profile'}  # exit(1)
	
	app.logger.info('End function: get_profile_by_name')
	return profile_detals


def get_flavors():
	app.logger.info('Start function - get_flavors')
	print("check: {} if exist".format("/tmp/{}/flavors".format('unicorn')))
	print(os.path.exists("/tmp/{}/flavors".format('unicorn')))
	if not os.path.exists("/tmp/{}/flavors".format('unicorn')):
		print("CLONEEECLONECOLEN!!!")
		utils.git_no_print('/tmp', 'unicorn')
	
	lst_flavors = [x.split('/')[4].replace('.yaml', '') for x in
	               glob.glob("/tmp/unicorn/flavors/*.yaml")]
	result = []
	for flavor in lst_flavors:
		flavors_info = get_flavor_by_name(flavor)
		flavors_info.update({'Flavor Name': flavor})
		result.append(flavors_info)
	
	app.logger.info('End function: get_flavors')
	return result


def get_flavors_capacity_build():
	app.logger.info('starting function get_flavors_capacity_build')
	dcs = set(get_dc())
	lst_flavors = get_flavors()
	print(lst_flavors)
	lst_capacity = list()
	for flavor in lst_flavors:
		# app.logger.info('Build for {} '.format(flavor))
		flavor_new = flavor.copy()
		for dc in dcs:
			app.logger.info('Build for {} - {}'.format(flavor['Flavor Name'], dc))
			global_variables(dc)
			
			# convert to boolean the value of is ssd
			if flavor.get('is_ssd', None) and flavor.get('is_ssd', None) == 'True' or flavor.get('is_ssd',
			                                                                                     None) is True:
				flavor['is_ssd'] = True
			else:
				flavor['is_ssd'] = False
			# convert to boolean the value of is ssd
			if flavor.get('is_nvme', None) and flavor.get('is_nvme', None) == 'True' or flavor.get('is_nvme',
			                                                                                       None) is True:
				flavor['is_nvme'] = True
			else:
				flavor['is_nvme'] = False
			
			result =\
			get_match_devices(dc, flavor['DiskSize'], flavor['TotalDisks'], cpu_cores=flavor.get('CpuCores', 20),
			                  total_memory=flavor['TotalMemory'], is_ssd=flavor['is_ssd'],
			                  is_nvme=flavor.get("is_nvme", False), force=True, report=True)[0]
			if type(result) is dict:
				flavor_new[dc] = 0
			else:
				flavor_new[dc] = len(result)
		
		# app.logger.info(flavor_new)
		lst_capacity.append(flavor_new)
		flavor_new['Flavor_Name'] = flavor_new.pop('Flavor Name')
		flavor_new['Price_int'] = flavor_new.pop('Price')
		print(flavor_new)
	
	from state.save_sessions import save_state
	app.logger.info('Write to Swift')
	print(lst_capacity)
	save_flavors(lst_capacity)


def get_flavor_capacity():
	from state.save_sessions import Save_state
	s = Save_state('flavors', 'tools')
	data = s.read_info()
	return data['jobs']


def get_security_group_networkname(lprole):
    app.logger.info("Start function - get_security_group_networkname by lprole")
    repo_name = 'puppet-ansible_conf'
    if not os.path.exists("/tmp/{}".format(repo_name)):
        utils.git_no_print('/tmp', repo_name)

    lst_items = utils.read_yaml("/tmp/puppet-ansible_conf/files/SDN/openstack-vars.yaml")
    app.logger.info('lprole = {}, value = {}'.format(lprole,lst_items))
    app.logger.info('End function: get_security_group_networkname ')
    return lst_items.get(lprole)


''' get profile by name https://lpbma...../profile/<profile_name> '''


def get_flavor_by_name(flavor_name):
	app.logger.info("Start function - get_flavor by name")
	
	path = validation.check_flavor_exist(flavor_name)
	
	if path:
		app.logger.info('Path file => ' + path)
		flavor_detals = utils.read_yaml(path)
	
	else:
		app.logger.error("yaml dosent exit for <{}> flavor".format(flavor_name))
		return {'Error': 'yaml dosent exit for this flavor'}  # exit(1)
	
	app.logger.info('End function: get_flavor_by_name')
	return flavor_detals


def get_status(user):
	app.logger.info('Start Get status for {}'.format(user))
	from state.save_sessions import Save_state
	from swift_api import SwiftApi
	
	# p = Save_state(user, 'state')
	sft = SwiftApi()
	lst_jobs = sft.get_jobs_4_user('state', user)
	# data = p.read_info()
	# lst_jobs = [f['jobs'] for f in data]
	app.logger.info('End Get status for ' + user)
	return lst_jobs


def get_devices(fields):
	app.logger.info('Start function: get_devices')
	dcim = DcimApi()
	app.logger.info('End function: get_devices')
	return dcim.get_devices_by_fields(fields)


def get_image():
	repo_name = 'puppet-lpcobbler'
	app.logger.info('Start function: get_image')
	# if not os.path.exists("/tmp/{}".format(repo_name)):
	if not os.path.exists("/tmp/{}".format(repo_name)):
		utils.git_no_print("/tmp", repo_name)
	# else:
	# app.logger.info('Pull repo to /tmp')
	app.logger.info('End function: get_image')
	return [x.split('/')[5].replace('.json', '') for x in
	        glob.glob("/tmp/{}/files/profiles.d/*.json".format(repo_name))]
	
	app.logger.error('End function: get_os with Error')
	return {'Error': 'Failed to get all operating system'}


def get_stocks_server_by_dc(dc):
	app.logger.info('Start function: get_stocks_server_by_dc')
	dcim = DcimApi()
	devices, devices_priority_2 = dcim.get_stock_servers_by_dc(dc)
	devices = devices + devices_priority_2
	app.logger.info('End function: get_stocks_server_by_dc')
	return devices


#### OpenStack ####
def get_openstack_projects():
	app.logger.info("Start function - get_project_openstack")
	if not os.path.exists("/tmp/{}".format('unicorn')):
		utils.git_no_print('/tmp', 'unicorn')
	lst_items = utils.read_yaml("/tmp/unicorn/OpenStack_parameters/projects.yaml")
	app.logger.info('End function: get_project_openstack')
	return lst_items


def get_openstack_flavors():
	app.logger.info("Start function - get_project_openstack")
	if not os.path.exists("/tmp/{}".format('unicorn')):
		utils.git_no_print('/tmp', 'unicorn')
	lst_items = utils.read_yaml("/tmp/unicorn/OpenStack_parameters/flavors.yaml")
	app.logger.info('End function: get_project_openstack')
	return lst_items


def get_openstack_images():
	app.logger.info("Start function - get_project_openstack")
	if not os.path.exists("/tmp/{}".format('unicorn')):
		utils.git_no_print('/tmp', 'unicorn')
	
	lst_items = utils.read_yaml("/tmp/unicorn/OpenStack_parameters/images.yaml")
	app.logger.info('End function: get_project_openstack')
	return lst_items


#### OpenStack ####


def get_jobs_deatils_by_user(user, dc=None, job_id=None):
	lst_jobs = list()
	state_dir = '/var/unicorn_states/{}/state/'.format(utils.get_dc())
	print('dir => {}'.format(state_dir))
	files = utils.get_last_12h_files(state_dir, '*_*')
	print("#####")
	print(files)
	# files = [os.path.basename(x) for x in glob.glob(state_dir)]
	for f in files:
		print(f)
		if user not in f:
			print('skip {}'.format(f))
			continue
		if job_id:
			if job_id not in f:
				print('skip {}'.format(f))
				continue
		id, user_f = f.split('_')
		print(id)
		print(user_f)
		rundeck = Rundeck_api()
		step, total_steps, step_index = rundeck.get_current_step(id)
		result = rundeck.check_job_status(id)
		result['step'] = step
		result['total_step'] = total_steps
		result['step_index'] = step_index
		lst_jobs.append(result)
	return lst_jobs
