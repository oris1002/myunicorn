#!/usr/bin/env python3

import os
import requests
import urllib3
import datetime
import operator
import random
from api import Api
urllib3.disable_warnings()
from utils import read_yaml
import os
import sys
import subprocess
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')
sys.path.append(home_dir)

dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))

def get_dcim_password_kubernetes():
    """ Looking for dcim password from enviroments.
    Args : None
    Return : Dcim password.
    """
    p = subprocess.Popen ('set | grep -i  dcim_password' , shell=True , stdout=subprocess.PIPE)
    dcim_password , err = p.communicate ()
    dcim_password = dcim_password.decode ('utf-8')
    dcim_password = dcim_password.split ('\n')[1].split ('=')[1]
    dcim_password = dcim_password.replace("\'","")

    p = subprocess.Popen('set | grep -i  dcim_user', shell=True, stdout=subprocess.PIPE)
    dcim_user, err = p.communicate()
    dcim_user = dcim_user.decode('utf-8')
    dcim_user = dcim_user.split('\n')[1].split('=')[1]
    dcim_user = dcim_user.replace("\'", "")
    
    global dict_fields
    dict_fields['dcim']['dcim_user'] = dcim_user
    dict_fields['dcim']['dcim_password'] = dcim_password


class DcimApi():
    def __init__(self):
        try:
            get_dcim_password_kubernetes()
            self._dcim_user = dict_fields['dcim']['dcim_user']
            self._dcim_password = dict_fields['dcim']['dcim_password']
            self._opendcim_server = dict_fields['dcim']['opendcim_server']
            self._api = Api('https://' + self._opendcim_server + '/api/v1/', self._dcim_user, self._dcim_password)
            
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"} # exit(1)
    
    def get_dcs_name(self):
        try:
            response = self._api.Get_Response('datacenter?attributes=Name') # get all datacenters
            if response:
                return list(x['Name'].split(' ')[0] for x in response['datacenter'])
            else:
                return {'Error':"An unkonwn error occurred while accessing the DCIM"}
        except Exception:
            # print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}

    def get_owner_name(self):
        try:
            response = self._api.Get_Response('department?attributes=Name')
            return list(x['Name'].split(' ')[0] for x in response['department'])
        except Exception:
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}


    def _datacenter_id(self, dc_name):
        try:
            datacenterIDs = []
            response = self._api.Get_Response('datacenter?wildcards&Name={}%%&attributes=DataCenterID'.format(dc_name))
            for dc_id in response['datacenter']:
                datacenterIDs.append(str(dc_id['DataCenterID']))
            return datacenterIDs
        
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
    
    def _get_owner_id(self, owner):
        try:
            response = self._api.Get_Response('department?wildcards&Name={}&attributes=Name,DeptID'.format(owner))
            for dept in response['department']:
                if(dept['Name'] == owner):
                    return str(dept['DeptID'])
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error':"An unkonwn error occurred while accessing the DCIM"}

    def _get_device_id(self, serial):
        try:
            response = self._api.Get_Response('device?SerialNo={}&attributes=DeviceID'.format(serial))
            if response:
                return str(response['device'][0]['DeviceID'])
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}

    def get_devices_by_fields(self, fields):
        con = False
        devices = []
        if fields.get('fields'):
            f = fields.get('fields').split(',') # fields list
            # get only the value by fields
            dictfilt = lambda x, y: dict([(i, x[i]) for i in x if i in set(y)])
            
        try:
            
            datacenterIDs = self._datacenter_id(fields['dc'])
            if fields.get('Owner'):
                owner_id = self._get_owner_id(fields['Owner'])
            else:
                owner_id = None
            
            for datacenter_id in datacenterIDs:
                all_devices = self._api.Get_Response('device/bydatacenter/{}'.format(datacenter_id))
                
                for i,device in enumerate(all_devices['device']):
                    if owner_id:
                        if str(device['Owner']) == owner_id:
                            device['Owner'] = fields['Owner']
                        else:
                            continue
                    
                    for key in fields:
                        if key not in ['fields','dc','Owner']:
                            if device.get(key,fields[key]) != fields[key]:
                                con = True # if continue in outside loop
                                break
                            else:
                                # change to false for run over the last value(The value may remain True)
                                con = False
                                
                    if con:
                        continue
                                
                    if fields.get('fields'):
                        devices.append(dictfilt(device,f))
                    else:
                        devices.append(device)
                        
            return devices
            
        except Exception as e:
            print(e)
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}

    def get_hostnames_by_dc(self,dc_name="dc_name"):
        try:
            dc_ips = self._datacenter_id(dc_name)
            owner_id_ex = [self._get_owner_id('Network'),self._get_owner_id('Storage')]
            lst_hostnames = list()
            for dc in dc_ips:
                
                response = self._api.Get_Response('device/bydatacenter/{}?attributes=Label,Owner'.format(dc)) # get hostnames
                # response = self._api.Get_Response('datacenter?attributes=Name') # get all datacenters
                
                if response:
                    lst_hostnames += list(x['Label'].lower() for x in response['device'] if x['Owner'] not in owner_id_ex )
                else:
                    return {'Error':"An unkonwn error occurred while accessing the DCIM"}
            return lst_hostnames
        except Exception:
            # print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}

    def get_stock_servers_by_dc(self,dc_name="dc_name"):
        
        try:
            all_devices = self.get_devices_by_fields({'dc':dc_name,'Owner':'Stock'})
            devices_priority_1 = list()
            devices_priority_2 = list()
            format_str = '%Y-%m-%d'  # The format date
            
            for device in all_devices:
                
                if device['WarrantyExpire']:
                    if device['WarrantyExpire'] != '1970-01-01':
                        warrantyExpire = datetime.datetime.strptime(device['WarrantyExpire'], format_str)
                        today = datetime.datetime.strptime(datetime.datetime.now().strftime(format_str),
                                                           format_str)  # today
                        diff = (today - warrantyExpire).days / 365  ### diff in years
                        if diff < 0:
                            devices_priority_1.append([device, diff])  # priority
                        elif diff >=0 and diff <= 365:
#                         else:
                            devices_priority_2.append([device, diff])
    
            return devices_priority_1, devices_priority_2
        except Exception as e:
            return {'Error': e}

    
    def get_rack_device(self, serial,dc):
        
        try:
            response = self._api.Get_Response('device?SerialNo={}&attributes=Cabinet'.format(serial))
            if response:
                response = self._api.Get_Response('cabinet?CabinetID={}'.format(response['device'][0]['Cabinet']))
                if dc not in response['cabinet'][0]['DataCenterName']:
                    print('Error: This device not exist in datacenter that choosed')
                    return {'Error': 'This device not exist in datacenter that choosed'}
    
                return response['cabinet'][0]['Location']
            
            else:
                return {'Error': 'Failed Get Cabinet ID'}
    
        except Exception as e:
            print(e)
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}

    def update_owner_and_hostname(self, **kwargs):
        try:
            deviceID = self._get_device_id(kwargs.get('serial'))
            ownerID = self._get_owner_id(kwargs.get('owner'))
            hostName = kwargs.get('hostname')
        
            response = self._api.post_data('device/{}'.format(deviceID),
                                           "{{\"Label\":\"{}\",\"Owner\":\"{}\"}}".format(hostName, ownerID))
            if not response:
                print("An unkonwn error occurred while accessing the DCIM")
                return {'Error': "An unkonwn error occurred while accessing the DCIM"}  # exit(1)
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}  # exit(1)
        
    def update_owner(self,serial,owner):
        try:
            deviceID = self._get_device_id(serial)
            ownerID = self._get_owner_id(owner)
    
            response = self._api.post_data('device/{}'.format(deviceID),"{{\"Owner\":\"{}\"}}".format(ownerID))
            if not response:
                print("An unkonwn error occurred while accessing the DCIM")
                return {'Error': "An unkonwn error occurred while accessing the DCIM"}  # exit(1)
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}  # exit(1)

    def set_hostname(self,**kwargs):
        try:
            deviceID = self._get_device_id(kwargs.get('serial'))
            response = self._api.post_data('device/{}'.format(deviceID), "{{\"Label\":\"{}\"}}".format(kwargs.get('hostname').upper()))
            if not response:
                print("An unkonwn error occurred while accessing the DCIM")
                return {'Error': "An unkonwn error occurred while accessing the DCIM"}
        except IOError as e:
            print("An unkonwn error occurred while accessing the DCIM")
            return {'Error': "An unkonwn error occurred while accessing the DCIM"}
    
    # def get_price_device(self, serial):
    #     try:
    #         response = self._api.Get_Response('device?SerialNo={}&attributes=PriceTag'.format(serial))
    #         if response:
    #             price = response['device'][0]['PriceTag']
    #             if price and price.isdigit():
    #                 return int(price)
    #             else:
    #                 return 0
    #         else:
    #             return {'Error': 'Failed Get PriceTag ID'}
    #
    #     except Exception as e:
    #         print(e)
    #         return {'Error': "An unkonwn error occurred while accessing the DCIM"}
