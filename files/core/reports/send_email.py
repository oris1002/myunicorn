to_list_default = 'dc-engineering-lp@liveperson.com,cloudops-lp@liveperson.com'
import subprocess
def get_datacenter_name():
    """ Looking for datacenter name from enviroments
    Args : None
    Return : dc name
    """
    # Check on which DC i'm running, in order to run locally
    p = subprocess.Popen ('set | grep -i  datacenter', shell=True , stdout=subprocess.PIPE)
    dc_name , err = p.communicate ()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    if dc_name.lower() == "tlv":
        dc_name = ""
    dc_name = dc_name.lower()
    dc_map = {'virginia':'va','oakland':'ca','amsterdam':'am','london':'lo','sydney':'sy','melbourne':'me'}
    
    return dc_map.get(dc_name)

def send_mail(filename,subject,to_list=to_list_default,):
    import smtplib
    import sys
    import os
    
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    
    _from = "unicorn@liveperson.com"

    # subject = "Unicorn Reports - Flavors Capacity"
    # filename = "/tmp/flavor_capacity.html"
    
    if os.path.isfile(filename):
        
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = _from
        msg['To'] = to_list
        
        fp = open(filename, "r")
        HTML_BODY = MIMEText(fp.read(), 'html')
        fp.close()
    
    else:
        print("File %s doesn't exists!".format(filename))
        sys.exit()
    
    try:
        msg.attach(HTML_BODY)
        
        s = smtplib.SMTP('mail-{}.lpdomain.com'.format(get_datacenter_name()))
        #for rec in to_list.split(","):
        #    s.sendmail(_from, rec, msg.as_string())
        print("Successfully sent email")
    except smtplib.SMTPException:
        print("Error: unable to send email")
    s.quit()
