import os
import sys
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core/reports','')
sys.path.append(home_dir)
unicorn_url = 'http://am.unicorn.int.liveperson.net/'
filename = '/tmp/installation_report.csv'
rundeck_url = None
_token = None
from bigQuery import BigQuery


import requests

from core.utils import read_yaml
import json

unicorn_url = 'http://am.unicorn.int.liveperson.net/'
filename = '/tmp/installation_report.csv'
rundeck_url = None
_token = None
import datetime


def _get_vars():
    _dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
    if type(_dict_fields) is not dict:
        return False
    
    global _token, rundeck_url
    # _token = _dict_fields['rundeck']['rundeck_token']
    _token = "olSbH2PjBgLWK1qR6OlEqnDvYifEgtS8"
    rundeck_url = "https://" + _dict_fields['rundeck']['rundeck_vip'] + '/api/30'
    return True


def get_execution_by_days(job_name,days,status=None):
    '''
    :param job_name: job name in rundeck
    :param status: running / failed / successed
    :param days: number of days
    :return:
    '''
    _get_vars()
    headers = {
        'Accept': "application/json",
        'Content-Type': "application/json",
        'X-Rundeck-Auth-Token': _token,
        'cache-control': "no-cache"
    }

    if status:
       url = "{url}/project/Install_Anything/executions?statusFilter={status}&jobFilter={jobName}&recentFilter={days}d&max=99999".format(url=rundeck_url,status=status, jobName=job_name,days=days)
    else:
        url = "{url}/project/Install_Anything/executions?jobFilter={jobName}&recentFilter={days}d&max=99999".format(url= undeck_url,jobName=job_name,days=days)
    
    response = requests.get(url, headers=headers)
    data = json.loads(response.text).get('executions', None)
    
    return data



def in_progress_jobs(project_name,dataset_name,table_name):
    col = [
        ("job_id", "INTEGER", "REQUIRED"),
        ("user", "STRING", "REQUIRED"),
        ("start_date", "DATETIME", "REQUIRED"),
        ("Status", "STRING", "REQUIRED"),
        ("DataCenter", "STRING", "REQUIRED"),
        ("SerialNo", "STRING", "REQUIRED"),
        ("Owner", "STRING", "REQUIRED"),
        ("Price", "INTEGER", "REQUIRED")
    
    ]
    rows_to_insert = get_data('Install_Server_by_Serial',600,status='running')
    table_ref = '{}.{}.{}'.format(project_name,dataset_name,table_name)
    my_conn= BigQuery(project_name,dataset_id=dataset_name,table_id=table_name)
                      #,path="/lp_unicorn/unicorn/credentioal.json")
    # my_conn._get_table(table_ref)
    my_conn.upload_data_2_table(rows_to_insert,columns=col)
    
    
def get_data(job_name,days,status=None):
    lst = get_execution_by_days('Install_Server_by_Serial',days,status=status)
    # lst = get_execution_by_days('Deco_Server_by_HostName','succeeded',days)
    # lst = get_installation_by_days(days)

    data = list()
    serials = list()
    if lst:
    
        for job in lst:
            if job.get('job', {}).get('options', {}).get('SerialNo', None) not in serials and 'tst' not in job.get(
                    'job', {}).get('options', {}).get('hostname', ''):
                data.append(
                    (job.get('id',9999),
                     job.get('user', ''),
                     datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                                "%Y-%m-%dT%H:%M:%SZ"),
                     job.get('status', 'Unknow'),
                     job.get('job', {}).get('options', {}).get('DataCenter', ''),
                     job.get('job', {}).get('options', {}).get('SerialNo', ''),
                     job.get('job', {}).get('options', {}).get('Owner', ''),
                     int(job.get('job', {}).get('options', {}).get('Price', '0'))
                     ))
                serials.append(job.get('job', {}).get('options', {}).get('SerialNo', None))
    
        return data
    
    

def main():
    project_name = 'cloudops-241008'
    dataset_name = 'Unicorn'
    table_name = 'All_Installation'
    # create_table('{}.{}.{}'.format(project_name,dataset_name,table_name))
    in_progress_jobs(project_name,dataset_name,table_name)

    
if __name__ == '__main__':
    main()
