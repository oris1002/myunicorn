__author__ = 'oris'
__verstion__ = '1.0.0'

from google.cloud import bigquery
from google.cloud.exceptions import NotFound
import os
# from dataset import dataset
# from table import table

'''https://googleapis.github.io/google-cloud-python/latest/bigquery/generated/google.cloud.bigquery.client.Client.html'''

class BigQuery():
    '''
    BigQuery connector in GCP
    Create a service account and save the json file [ https://console.cloud.google.com/iam-admin/serviceaccounts ]
    Credentials json file defualt is => '~/.gcp/{project_id}.json'
    '''
    def __init__(self,project_id, dataset_id=None, table_id=None,path=None):
        '''
        :param project_id: (google.cloud.bigquery.Project.ProjectReference):
                A reference to the Project to look for.
        '''
        try:
            self._project_id = project_id
            print('project_id => {}'.format(self._project_id))
            # load a credentials
            if not os.environ.get('GOOGLE_APPLICATION_CREDENTIALS',None):
                if path:
                    print('loading credentials from: {}'.format(path))
                    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = path
    
                else:
                    print('loading credentials from: {}'.format('~/.gcp/{}.json'.format(self._project_id)))
                    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.expanduser('~/.gcp/{}.json'.format(self._project_id))
            else:
                print('GOOGLE_APPLICATION_CREDENTIALS environment variable already define. {} '.format(os.environ['GOOGLE_APPLICATION_CREDENTIALS'] ))

            # client (google.cloud.bigquery.client.Client): A client to connect to the BigQuery API.
            self.client = bigquery.Client()
            if dataset_id:
                self._dataset_id = dataset_id
            
            if table_id:
                self._table_id = table_id
        except Exception as e:
            print(e)
            exit(1)

    def _dataset_exists(self, _dataset_ref):
        """Return if a dataset exists.
            :param dataset_reference (google.cloud.bigquery.dataset.DatasetReference):
                        A reference to the dataset to look for.
           :return: bool: ``True`` if the dataset exists, ``False`` otherwise.
        """
        # dataset_reference = '{}.{}'.format(self._project_id, _dataset_id)
        try:
            self.client.get_dataset(_dataset_ref)
            return True
        except NotFound:
            return False

    def _get_dataset(self, _dataset_ref):
    
        dataset = self.client.get_dataset(_dataset_ref)
    
        # full_dataset_id = "{}.{}".format(dataset.project, dataset.dataset_id)
        friendly_name = dataset.friendly_name
        print(
            "Got dataset '{}' with friendly_name '{}'.".format(
                full_dataset_id, friendly_name
            )
        )
    
        # View dataset properties
        print("Description: {}".format(dataset.description))
        print("Labels:")
        labels = dataset.labels
        if labels:
            for label, value in labels.items():
                print("\t{}: {}".format(label, value))
        else:
            print("\tDataset has no labels defined.")
    
        # View tables in dataset
        print("Tables:")
        tables = list(self.client.list_tables(dataset))  # API request(s)
        if tables:
            for table in tables:
                print("\t{}".format(table.table_id))
        else:
            print("\tThis dataset does not contain any tables.")
    
        # [END bigquery_get_dataset]

    def _create_dataset(self, _dataset_ref):
        '''
        :param project_id: (google.cloud.bigquery.Project.ProjectReference):
                A reference to the Project to look for.
        :param _dataset_ref: (google.cloud.bigquery.dataset.DatasetReference):
                A reference to the dataset to look for.
        '''
        try:
            dataset = bigquery.Dataset(_dataset_ref)
            self.client.create_dataset(dataset)  # API request
            print(
                "Created dataset {}.{}".format(dataset.project, dataset.dataset_id)
            )
            # return dataset
        except Exception as e:
            print(e)
            exit(1)
        # [END bigquery_create_dataset]



    def _table_exists(self, table_reference):
        """Return if a dataset exists.
        :param: table_id (google.cloud.bigquery.table.TableReference):
                A reference to the dataset to look for.
        :param: dataset_id (google.cloud.bigquery.dataset.Dataseteference):
                A reference to the dataset to look for.
        :return:
            bool: ``True`` if the table exists, ``False`` otherwise.
        """
    
        # table_reference = '{}.{}.{}'.format(self._project_id, _dataset_id, _table_id)
    
        try:
            self.client.get_table(table_reference)
            return True
        except NotFound:
            return False

    def _get_table(self, table_ref):
    
        table = self.client.get_table(table_ref)
    
        print(
            "Got table '{}.{}.{}'.".format(table.project, table.dataset_id, table.table_id)
        )
    
        # View table properties
        print("Table schema: {}".format(table.schema))
        print("Table description: {}".format(table.description))
        print("Table has {} rows".format(table.num_rows))
        # [END bigquery_get_table]
        table
        return table

    def _create_table(self, table_ref, columns):
        '''
        :param: table_ref (google.cloud.bigquery.table.TableReference):
                A reference to the dataset to look for.
        :param columns: List of Columns
                [
            ("job_id", "INTEGER", "REQUIRED"),
            ("user", "STRING", "REQUIRED"),
            ("start_date", "DATETIME", "REQUIRED"),
            ("DataCenter", "STRING", "REQUIRED"),
            ("SerialNo", "STRING", "REQUIRED"),
            ("Owner", "STRING", "REQUIRED"),
            ("Price", "STRING", "REQUIRED"),
        ]
        mode options [ REQUIRED,NULLABLE ]
        defualt mode is NULLABLE
        for more info: https://cloud.google.com/bigquery/docs/schemas

        '''
    
        schema = [bigquery.SchemaField(c[0], c[1], mode=c[2]) for c in columns]
    
        table = bigquery.Table(table_ref, schema=schema)
        table = self.client.create_table(table)  # API request
        print(
            "Created table {}.{}.{}".format(table.project, table.dataset_id, table.table_id)
        )
        # [END bigquery_create_dataset]

    def insert_rows_2_table(self, data, _dataset_id=None, _table_id=None, columns=None):
        '''
        :param dataset_id (google.cloud.bigquery.dataset.Dataseteference):
                A reference to the dataset to look for.
        :param table_id:
        :param data: List of data to load:
                [ (value1,value2....),
                  (value1,value2....),
                  ...
                ]
        :param columns: List of Columns
                [
                 ("col1", "INTEGER", "REQUIRED"),
                 ("col2", "STRING", "REQUIRED"),
                 ("col3", "DATETIME", "REQUIRED"),
                  ....
                ]
        mode options [ REQUIRED,NULLABLE ]
        defualt mode is NULLABLE
        for more info: https://cloud.google.com/bigquery/docs/schemas

        :return:
        '''
        if _dataset_id:
            self._dataset_id = dataset_id
        if _table_id:
            self._table_id = table_id
        
        dataset_ref = '{}.{}'.format(self._project_id, self._dataset_id)
        
        table_ref = '{}.{}.{}'.format(self._project_id, self._dataset_id,
                                      self._table_id)  # 'cloudops-241008.Unicorn.Installation2'
        print('dataset_ref = {}'.format(dataset_ref))
        print('table_ref = {}'.format(table_ref))
        
        if not self._dataset_exists(dataset_ref):
            print('Created dataset: {}'.format(dataset_ref))
            self._create_dataset(dataset_ref)
        
        if not self._table_exists(table_ref):
            if not columns:
                print('The table {} dosent exist you must given a columns list'.format(table_ref))
                return False
            print('Created table: {}'.format(table_ref))
            self._create_table(table_ref, columns)
        
        # print("Get table {}".format(table_ref))
        
        table = self.client.get_table(table_ref)  # API request
        rows_to_insert = data

        if rows_to_insert:
            print("Loading {} rows...".format(len(rows_to_insert)))
            errors = self.client.insert_rows(table, rows_to_insert)  # API request
            print("----- Done ----")
        else:
            print('No rows to upload')
        
    def _delete_table(self,table_ref):
        '''
        
        :param: table_ref (google.cloud.bigquery.table.TableReference):
                A reference to the dataset to look for.
                
        '''
        print('Deleting old table {}'.format(table_ref))
        self.client.delete_table(table_ref)
        
        
