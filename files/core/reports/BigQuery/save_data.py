#!/usr/bin/python3.6
import os
import sys
import requests
import json
# todo: replace
# from connector import BigQuery
import datetime
import subprocess

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core/reports/BigQuery', '')
sys.path.append(home_dir)
from core.reports.BigQuery.connector import BigQuery
from core.utils import read_yaml

rundeck_url = None
_token = None


def get_datacenter_name():
	""" Looking for datacenter name from enviroments
	Args : None
	Return : dc name
	"""
	# Check on which DC i'm running, in order to run locally
	p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
	dc_name, err = p.communicate()
	dc_name = dc_name.decode('utf-8')
	dc_name = dc_name.split('\n')[1].split('=')[1]
	if dc_name.lower() == "tlv":
		dc_name = ""
	dc_name = dc_name.lower()
	dc_map = {'virginia': 'va', 'oakland': 'ca', 'amsterdam': 'am', 'london': 'lo', 'sydney': 'sy', 'melbourne': 'me'}
	
	return dc_map.get(dc_name)


unicorn_url = 'http://{}.unicorn.int.liveperson.net/'.format(get_datacenter_name())


class Api:
	def __init__(self, base, user=None, pwd=None, headers={"Accept": "application/json"}):
		self.__base = base
		self.__user = user
		self.__pwd = pwd
		self.__headers = headers
	
	# self.headers = {"Accept": "application/json"}
	# self.__proxies = self.__get_proxy()
	
	def Get_Response(self, option):
		url = ''.join([self.__base, option])
		try:
			response = requests.get(url, auth=(self.__user, self.__pwd), headers=self.__headers, verify=False)
			#   ,proxies=self.__proxies)
			if response.status_code != 200:
				logger.error(response.json()['error']['message'])
				return False  # sys.exit(1)
		
		except requests.exceptions.RequestException as e:
			logger.error(e)
			print('Error: Unable to connect with api')
			return False  # sys.exit(1)
		
		return response.json()
	
	def get_Response_base(self, option):
		url = ''.join([self.__base, option])
		try:
			response = requests.get(url, headers=self.__headers, verify=False)
			#   ,proxies=self.__proxies)
			if response.status_code != 200:
				return json.loads(response.text)
			return response.json()
		
		except requests.exceptions.RequestException as e:
			logger.error(e)
			return {'Error': 'Unable to connect with api'}
		
		except Exception as e:
			logger.error(e)
			return {'Error': 'Unable to connect with api'}
		return response.json()
	
	def post_data(self, option, data):
		url = ''.join([self.__base, option])
		logger.info(url)
		try:
			response = requests.post(url, auth=(self.__user, self.__pwd), headers=self.__headers, data=str(data))
			# ,proxies=self.__proxies).json()
			# if 'error' in response.keys():
			if response.status_code != 201:
				response = response.json()
				logger.error(response)
				return response
			# sys.exit(1)
			
			return response.json()
		
		except requests.exceptions.RequestException as e:
			logger.error(e)
			# print('Error: Unable to connect with api')
			return False
		# sys.exit(1)
		
		except Exception as e:
			logger.error(e)
			return False
		
		return response


def get_request(option):
	api = Api(unicorn_url)
	response = api.get_Response_base(option)
	if 'Error' in response.keys():
		for k, v in response.items():
			print(k + ": " + v)
		# [print(k + ": " + v) for k, v in response.items()]
		exit(1)
	return response


def _get_vars():
	_dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
	if type(_dict_fields) is not dict:
		return False
	
	global _token, rundeck_url
	# todo: replace
	# _token = _dict_fields['rundeck']['rundeck_token']
	_token = "olSbH2PjBgLWK1qR6OlEqnDvYifEgtS8"
	rundeck_url = "https://" + _dict_fields['rundeck']['rundeck_vip'] + '/api/30'
	return True


def get_data(job_name, days, mode, status=None):
	lst = get_execution_by_days(job_name, days, status=status)
	# lst = get_execution_by_days('Deco_Server_by_HostName','succeeded',days)
	# lst = get_installation_by_days(days)
	
	data = list()
	serials = list()
	if lst:
		
		for job in lst:
			if mode == 'install':
				# if job.get('job', {}).get('options', {}).get('SerialNo', None) not in serials and 'tst' not in job.get(
				#         'job', {}).get('options', {}).get('hostname', ''):
				data.append(
					(job.get('id', 9999),
					 job.get('user', ''),
					 datetime.datetime.strptime(job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ"),
					 job.get('status', 'Unknow'),
					 job.get('job', {}).get('options', {}).get('DataCenter', ''),
					 job.get('job', {}).get('options', {}).get('SerialNo', ''),
					 job.get('job', {}).get('options', {}).get('Owner', ''),
					 job.get('job', {}).get('options', {}).get('Flavor_name', ''),
					 int((datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
					                                 "%Y-%m-%dT%H:%M:%SZ") - datetime.datetime.strptime(
						 job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ")).seconds / 60),
					 int(job.get('job', {}).get('options', {}).get('Price', '0'))
					 ))
			# serials.append(job.get('job', {}).get('options', {}).get('SerialNo', None))
			elif mode == 'deco':
				data.append(
					(job.get('id', 9999),
					 job.get('user', ''),
					 datetime.datetime.strptime(job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ"),
					 job.get('status', 'Unknow'),
					 job.get('job', {}).get('options', {}).get('DataCenter', ''),
					 job.get('job', {}).get('options', {}).get('SerialNo', ''),
					 job.get('job', {}).get('options', {}).get('Owner', ''),
					 int((datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
					                                 "%Y-%m-%dT%H:%M:%SZ") - datetime.datetime.strptime(
						 job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ")).seconds / 60),
					 ))
		
		return data


def get_execution_by_days(job_name, days, status=None):
	'''
	:param job_name: job name in rundeck
	:param status: running / failed / successed
	:param days: number of days
	:return:
	'''
	_get_vars()
	headers = {
		'Accept': "application/json",
		'Content-Type': "application/json",
		'X-Rundeck-Auth-Token': _token,
		'cache-control': "no-cache"
	}
	
	if status:
		url = "{url}/project/Install_Anything//executions?jobFilter={jobName}&max=99999&statusFilter={status}&recentFilter={days}".format(
			url=rundeck_url, jobName=job_name, status=status, days=days)
	else:
		url = "{url}/project/Install_Anything/executions?jobFilter={jobName}&recentFilter={days}d&max=99999".format(
			url=rundeck_url, jobName=job_name, days=days)
	
	response = requests.get(url, headers=headers)
	data = json.loads(response.text).get('executions', None)
	
	return data


def get_and_upload_data(project_name, dataset_name, table_name, mode, days, path=None, status=None):
	col_map = {'install': [
		("job_id", "INTEGER", "REQUIRED"),
		("user", "STRING", "REQUIRED"),
		("start_date", "DATETIME", "REQUIRED"),
		("Status", "STRING", "REQUIRED"),
		("DataCenter", "STRING", "REQUIRED"),
		("SerialNo", "STRING", "REQUIRED"),
		("Owner", "STRING", "REQUIRED"),
		("Flavor_Name", "STRING", "REQUIRED"),
		("Running_Time", "INTEGER", "REQUIRED"),
		("Price", "INTEGER", "REQUIRED")
	
	], 'deco': [
		("job_id", "INTEGER", "REQUIRED"),
		("user", "STRING", "REQUIRED"),
		("start_date", "DATETIME", "REQUIRED"),
		("Status", "STRING", "REQUIRED"),
		("DataCenter", "STRING", "REQUIRED"),
		("SerialNo", "STRING", "REQUIRED"),
		("Owner", "STRING", "REQUIRED"),
		("Running_Time", "INTEGER", "REQUIRED"),
	]}
	job_name_map = {'install': 'Install_Server_by_Serial', 'deco': 'Deco_Server_by_HostName'}
	
	col = col_map.get(mode)
	rows_to_insert = get_data(job_name_map[mode], days, mode, status=status)
	_table_ref = '{}.{}.{}'.format(project_name, dataset_name, table_name)
	upload_data(rows_to_insert, project_name, dataset_name, table_name, col, path)


def upload_data(data, project_name, dataset_name, table_name, col, path=None):
	_table_ref = '{}.{}.{}'.format(project_name, dataset_name, table_name)
	# todo: replace
	if path:
		my_conn = BigQuery(project_name, dataset_id=dataset_name, table_id=table_name, path=path)
	else:
		my_conn = BigQuery(project_name, dataset_id=dataset_name, table_id=table_name)
	
	my_conn._delete_table(_table_ref)
	my_conn.insert_rows_2_table(data, columns=col)


import schedule #todo: add to requirements
import time



def get_flavors():
	response = get_request('flavor_cpacity')
	response = response['flavor']
	print('\nflavors list:\n')
	lst_data = []
	lst_flavors = sorted(response, key=lambda k: k['Flavor_Name'])
	
	lst_data = []
	for flavor in lst_flavors:
		lst_data.append(
			(flavor.get('Flavor_Name', ''),
			 flavor.get('DiskSize', ''),
			 flavor.get('TotalDisks', ''),
			 flavor.get('TotalMemory'),
			 int(flavor.get('CpuCores', '20')) * 2,
			 flavor.get('Num_Of_Boot_Disks'),
			 flavor.get('Boot_Disk_Raid'),
			 flavor.get('Data_Disk_Raid'),
			 str(flavor.get('is_ssd', 'False')),
			 '$' + str(flavor.get('Price_int', '')),
			 str(flavor.get('Virginia', '0')),
			 str(flavor.get('Oakland', '0')),
			 str(flavor.get('London', '0')),
			 str(flavor.get('Amsterdam', '0')),
			 str(flavor.get('Sydney', '0')),
			 str(flavor.get('Melbourne', '0'))
			 )
		)
	return lst_data

def update_flavor(project_name,dataset_name,table_name):
    col = [
        ("Flavor_Name", "STRING", "REQUIRED"),
        ("DiskSize", "INTEGER", "REQUIRED"),
        ("TotalDisks", "INTEGER", "REQUIRED"),
        ("TotalMemory", "INTEGER", "REQUIRED"),
        ("Cpu_Cores", "INTEGER", "REQUIRED"),
        ("Num_Of_Boot_Disks", "STRING", "REQUIRED"),
        ("Boot_Disk_Raid", "STRING", "REQUIRED"),
        ("Data_Disk_Raid", "STRING", "REQUIRED"),
        ("Is_SSD", "STRING", "REQUIRED"),
        ("Price", "STRING", "REQUIRED"),
        ("Virginia", "INTEGER", "REQUIRED"),
        ("Oakland", "INTEGER", "REQUIRED"),
        ("London", "INTEGER", "REQUIRED"),
        ("Amsterdam", "INTEGER", "REQUIRED"),
        ("Sydney", "INTEGER", "REQUIRED"),
        ("Melbourne", "INTEGER", "REQUIRED")
    
    ]
    rows_to_insert = get_flavors()
    
    table_ref = '{}.{}.{}'.format(project_name, dataset_name, table_name)
    my_conn = BigQuery(project_name, dataset_id=dataset_name, table_id=table_name,
                       path='/lp_unicorn/unicorn/files/variables/gcp_creds.json')
    my_conn._delete_table(table_ref)
    # my_conn._get_table(table_ref)
    my_conn.insert_rows_2_table(rows_to_insert, columns=col)
    # for i in range(len(rows_to_insert)):
    #     pass

def run():
    get_and_upload_data("cloudops-241008", "Unicorn", "All_Installation_3", 'install', 1000000,
                        path='/lp_unicorn/unicorn/files/variables/gcp_creds.json')
    update_flavor("cloudops-241008", "Unicorn", "Flavor_Capacity_reg_new")
    


if __name__ == '__main__':
	#     # upload_data("cloudops-241008.Unicorn.All_Decommission")
	#     in_progress_jobs("cloudops-241008","Unicorn","In_progress")
	#     get_and_upload_data("cloudops-241008", "Unicorn", "In_progress_installation",'install')
	#     get_and_upload_data("cloudops-241008", "Unicorn", "In_progress_decommission",'deco')
	####### todo:
	
	# delete_table("cloudops-241008", "Unicorn", "All_Installation_2",
	#                         path='/lp_unicorn/unicorn/files/variables/gcp_creds.json')
	if get_datacenter_name() == 'ca':
		run()
		schedule.every().hour.do(run)
		
		while 1:
			schedule.run_pending()
			time.sleep(1)
####### todo:

# get_and_upload_data("cloudops-241008", "Unicorn", "All_Installation_2", 'install', 1000000,
#                     path='/lp_unicorn/unicorn/files/variables/gcp_creds.json')


# get_and_upload_data("cloudops-241008", "Unicorn", "All_Decommission",'deco',965)



