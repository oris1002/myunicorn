import requests
import logging
import json
import subprocess

class Api:
    def __init__(self, base, user=None, pwd=None, headers={"Accept": "application/json"}):
        self.__base = base
        self.__user = user
        self.__pwd = pwd
        self.__headers = headers
        # self.headers = {"Accept": "application/json"}
        # self.__proxies = self.__get_proxy()

    def Get_Response(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, auth=(self.__user, self.__pwd), headers=self.__headers, verify=False)
            #   ,proxies=self.__proxies)
            if response.status_code != 200:
                logger.error(response.json()['error']['message'])
                return False  # sys.exit(1)

        except requests.exceptions.RequestException as e:
            logger.error(e)
            print('Error: Unable to connect with api')
            return False  # sys.exit(1)

        return response.json()

    def get_Response_base(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, headers=self.__headers, verify=False)
            #   ,proxies=self.__proxies)
            if response.status_code != 200:
                return json.loads(response.text)
            return response.json()

        except requests.exceptions.RequestException as e:
            logger.error(e)
            return {'Error': 'Unable to connect with api'}

        except Exception as e:
            logger.error(e)
            return {'Error': 'Unable to connect with api'}
        return response.json()

    def post_data(self, option, data):
        url = ''.join([self.__base, option])
        logger.info(url)
        try:
            response = requests.post(url, auth=(self.__user, self.__pwd), headers=self.__headers, data=str(data))
            # ,proxies=self.__proxies).json()
            # if 'error' in response.keys():
            if response.status_code != 201:
                response = response.json()
                logger.error(response)
                return response
                # sys.exit(1)
    
            return response.json()
    
        except requests.exceptions.RequestException as e:
            logger.error(e)
            # print('Error: Unable to connect with api')
            return False
            # sys.exit(1)
    
        except Exception as e:
            logger.error(e)
            return False
    
        return response

def get_datacenter_name():
    """ Looking for datacenter name from enviroments
    Args : None
    Return : dc name
    """
    # Check on which DC i'm running, in order to run locally
    p = subprocess.Popen ('set | grep -i  datacenter', shell=True , stdout=subprocess.PIPE)
    dc_name , err = p.communicate ()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    if dc_name.lower() == "tlv":
        dc_name = ""
    dc_name = dc_name.lower()
    dc_map = {'virginia':'va','oakland':'ca','amsterdam':'am','london':'lo','sydney':'sy','melbourne':'me'}
    
    return dc_map.get(dc_name)

unicorn_url = 'http://{}.unicorn.int.liveperson.net/'.format(get_datacenter_name())

def get_request(option):
    api = Api(unicorn_url)
    response = api.get_Response_base(option)
    if 'Error' in response.keys():
        for k, v in response.items():
            print(k + ": " + v)
        # [print(k + ": " + v) for k, v in response.items()]
        exit(1)
    return response

def build_html():
    response = get_request('flavor_cpacity')
    response = response['flavor']
    print('\nflavors list:\n')
    lst_data = []
    lst_flavors = sorted(response, key=lambda k: k['Flavor_Name'])


    lst_data = []
    for flavor in lst_flavors:
        lst_data.append(
            (flavor.get('Flavor_Name', ''),
             flavor.get('DiskSize', ''),
             flavor.get('TotalDisks', ''),
             flavor.get('TotalMemory'),
             flavor.get('CpuCores', '20'),
             flavor.get('Num_Of_Boot_Disks'),
             flavor.get('Boot_Disk_Raid'),
             flavor.get('Data_Disk_Raid'),
             flavor.get('is_ssd', 'False'),
             '$' + str(flavor.get('Price_int', '')),
             str(flavor.get('Virginia', '0')),
             str(flavor.get('Oakland', '0')),
             str(flavor.get('London', '0')),
             str(flavor.get('Amsterdam', '0')),
             str(flavor.get('Sydney', '0')),
             str(flavor.get('Melbourne', '0'))
             )
        )
    headers = ('Flavor Name', 'DiskSize', 'TotalDisks', 'TotalMemory', 'Cpu_Cores',
               'Num_Of_Boot_Disks', 'Boot_Disk_Raid', 'Data_Disk_Raid',
               'Is SSD', 'Price', 'Virginia', 'Oakland', 'London', 'Amsterdam', 'Sydney',
               'Melbourne')

    import tablib
    style = '''
            <style>
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
              width: 100%;
            }

            td, th {
              border: 1px solid #dddddd;
              text-align: left;
              padding: 8px;
            }

            tr:nth-child(even) {
              background-color: #dddddd;
            }
            </style>'''
    with open('/tmp/flavor_capacity.html', 'w') as f:
        f.write('')
    with open('/tmp/flavor_capacity.html', 'a') as f:
        data = tablib.Dataset(*lst_data, headers=headers)
        f.write(style)
        #f.write((data.html))


from send_email import send_mail


def get_datacenter_name():
    """ Looking for datacenter name from enviroments
	Args : None
	Return : dc name
	"""
    # Check on which DC i'm running, in order to run locally
    p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    if dc_name.lower() == "tlv":
        dc_name = ""
    dc_name = dc_name.lower()
    dc_map = {'virginia': 'va', 'oakland': 'ca', 'amsterdam': 'am', 'london': 'lo', 'sydney': 'sy', 'melbourne': 'me'}
    
    return dc_map.get(dc_name)

if __name__ == '__main__':
    # build html table of flavor capacity
    if get_datacenter_name() == 'ca':
        build_html()
        send_mail(filename="/tmp/flavor_capacity.html",subject="Unicorn Reports - Flavors Capacity")
