import requests
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()
# import logger
import sys
import json
import time
import logging
logging.getLoggerClass()


class Api:
    def __init__(self, base, user=None, pwd=None,headers={"Accept": "application/json"}):
        self.__base = base
        self.__user = user
        self.__pwd = pwd
        self.__headers = headers
        # self.headers = {"Accept": "application/json"}
        # self.__proxies = self.__get_proxy()
        
    
    def Get_Response(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, auth=(self.__user, self.__pwd), headers=self.__headers, verify=False)
                                 #   ,proxies=self.__proxies)
            retries = 1
            while response.status_code != 200 and retries < 4:
                response = requests.get(url, auth=(self.__user, self.__pwd), headers=self.__headers, verify=False)
                #   ,proxies=self.__proxies)
                retries += 1
                time.sleep(10) #sleep 10 sec
            
            if response.status_code != 200:
                logging.error(response.json()['error']['message'])
                return False # sys.exit(1)
            
            return response.json()

        except requests.exceptions.RequestException as e:
            logging.error(e)
            print('Error: Unable to connect with api')
            return False  # sys.exit(1)
        
        
    def get_Response_base(self, option):
        url = ''.join([self.__base, option])
        try:
            response = requests.get(url, headers=self.__headers, verify=False)
            #   ,proxies=self.__proxies)

            retries = 1
            while response.status_code != 200 and retries < 4:
                response = requests.get(url, headers=self.__headers, verify=False)
                retries += 1
                time.sleep(10) #sleep 10 sec

            if response.status_code != 200:
                return json.loads(response.text)
            
            return response.json()
        except requests.exceptions.RequestException as e:
            logging.error(e)
            # print('Error: Unable to connect with api')
            return {'Error': 'Unable to connect with api'}
            # return False
            # sys.exit(1)

        except Exception as e:
            logging.error(e)
            # print('Error: Unable to connect with api')
            return {'Error': 'Unable to connect with api'}
            # return False
            # sys.exit(1)
        


    def post_data(self, option, data):
        url = ''.join([self.__base, option])
        logging.info(url)
        try:
            response = requests.post(url, auth=(self.__user, self.__pwd), headers=self.__headers, data=str(data), verify=False)
                                     #,proxies=self.__proxies).json()
            # if 'error' in response.keys():
            
            retries = 1
            while response.status_code != 201 and retries < 3:
                response = requests.post(url, auth=(self.__user, self.__pwd), headers=self.__headers, data=str(data),
                                         verify=False)
                retries += 1
                time.sleep(10) #sleep 10 sec



            if response.status_code != 201:
                response = response.json()
                logging.error(response)
                return response
                # sys.exit(1)
            
            return response.json()
        
        except requests.exceptions.RequestException as e:
            logging.error(e)
            # print('Error: Unable to connect with api')
            return False
            # sys.exit(1)

        except Exception as e:
            logging.error(e)
            return False
    

    def __get_proxy(self):
        import os
        mapping_dc = {'v': 'proxy-va',
                      'l': 'proxy-uk',
                      'o': 'proxy-ca',
                      'a': 'proxy-nl',
                      's': 'proxy-sy',
                      'm': 'proxy-me'}
        dc_char = os.uname()[1][1]
        return {'https': 'https://{}:8080'.format(mapping_dc.get(dc_char, ''))}
