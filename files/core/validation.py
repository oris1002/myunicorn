import os
import sys
import yaml
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')
root_dir = '/tmp/unicorn'
sys.path.append(home_dir)
from core.get_functions import *
from get_functions import *
import utils
'''
Validation Functions
'''

# def global_variables(dc):
#     global LIMIT_STOCK, spider_dc, hostname_conv,segments
#     variables = utils.read_yaml(home_dir + '/variables/variables.yaml')
#
#     # LIMIT_STOCK = variables['limit'][dc]
#     # spider_dc = variables['spider_dc'][dc]
#     # hostname_conv = variables['name_conv'][dc]
#     segments = variables['segments']


def validation_image(image):
    print('Start Validation Image function')
    print(image + ' inserted')
    lst_images = get_image()
    
    if type(lst_images) is dict:
        print(lst_images)
        return False
    
    if image not in lst_images:
        print("{}, Invalid image. please select one from following list:{}".format(image, '\n'.join(lst_images)))
        return False # exit(1)
    
    return True

def validation_profile(profile):
    lst_profile = get_profile_by_name()
    if type(lst_profile) is dict:
        print(lst_profile)
        return False
    
    if profile not in lst_profile:
        print("{}, Invalid profile. please select one from following list:{}".format(profile,'\n'.join(lst_profile)))
        return False  # exit(1)
    return True


def validation_yaml(path,profile,flavor_path):
    print('Start validation_yaml function')
    if check_profile_exist(profile):
        with open(path, 'r') as stream:
            with open(flavor_path,'r') as flavor_info:
                try:
                    global dict_param
                    dict_param = (yaml.safe_load(stream))
                    dict_param = dict_param
                    dict_param.update(yaml.safe_load(flavor_info))
                    print('End validation_yaml function')
                    return dict_param
                except yaml.YAMLError as exc:
                    print(exc)
    else:
        print('Error: No yaml file for this profile')
        return {'Error': 'No yaml file for this profile'}


def check_profile_exist(profile_name):
    root_dir = '/tmp/unicorn'
    if not os.path.exists("/tmp/{}/profiles".format('unicorn')):
        utils.git_no_print('/tmp', 'unicorn')
    path = '{}/profiles/{}.yaml'.format(root_dir,profile_name.lower())
    if not os.path.isfile(path):
        return False
    return path

def check_flavor_exist(flavor):
    root_dir = '/tmp/unicorn'
    if not os.path.exists("/tmp/{}/flavors".format('unicorn')):
        utils.git_no_print('/tmp', 'unicorn')
        
    path = '{}/flavors/{}.yaml'.format(root_dir,flavor.lower())
    if not os.path.isfile(path):
        return False
    return path


def check_datacente(dc):
    lst_dcs = get_dc()
    if dc.title() not in lst_dcs:
        print('{} invalid datacenter'.format(dc))
        return False
    return True

def check_segment(dict_fields,segment):
    segments = dict_fields['segments']
    if segment not in segments.keys():
        return False
    return segments[segment]


def validation_hostname(dc,hostname,type='physical'):
    variables = utils.read_yaml(home_dir + '/variables/variables.yaml')
    print('conv dc=> {}'.format(variables['name_conv'][dc]))
    print('conv type => {}'.format(variables['type'][type]))
    print(type,hostname,dc)
    nameconv = variables['name_conv'][dc] + variables['type'][type]
    return hostname.startswith(nameconv)

import re
def validation_convention_hostnmae(data):
    name_conv = None
    if len(set(data['hostname'].split(','))) == 1: # insert one name --hostname raor-ori / raor-ori101
        for name in set(data['hostname'].split(',')):
            regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}[0-9]{3}" #with number
            print('regex' + regex)
            print(name)
            if not re.match(regex, name):  # qtor-xxx999
                regex = r"[q|s|r|a][t|v|o|l|a|s|m][p|v|o][e|s|a|u|r]-[a-z]{3}" #no number
                if not re.match(regex, name):  # qtor-xxx
                    print('Error:{} Invalid convention name'.format(name))
                    return {'Error': '{} Invalid convention name. Should be like qtor-xxx'.format(name)},None
                # qtor-xxx
                data['Hostname_connvention'] = data['hostname'].split('-')[1]
                name_conv = data['hostname'].split('-')[0]
                data['hostname'] = None
            else:
                # data['hostname'] = data['hostname']
                print("$%$%$%$%")
                print(data['hostname'].split('-'))
                name_conv = data['hostname'].split('-')[0]
                
    return data,name_conv