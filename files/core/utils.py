import os
import subprocess
import yaml
import sys
import glob
import datetime
from datetime import timedelta,datetime
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')
sys.path.append(home_dir)
from flask import current_app as app
from logging import getLogger
logger = getLogger()



def get_last_12h_files(path,pattern='*'):
    files = glob.glob('{}/{}'.format(path,pattern))
    files.sort(key=os.path.getctime)
    #print("\n".join(files))
    files.reverse()
    TODAY_CHECK = datetime.now()
    print(TODAY_CHECK)
    TWELVE_AGO = TODAY_CHECK - timedelta(hours=12, minutes=0)
    print(TWELVE_AGO)
    tmp = list()
    for i in files:
        print(datetime.fromtimestamp(os.path.getctime(i)))
        if TODAY_CHECK >= datetime.fromtimestamp(os.path.getctime(i)) >= TWELVE_AGO:
            print('Inside the rang:  {}'.format(i))
            tmp.append(os.path.basename(i));
        else:
            break
    print(tmp)
    return tmp


def read_yaml(path):
    with open(path, 'r') as stream:
        try:
            return yaml.safe_load(stream)
        
        except yaml.YAMLError as exc:
            # app.logger.error(exc)
            print(exc)
            

dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
def git_no_print(working_dir, repo):
    '''
    Args: working_dir: the directory clone to
            repo: the repository name clone to from lpgithub
    Return: git clone <repo> to <working_dir>/<repo>
    '''
    
    # clone / pull git repository
    try:
        print('get git hub token')
        p = subprocess.Popen('set | grep -i  github_token', shell=True, stdout=subprocess.PIPE)
        dcim_password, err = p.communicate()
        dcim_password = dcim_password.decode('utf-8')
        dcim_password = dcim_password.split('\n')[1].split('=')[1]
        github_token = dcim_password.replace("\'", "")
        print('success get git hub token')
        
        
        working_dir = '{}/{}'.format(working_dir, repo)
        os.system('rm -rf {}'.format(working_dir))
        print('try to clone repo: ' + repo)
        if os.path.exists(working_dir):
            command = 'cd ' + working_dir + ';' + 'git reset --hard origin/master; git pull'
            description = 'Fetching the most recent config from git/{}'.format(repo)
            print('cloned repo: ' + repo)
        else:
            command = 'git clone https://x-token-auth:{}@lpgithub.dev.lprnd.net/Production/{}.git {}'.format(github_token,repo, working_dir)
            description = 'Cloning the config from git/{}'.format(repo)
        exec_subprocess(command, description)
        return True
    
    except KeyboardInterrupt as e:
        print ("\nClone {} repository already started, please wait while finishing clone process..".format(repo))
        command = 'git clone git@lpgithub.dev.lprnd.net:Production/{}.git ' \
                  '{}'.format(repo, working_dir)
        exec_subprocess('rm -rf ' + working_dir, 'remove ansible folder')
        exec_subprocess(command, description)
        # app.logger.error(e)
        return False
    except IOError as e:
        # app.logger.error(e)
        return False
    except Exception  as e:
        # app.logger.error(e)
        return False

        
def exec_subprocess(command, description, debug=False):
    """
    :param command: gets the ansible playbook command from generate_config and commit_config functions.
    :param playbook: gets the playbook name from global variable.
    :return: if success return output command that was running in the playbook.
    """
    try:
        p1 = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True, stderr=subprocess.PIPE)
        output = p1.communicate()[0]
        
        if output and 'up-to-date' not in str(output):
            print(str(output))
            
        if debug:
            print(str(output))
            
        if p1.returncode != 0:
            (out, err) = p1.communicate()
            print('The command {} failed to run, contact network team out={}, err={}'.format(description, str(out), str(err)))
            
        return True
    
    except IOError as e:
        app.logger.error(e)
        return False
    except Exception as e:
        app.logger.error(e)
        return False

def get_dc():
    # Check on which DC i'm running, in order to run locally
    print('Get DC')
    p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    # dict_fields['datacenter'] = dc_name.title()
    return dc_name.title()
