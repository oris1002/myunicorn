from flask import Flask,jsonify
from flask import request
from flask_restplus import Resource, Api
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity

import logging.handlers
from verify_ad_auth import verify_auth
import os
import sys
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')

app = Flask('Unicorn_app')
app.config['JWT_SECRET_KEY'] = 'super-secret'
api = Api(app)
jwt = JWTManager(app)

import get_functions as get
import post_function as post



###
######################### GET ######################
# Any valid JWT can access this endpoint
@app.route('/datacenter', methods=['GET'])
def datacenter():
    # return all Dcs (from DCIM )
    app.logger.info('Get request - Datacenter')
    result = get.get_dc()

    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500

    app.logger.info('Success Get - Datacenter')
    return jsonify({'dc': result}), 200

@app.route('/image', methods=['GET'])
def image():
    # return a list of all images ( from cobbler )
    app.logger.info('Get request - image')
    result = get.get_image()
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - image')
    return jsonify({'image': result}), 200

@app.route('/profile', methods=['GET'])
def profile():
    # return a list of all profiles name (DCIM Owner)
    app.logger.info('Get request')
    result = get.get_profiles()
    if type(result) is dict: # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - profile')
    return jsonify({'profile': result}), 200 #OK


@app.route('/profile/<string:name>', methods=['GET'])
def profile_by_name(name):
    # return the values of profile by name
    app.logger.info('Get request - profile by Name({})'.format(name))
    result = get.get_profile_by_name(name)
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - profile by Name')
    return jsonify({'profile': result[1]}), 200


@app.route('/flavor_cpacity', methods=['GET'])
def flavor_cpacity():
    # return values of all flavors
    app.logger.info('Get request')
    result = get.get_flavor_capacity()
    if type(result) is dict: # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - get_flavor_capacity')
    return jsonify({'flavor': result}), 200

@app.route('/flavor_cpacity_build', methods=['GET'])
def flavor_cpacity_build():
    # return values of all flavors
    app.logger.info('Get request')
    result = get.get_flavors_capacity_build()
    if type(result) is dict: # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - flavor')
    return jsonify({'flavor': result}), 200

# get_jobs_deatils_by_user
@app.route('/show_jobs/<string:user>', methods=['GET'])
def show_jobs(user):
    # return jobs
    app.logger.info('Get request')
    result = get.get_jobs_deatils_by_user(user=user)
    if type(result) is dict: # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - flavor')
    return jsonify({'jobs': result}), 200

@app.route('/flavor', methods=['GET'])
def flavor():
    # return values of all flavors
    app.logger.info('Get request')
    result = get.get_flavors()
    if type(result) is dict: # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - flavor')
    return jsonify({'flavor': result}), 200


@app.route('/flavor/<string:name>', methods=['GET'])
def flavor_by_name(name):
    # return the values flavor by name
    app.logger.info('Get request - profile by Name({})'.format(name))
    result = get.get_flavor_by_name(name)
    
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    
    app.logger.info('Success Get - profile by Name')
    return jsonify({'flavor': result}), 200

@app.route('/status_jobs/<string:user>', methods=['GET'])
def status_jobs(user):
    # return the jobs status by user (from swift )
    app.logger.info('Get request - show status jobs for user({})'.format(user))
    result = get.get_status(user)
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    
    app.logger.info('Success Get - show status bos for' + user)
    return jsonify({'jobs': result}), 200

@app.route('/devices', methods=['GET'])
def devices():
    # get the devices from DCIM
    app.logger.info('Get request - Device')
    app.logger.info('by filter => {}'.format(request.args.to_dict()))
    result = get.get_devices(request.args.to_dict())
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - devices')
    return jsonify({'device': result}), 200
      ####### Show Openstack #########

@app.route('/openstack/flavors', methods=['GET'])
def openstack_flavors():
    # return values of all flavors
    app.logger.info('Get request openstack_flavors')
    result = get.get_openstack_flavors()
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - flavor')
    return jsonify({'flavor': result}), 200

@app.route('/openstack/images', methods=['GET'])
def openstack_images():
    # return values of all flavors
    app.logger.info('Get request openstack_images')
    result = get.get_openstack_images()
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - images')
    return jsonify({'image': result}), 200

@app.route('/openstack/projects', methods=['GET'])
def openstack_projects():
    # return values of all flavors
    app.logger.info('Get request openstack_projects')
    result = get.get_openstack_projects()
    if type(result) is dict:  # return dict in failed {'Error':message }
        app.logger.error(result)
        return jsonify(result), 500
    app.logger.info('Success Get - projects')
    return jsonify({'project': result}), 200
      ####### Show Openstack #########
######################### GET ######################


######################### POST ######################
@app.route('/create', methods=['POST'])
@jwt_required
def create():
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Post - Create by => ' + request.json['user'])
    result = post.create(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - Create New server')
    return jsonify(result), 201

@app.route('/create_vm', methods=['POST'])
@jwt_required
def create_vm():
    # create_vm
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Post - Create_vm by => ' + request.json['user'])
    result = post.create_vm(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - Create New VM instance')
    return jsonify(result), 201

# create_openstack_instance
@app.route('/create_openstack', methods=['POST'])
@jwt_required
def create_openstack():
    # create_vm
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Post - create_openstack by => ' + request.json['user'])

    if request.json['user'] == 'task_user':
        if request.json.get('token','') != get_task_user_token():
            return {'Error':'Invalid Token'}
    
    if request.json['dc'].lower() in ['telaviv', 'tlv']:
        result = post.create_openstack_instance_tlv(request.json)
    else:
        result = post.create_openstack_instance(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - Create New openstack instance')
    return jsonify(result), 201


# create_clone_openstack
@app.route('/create_clone_openstack', methods=['POST'])
@jwt_required
def create_clone_openstack():
    # create_vm
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Post - create_openstack by => ' + request.json['user'])
    if request.json['user'] == 'task_user':
        if request.json.get('token','') != get_task_user_token():
            return {'Error':'Invalid Token'}
    if request.json['dc'].lower() in ['telaviv', 'tlv']:
        result = post.create_clone_openstack_tlv(request.json)
    else:
        result = post.create_clone_openstack(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - Create New openstack instance')
    return jsonify(result), 201

@app.route('/retry', methods=['POST'])
@jwt_required
def retry():
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Post - runagain by => ' + request.json['user'])
    result = post.retry(request.json['job_id'],request.json['user'])
    if result.get('Error', None):  # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - Create New server')
    return jsonify(result), 201


@app.route('/delete', methods=['POST'])
@jwt_required
def delete():
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Delete - Create by => ' + request.json['user'])
    result = post.delete(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - delete server(s)')
    return jsonify(result), 201


@app.route('/delete_vm', methods=['POST'])
@jwt_required
def delete_vm():
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('Delete - Create by => ' + request.json['user'])
    result = post.delete_vm(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - delete vm server(s)')
    return jsonify(result), 201
    
@app.route('/delete_openstack_tlv', methods=['POST'])
def delete_openstack_tlv():
    # working only with a access token
    # Check and build all parameters and save in swift under spesific uuid
    app.logger.info('delete_openstack_tlv - Create by => ' + request.json['user'])
    
    result = post.delete_openstack_instance(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - delete vm server(s)')
    return jsonify(result), 201

@app.route('/firmware_update', methods=['POST'])
@jwt_required
def firmware_update():
    app.logger.info('Firmware update - Create by => ' + request.json['user'])
    result = post.server_firmware_update(request.json)
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return jsonify(result), 500
    app.logger.info('Success Get - delete server(s)')
    return jsonify(result), 201


@app.route('/install', methods=['POST'])
@jwt_required
def install():
    # working only with a access token
    app.logger.info('Post - Install by uuid => ' + request.json['uuid'])
    mode = request.json.get('mode','install')
    result = post.instaling(mode,request.json.get('uuid'))
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return result,500
    app.logger.info('Success Get - Install New server')
    return jsonify(result), 201

@app.route('/install_tlv', methods=['POST'])
def install_tlv():
    # working only with a access token
    app.logger.info('Post - Install by uuid => ' + request.json['uuid'])
    mode = request.json.get('mode','install')
    result = post.instaling(mode,request.json.get('uuid'))
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return result,500
    app.logger.info('Success Get - Install New server')
    return jsonify(result), 201

@app.route('/cancel', methods=['POST'])
def canceled_request():
    canceled_request
    app.logger.info('Post - Cancell by uuid => ' + request.json['uuid'])
    result = post.canceled_request(request.json.get('uuid'))
    if result.get('Error',None): # if return with error
        app.logger.error('{}'.format(result))
        return result,500
    app.logger.info('Success Get - Install New server')
    return jsonify(result), 201

@app.route('/authentication', methods=['POST'])
def authentication():
    # check authentication in AD
    user = request.json.get('username')
    app.logger.info('Check authentication for=> ' + user)
    # call to function and split the values like to send a, b
    if verify_auth(**(request.json)):
        app.logger.info('Createing an access_token for=> ' + user)
        access_token = create_access_token(identity= user)
        return jsonify({'access_token': access_token}), 201
    
    app.logger.error('Authentication Failed for=> ' + user)
    return jsonify({'Error':'Authentication Failed'}), 500





if __name__ == '__main__':

    handler = logging.StreamHandler(sys.stdout)
    # formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    app.logger.setLevel(logging.INFO)
    
    app.run(debug=False,port=8081,host='0.0.0.0')

'''
Examples :
        app.logger.warning('A warning occurred (%d apples)', 42)
        app.logger.error('An error occurred')
        app.logger.info('Info')
'''
