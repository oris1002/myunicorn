import utils
import sys
import os

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core', '')
sys.path.append(home_dir)
from dcim_api import *
import glob
import re
import yaml
from pathlib import Path


def global_variables(dc, dict_fields={}):
	global LIMIT_STOCK, spider_dc, hostname_conv, black_list
	dict_fields = utils.read_yaml(home_dir + '/variables/variables.yaml')
	
	LIMIT_STOCK = dict_fields['limit'].get(dc, 0)
	spider_dc = dict_fields['spider_dc'].get(dc, None)
	hostname_conv = dict_fields['name_conv'].get(dc, 'xxxx') + dict_fields['type'][dict_fields.get('mode', 'physical')]
	segments = dict_fields['segments']
	black_list = dict_fields['black_list']
	# lock_serials = dict_fields['lock_serials']
	# lock_hostnames = dict_fields['lock_hostnames']


# todo: no matter how much parameters get for check
def check_parameters_servers(servers, disk_size, total_disks, cpu_cores=20, total_memory=128, is_ssd=False,
                             is_nvme=False,
                             hw_type=None):

	match_servers = []
	print("Start function => check_parameters_servers")
	for server in servers:
		print("check if match {}".format(server[0]['SerialNo']))
		# DiskNVMeSize
		if not server[0]['DiskNVMeSize'].isdigit() or not server[0]['DiskSSDSize'].isdigit() or not server[0][
			'DiskSSDSize'].isdigit() or not server[0]['DiskSize'].isdigit() or not server[0].get('TotalMemory',
		                                                                                         '0GB').replace('GB',
		                                                                                                        '').isdigit():
			print("{} - doesnt match #1 condition".format(server[0]['SerialNo']))
			continue
		if server[0]['Health'] != 'OK' or int(server[0].get('CpuCores', 0)) < cpu_cores or float(
				server[0].get('TotalMemory', '0GB').replace('GB', '')) < total_memory:
			print('health = {},server cpu = {}, cpu check = {}, servermemory = {},memory check = {}'.format(
				server[0]['Health'], int(server[0]['CpuCores']), cpu_cores, float(
					server[0]['TotalMemory'].replace('GB', '')), total_memory))
			print("{} - doesnt match #2 condition".format(server[0]['SerialNo']))
			continue
		
		if is_ssd:
			if server[0]['SSDDISKS'] != 'YES' or int(server[0].get('DiskSSDSize', 0)) != disk_size or int(
					server[0].get('TotalSSDDisks', 0)) != total_disks:
				print(
					"SSD server disk type = {}, server disk size = {} , disk size check = {} server  total disk = {}, total disk check = {}".format(
						server[0]['SSDDISKS'], int(server[0]['DiskSSDSize']), disk_size, server[0]['TotalSSDDisks'],
						total_disks))
				print("{} - doesnt match #3 condition".format(server[0]['SerialNo']))
				continue
		elif is_nvme:
			if server[0]['NVMeDisks'] == 'YES':
				if int(server[0].get('DiskNVMeSize', 0)) < disk_size or int(
						server[0]['TotalNVMeDisks']) < total_disks:
					print("{} - doesnt match #5 condition".format(server[0]['SerialNo']))
					continue
			else:
				print("{} - doesnt match #6 condition".format(server[0]['SerialNo']))
				continue
		elif int(server[0]['DiskSize']) != disk_size or int(server[0]['TotalDisks']) != total_disks or int(server[0][
			                                                                                                   'TotalSSDDisks']) > 0:
			print("server disk size = {}, disk size check = {}, server total disks = {}, total disk check ={}".format(
				int(server[0]['DiskSize']), disk_size, int(server[0]['TotalDisks']), total_disks))
			print("{} - doesnt match #4 condition".format(server[0]['SerialNo']))
			continue
		
		if hw_type:
			if server[0]['WarrantyCo'] != hw_type:
				continue
		# todo: if openstack can be more than 4 disk(>4) and install on 4 disk
		
		match_servers.append(server)
	print("End function => check_parameters_servers")
	return match_servers



def get_devices_in_arista(devices, device_p2, report=False):
	print("Start function => get_devices_in_arista")
	repo_name = 'puppet-ansible_conf'
	lst_git = []
	if utils.git_no_print('/tmp', repo_name):
		# path = '/tmp/{}/files/playbooks/inventories/{}'.format(repo_name, spider_dc)
		# cfiles = recursive_glob(spider_dc, '/tmp/puppet-ansible_conf/', 'leaf.yml')
		for path in Path('/tmp/puppet-ansible_conf').rglob('leaf.yml'):
			if not report:
				print(spider_dc)
				if spider_dc in str(path):
					print(str(path))
					serials = get_serials_arista(str(path))
					if serials:
						lst_git += serials
						# lst_git += get_serials_arista(str(path))
			else:
				print(str(path))
				serials = get_serials_arista(str(path))
				if serials:
					lst_git += serials
				# lst_git += get_serials_arista(str(path))
			#         for path in cfiles:
			#             serials =  get_serials_arista(path)
			#             if serials:
			#                 lst_git += get_serials_arista(path)
			print('CORE: ARISTA SERIAL {}'.format(lst_git))
			print("End function => get_devices_in_arista")
		
		return intersection(devices, lst_git), intersection(device_p2, lst_git)
	print("End function => get_devices_in_arista with error")
	return {'Error': 'Failed to pull ' + repo_name + ' repository'}


def intersection(devices, lst_serials):
	# return list obj are serial exist in both list
	new = []
	for d in devices:
		if d[0].get('SerialNo', '') in lst_serials:
			new.append(d)
	return new


def get_serials_arista(filename):
	# return the serials are exist in spider and in Vlan 30 - default
	partten = '.*serial: (.*)'
	lst_serials = list()
	# for filename in glob.glob('{}/*/*/all.yml'.format(path)):
	
	with open(filename) as f:
		lines = yaml.safe_load(f)
		if lines.get('interfaces', None):
			for line in lines['interfaces']:
				if line.get('port_profile', None) == 'default' and line.get('serial', 'Empty') != 'Empty':
					lst_serials.append(line.get('serial'))
		else:
			return None
	# print(lst_serials)
	return set(lst_serials)


import fnmatch
import os


def recursive_glob(dc, rootdir='.', pattern='*'):
	return [os.path.join(looproot, filename)
	        for looproot, _, filenames in os.walk(rootdir) if '/' + dc + '/' in looproot
	        for filename in filenames
	        if fnmatch.fnmatch(filename, pattern)]


def get_ready_devices(dc, disk_size, total_disks, cpu_cores=20, total_memory=128, quantity_servers=1, is_ssd=False,
                      is_nvme=False,
                      force=False, hw_type=None):
	print("Start function => get_ready_devices")
	group_server = 1
	# create DCIM object instance
	dcim = DcimApi()
	
	devices, devices_priority_2 = dcim.get_stock_servers_by_dc(dc)
	
	if 'Error' not in devices:
		
		devices, devices_priority_2 = get_devices_in_arista(devices, devices_priority_2)
		if not devices and not devices_priority_2:
			return {'Error': 'No devices in stock!'}, None
			# exit(1)
		
		print("stock server capacity {}".format(len(devices) + len(devices_priority_2)))
		print('LIMIT_STOCK=> {}'.format(LIMIT_STOCK))
		
		if len(devices) + len(devices_priority_2) <= LIMIT_STOCK and not force:
			return {'Error': 'No free stock of servers per your request, please contact to DC Eng. team'}, None
		
		# sorted by live time
		devices = sorted(devices, key=operator.itemgetter(1))  # sorted newest to oldest
		devices_priority_2 = sorted(devices_priority_2, key=operator.itemgetter(1))  # sorted newest to oldest
		
		match_devices = list()
		serial_raedy = list()
		
		# looking for in server no expired
		print("Devices for check if match")
		print(devices)
		match_devices += check_parameters_servers(devices, disk_size, total_disks, cpu_cores=cpu_cores,
		                                          total_memory=total_memory, is_ssd=is_ssd, is_nvme=is_nvme,
		                                          hw_type=hw_type)
		print("match devices")
		print(match_devices)
		if not match_devices or len(match_devices) < quantity_servers:
			# looking for in server live more 4 years
			group_server = 2
			match_devices += check_parameters_servers(devices_priority_2, disk_size, total_disks, cpu_cores=cpu_cores,
			                                          total_memory=total_memory, is_ssd=is_ssd, is_nvme=is_nvme,
			                                          hw_type=hw_type)
		
		devices_by_racks = {}
		# check if we have capacity
		if len(match_devices) >= quantity_servers:
			for d in match_devices:
				l = devices_by_racks.get(d[0].get('Cabinet'), [])
				l.append(d)
				devices_by_racks[d[0].get('Cabinet')] = l
			
			if len(devices_by_racks) == 1 or quantity_servers == 1:
				servers = match_devices[0:quantity_servers]
				# servers = random.sample(match_devices, quantity_servers)
				for s in servers:
					rack = dcim.get_rack_device(s[0]['SerialNo'], dc)
					# price = dcim.get_price_device(s[0]['SerialNo'])
					
					if type(rack) is dict:
						if 'Error' in rack.keys():
							return rack
					
					# if type(price) is dict:
					#         return price
					
					print(s[0]['SerialNo'], dc)
					serial_raedy.append((s[0]['SerialNo'], rack))
				return serial_raedy, group_server
			
			# balance servers in racks
			elif len(devices_by_racks) <= quantity_servers:
				lst_cabinat = [[k, len(v)] for k, v in devices_by_racks.items()]
				need_to_install = quantity_servers
				while need_to_install != 0:
					for i, cabinet in enumerate(lst_cabinat):
						if cabinet[1] > 0:
							serial = devices_by_racks[cabinet[0]].pop(0)[0]['SerialNo']
							rack = dcim.get_rack_device(serial, dc)
							# price = dcim.get_price_device(serial)
							if type(rack) is dict:
								if 'Error' in rack.keys():
									return rack
							
							serial_raedy.append((serial, rack))
							lst_cabinat[i] = [cabinet[0], cabinet[1] - 1]
							need_to_install -= 1
						if need_to_install == 0:
							break
			
			print("Start function => get_ready_devices")
			return serial_raedy, group_server
		
		elif not match_devices:
			return {'Error': 'No match devices in stock!'}, None
		else:
			return {'Error': 'No capacity in stock!'}, None
	else:
		return {'Error': devices}, None


def get_match_devices(dc, disk_size, total_disks, cpu_cores=20, total_memory=128, quantity_servers=1, is_ssd=False,
                      is_nvme=False,
                      force=False, hw_type=None, report=False):
	print("Start function => get_ready_devices")
	group_server = 1
	# create DCIM object instance
	dcim = DcimApi()
	
	devices, devices_priority_2 = dcim.get_stock_servers_by_dc(dc)
	print("devices from DCIM")
	print(devices)
	if 'Error' not in devices:
		if not report:
			devices, devices_priority_2 = get_devices_in_arista(devices, devices_priority_2)
		else:
			devices, devices_priority_2 = get_devices_in_arista(devices, devices_priority_2, report)
		if not devices and not devices_priority_2:
			return {'Error': 'No devices in stock!'}, None
			# exit(1)
		print("stock server capacity {}".format(len(devices) + len(devices_priority_2)))
		print('LIMIT_STOCK=> {}'.format(LIMIT_STOCK))
		if len(devices) + len(devices_priority_2) <= LIMIT_STOCK and not (force or report):
			return {'Error': 'No free stock of servers per your request, please contact to DC Eng. team'}, None
		
		# sorted by live time
		devices = sorted(devices, key=operator.itemgetter(1))  # sorted newest to oldest
		devices_priority_2 = sorted(devices_priority_2, key=operator.itemgetter(1))  # sorted newest to oldest
		
		match_devices = list()
		serial_raedy = list()
		
		# looking for in server no expired
		match_devices += check_parameters_servers(devices, disk_size, total_disks, cpu_cores=cpu_cores,
		                                          total_memory=total_memory, is_ssd=is_ssd, is_nvme=is_nvme,
		                                          hw_type=hw_type)
		if not match_devices or len(match_devices) < quantity_servers:
			# looking for in server live more 4 years
			group_server = 2
			match_devices += check_parameters_servers(devices_priority_2, disk_size, total_disks, cpu_cores=cpu_cores,
			                                          total_memory=total_memory, is_ssd=is_ssd, is_nvme=is_nvme,
			                                          hw_type=hw_type)
		if report:
			match_devices += check_parameters_servers(devices_priority_2, disk_size, total_disks, cpu_cores=cpu_cores,
			                                          total_memory=total_memory, is_ssd=is_ssd, is_nvme=is_nvme,
			                                          hw_type=hw_type)
		
		return match_devices, group_server


def looking_4_devices(dict_fields, dc, disk_size, total_disks, quantity_servers, cpu_cores=20, total_memory=128,
                      is_ssd=False, is_nvme=False, force=False, hw_type=None):
	global_variables(dc, dict_fields)
	
	serials, group_servers = get_ready_devices(dc.title(), disk_size, total_disks, cpu_cores, total_memory,
	                                           quantity_servers, is_ssd=is_ssd, is_nvme=is_nvme, force=force,
	                                           hw_type=hw_type)
	# print('your devices are ready:\n\t{}'.format('\n\t'.join(serials)))
	return serials, group_servers