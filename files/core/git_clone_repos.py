'''
git clone to:
1) Unicorn
2) puppet_ansible_conf
3) puppet-lpcobbler
'''
import os, sys
import multiprocessing
import time
from utils import git_no_print

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core','')
sys.path.append(home_dir)

lst_repo_name = ['unicorn','puppet_ansible_conf','puppet-lpcobbler']

def clone_all_repos():
    for i in lst_repo_name:
        git_no_print('/tmp',i)
        
while True:
    try:
        clone_all_repos()
        time.sleep(600)
    except Exception as e:
        print(e)
        exit(1)
    

#
# def foo(repo):
# 	# print('{} pppp'.format(repo))
#     return('{} pppp'.format(repo))
#
# def run_parallel(lst):
#     """ create pool of servers for multiprocessing.
#     Args : server_ilo_list,dictFields,model.
#     Return :
#     """
#
#     list_of_metrics = []
#     cpu_use = multiprocessing.cpu_count()
#     pool = multiprocessing.Pool(processes=cpu_use * 10)
#
#     for e in lst:
#         print(e)
#         pool.apply_async(foo,args=(e),callback=list_of_metrics.append)
#         # time.sleep (2)
#
#     pool.close()
#     pool.join()
#
#     for worker in pool._pool:
#         assert not worker.is_alive()
#     print('terminate() succeeded')
#     return list_of_metrics
