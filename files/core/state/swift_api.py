#!/usr/bin/env python3
import yaml
import os
import sys
import subprocess

import swiftclient

home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core/state','')
sys.path.append(home_dir)
from utils import read_yaml
dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))

def get_swift_password_kubernetes():
    """ Looking for swift password from enviroments.
    Args : None
    Return : swift password.
    """
    p = subprocess.Popen('set | grep -i  swift_password', shell=True, stdout=subprocess.PIPE)
    swift_password, err = p.communicate()
    swift_password = swift_password.decode('utf-8')
    swift_password = swift_password.split('\n')[1].split('=')[1]
    swift_password = swift_password.replace("\'", "")
    
    global dict_fields
    dict_fields['swift']['swift_password'] = swift_password

    # Check on which DC i'm running, in order to run locally
    p = subprocess.Popen ('set | grep -i  datacenter', shell=True , stdout=subprocess.PIPE)
    dc_name , err = p.communicate ()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    dict_fields['datacenter'] = dc_name.title()
    

    p = subprocess.Popen ('set | grep -i  swift_user', shell=True , stdout=subprocess.PIPE)
    swift_user , err = p.communicate ()
    swift_user = swift_user.decode('utf-8')
    swift_user = swift_user.split('\n')[1].split('=')[1]
    dict_fields['swift']['swift_user'] = swift_user

class SwiftApi():
    def __init__(self,dc=None):
        try:
            get_swift_password_kubernetes()
            self._swift_user = dict_fields['swift']['swift_user']
            self._swift_password = dict_fields['swift']['swift_password']
            
            if not dc:
                dc = dict_fields['datacenter']
                
            self._authurl = 'https://' + dict_fields['swift']['authurl'][dc] + '/auth/v1.0'
            
            self._authurl_1 =['https://' + dict_fields['swift']['authurl']['Virginia'] + '/auth/v1.0',
                              'https://' + dict_fields['swift']['authurl']['London'] + '/auth/v1.0',
                              'https://' + dict_fields['swift']['authurl']['Sydney'] + '/auth/v1.0']
            
            print('authurl = ' + self._authurl)
            self.conn = swiftclient.Connection(
                user=self._swift_user,
                key=self._swift_password,
                authurl=self._authurl)
        
        except Exception as e:
            print("An unkonwn error occurred while accessing the swift")
            self.error = "An unkonwn error occurred while accessing the swift"


    def get_list_files(self,container_name):
        print('Start function get_list_files')
        lst_names = []
        for data in self.conn.get_container(container_name)[1]:
            print('success get all files from {} container'.format(container_name))
            lst_names.append(data['name'])
        print('End function get_list_files return name list {}'.format(lst_names))
        return lst_names

    def get_file(self, container_name, name):
        try:
            for data in self.conn.get_container(container_name)[1]:
                if data['name'] == name:
                    data = self.conn.get_object(container_name, name)
                    data = yaml.safe_load(data[1])
                    if type(data) is list:
                        data = data[0]
                    return data
            return False
        except Exception as e:
            print('Error An unkonwn error occurred while accessing the swift')
            print(e)
            return {'Error': 'An unkonwn error occurred while accessing the swift'}

    def get_jobs_4_user(self, container_name, name): # todo!
        try:
            lst_data = list()
            for data in self.conn.get_container(container_name)[1]:
                if name in data['name']:  # get the files are include name
                    data = self.get_file(container_name,data['name'])
                    if not data.get('jobs',None):
                        continue
                    lst_data.append(data.get('jobs'))
                    # return yaml.safe_load(data[1])
        
            return lst_data
            # return False
        except Exception as e:
            print('Error An unkonwn error occurred while accessing the swift')
            print(e)
            return {'Error': 'An unkonwn error occurred while accessing the swift'}

    def delete_file(self, container_name, name):
        try:
            for data in self.conn.get_container(container_name)[1]:
                if data['name'] == name:
                    self.conn.delete_object(container_name, name)
                    for url in self._authurl_1:
                        if self._authurl != url:
                            conn = swiftclient.Connection(
                                user=self._swift_user,
                                key=self._swift_password,
                                authurl=url)
                            conn.delete_object(container_name, name)
                
                    return True
            return False
        except Exception as e:
            print('Error An unkonwn error occurred while accessing the swift')
            print(e)
            return {'Error': 'An unkonwn error occurred while accessing the swift'}
        
    def update(self,container_name,name,value):
        # if self.get_file(container_name,name):
        try:
            self.conn.put_object(container_name, name,
                             contents=yaml.dump(value),
                             content_type='text/yaml')
            
            for url in self._authurl_1:
                if self._authurl != url:
                    conn =swiftclient.Connection(
                        user=self._swift_user,
                        key=self._swift_password,
                        authurl=url )
                    conn.put_object(container_name, name,
                                 contents=yaml.dump(value),
                                 content_type='text/yaml')
        except Exception as e:
            print('Error An unkonwn error occurred while accessing the swift')
            print(e)
            return {'Error':'An unkonwn error occurred while accessing the swift'}
            
    def close(self):
        self.conn.close()
