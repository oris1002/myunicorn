import os
import sys


home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/state','')
sys.path.append(home_dir)

from state.save_sessions import Save_state
from state.swift_api import SwiftApi


def update_details():
    swift = SwiftApi()
    lst_files_name = swift.get_list_files('state')
    lst_files_name.reverse()
    for name in lst_files_name:
        if '_' in name:
            s = Save_state(name,'state')
            s.update_jobs_deatils()
            # s.remove_expired()
        # print(json.dumps(s.read_info(), indent=4, sort_keys=True))
    sche.enter(10, 1, update_details)

import sched, time
sche = sched.scheduler(time.time, time.sleep)

sche.enter(10, 1, update_details)
sche.run()
