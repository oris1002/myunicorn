#!/usr/bin/env python3
import yaml
import os
import sys
import subprocess
import os


home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/core/state','')
sys.path.append(home_dir)
from utils import read_yaml
dict_fields = read_yaml((home_dir + '/variables/variables.yaml'))
import os

def get_dc():
    # Check on which DC i'm running, in order to run locally
    print('Get DC')
    p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    dict_fields['datacenter'] = dc_name.title()
    print('Env DC =>' + str(dc_name.title()))

class File_State():
    def __init__(self,dc=None,root_dir="/var/unicorn_states"):
        try:
            
            # if not dc:
            #     self.dc = get_dc()
            # self.dc = dc
            print("File_State DC = {}".format(dc))
            self.dc = dc if dc else get_dc()
            self._authurl = "{}/{}".format(root_dir,self.dc)

            if not os.path.exists(self._authurl):
                os.makedirs(self._authurl + '/request')
                os.makedirs(self._authurl + '/state')
                os.makedirs(self._authurl + '/tools')

            print('authurl = ' + self._authurl)
        
        except Exception as e:
            print(e)
            return {'Error':e}

    import fnmatch
    def recursive_glob(self,type, rootdir='.', pattern='*'):
        return [os.path.join(looproot, filename).replace(rootdir+'/'+type+'/','')
                for looproot, _, filenames in os.walk(rootdir) if type in looproot
                for filename in filenames
                if fnmatch.fnmatch(filename, pattern)]
    
    def get_list_files(self,container_name):
        print('Start function get_list_files')
        lst_names = []
        path = "{}/{}".format(self._authurl,container_name)
        #todo return list path files
        return self.recursive_glob(container_name,self._authurl)
        # for data in self.recursive_glob(str(container_name),str(self._authurl)):
        #     print('success get all files from {} container'.format(container_name))
        #     # lst_names.append(data['name'])
        print('End function get_list_files return name list {}'.format(lst_names))
        return lst_names

    
    def get_file(self, container_name, name):
        try:
            tmp = {'user': name,'lastUpdate':None,'jobs':[]}
            print("lokking for {} {}".format(container_name,name))
            if os.path.exists("{}/{}/{}".format(self._authurl,container_name,name)):
                data = read_yaml("{}/{}/{}".format(self._authurl,container_name,name))
                print("####Data is: {}".format(data))
                return data if data else tmp
            else:
                return tmp
            
        except Exception as e:
            print(e)
            return {'Error':e}

    def get_jobs_4_user(self, container_name, name): # todo!
        try:
            lst_data = list()
            for data in read_yaml("{}/{}/{}".format(self._authurl,container_name,name)):
                if name in data['name']:  # get the files are include name
                    data = self.get_file(container_name,data['name'])
                    if not data.get('jobs',None):
                        continue
                    lst_data.append(data.get('jobs'))
                    # return yaml.safe_load(data[1])
        
            return lst_data
            # return False
        except Exception as e:
            print(e)
            return {'Error':e}

    def delete_file(self, container_name, name):
        try:
            for data in self.conn.get_container(container_name)[1]:
                if data['name'] == name:
                    self.conn.delete_object(container_name, name)
                    for url in self._authurl_1:
                        if self._authurl != url:
                            conn = swiftclient.Connection(
                                user=self._swift_user,
                                key=self._swift_password,
                                authurl=url)
                            conn.delete_object(container_name, name)
                
                    return True
            return False
        except Exception as e:
            print(e)
            return {'Error':e}
    
    def update(self,container_name,name,value):
        try:
            path = "{}/{}/{}".format(self._authurl, container_name, name)
            print(path)
            if container_name == 'tools':
                with open(path, 'w') as f:
                    f.write(yaml.dump(value))
            else:
                with open(path,'a') as f:
                    f.write(yaml.dump(value))
                
        except Exception as e:
            print(e)
            return {'Error':e}
            
    def close(self):
        pass

s = File_State('Amsterdam','/tmp/unicorn')
s.get_list_files('request')
# s.update('request','oris-uuid',{'test': 'oris','who':'queen'})
