import datetime
import os
import sys
import json
import subprocess
home_dir_project = os.path.dirname(os.path.realpath(__file__))
home_dir = os.path.dirname(os.path.realpath(__file__)).replace('/state','')
sys.path.append(home_dir_project)
sys.path.append(home_dir)
from dcim_api import DcimApi
fmt = "%Y-%m-%d %H:%M"
fmt_rundeck = "%Y-%m-%dT%H:%M:%SZ"
from rundeck_api import Rundeck_api
from swift_api import SwiftApi
from file_state import File_State
from reports.BigQuery.save_data import upload_data

def get_dc():
    # Check on which DC i'm running, in order to run locally
    print('Get DC')
    p = subprocess.Popen('set | grep -i  datacenter', shell=True, stdout=subprocess.PIPE)
    dc_name, err = p.communicate()
    dc_name = dc_name.decode('utf-8')
    dc_name = dc_name.split('\n')[1].split('=')[1]
    # dict_fields['datacenter'] = dc_name.title()
    return dc_name.title()
    print('Env DC =>' + str(dc_name.title()))
    
# todo: lock(id DB dont must)
class Save_state():
    def __init__(self,name,type,dc=None):
        self.name = name
        
        # self.data = {'user': self.name,
        #              'lastUpdate':None,
        #              'jobs':[]}
        if dc:
            self.dc = dc
        else:
            self.dc = get_dc()
            
        sfs = File_State(dc=self.dc)
        self.data = sfs.get_file(type,name)
        self.type = type
        # self.create_file()

        
    def create_file(self): # todo always create new
        # write python dict to a file
        if hasattr(self, 'dc'):
            swapi = SwiftApi(dc=self.dc)
        else:
            swapi = SwiftApi()
            
        if hasattr(swapi,'error'):
            print(swapi.error)
            self.error = swapi.error
            
        if not swapi.get_file(self.type,self.name):
            self.data['created'] = datetime.datetime.today().strftime(fmt)
            swapi.update(self.type,self.name,self.data)
        else:
            self.data = swapi.get_file(self.type,self.name)
        swapi.close()
            
        
    def read_info(self):
        # read python dict back from the file
        
        if hasattr(self, 'dc'):
            # swapi = SwiftApi(dc=self.dc)
            sfs = File_State(dc=self.dc)
        else:
            sfs = File_State()
            # swapi = SwiftApi()
        data = sfs.get_file(self.type,self.name)
        # data = swapi.get_file(self.type,self.name)
        # swapi.close()
        return data

    def delete_file(self):
        # read python dict back from the file
    
        if hasattr(self, 'dc'):
            swapi = SwiftApi(dc=self.dc)
        else:
            swapi = SwiftApi()
            
        swapi.delete_file(self.type, self.name)
        swapi.close()
        
        return data

    def add_job(self, new_data):
        # update dict and write to the file again
        print(self.data)
        self.data['jobs'].append(new_data)
        self.data['lastUpdate'] = datetime.datetime.today().strftime(fmt)
        
        if hasattr(self, 'dc'):
            # swapi = SwiftApi(dc=self.dc)
            sfs = File_State(dc=self.dc) #session file state
        else:
            sfs = File_State()
            # swapi = SwiftApi()
    
        sfs.update(self.type, self.name, self.data)
        # swapi.close()

    def flavor_update(self, new_data):
        # update dict and write to the file again
        print('flavor_update')
        print(self.data)
        print(new_data)
        self.data['jobs'] = new_data
        self.data['lastUpdate'] = datetime.datetime.today().strftime(fmt)
    
        if hasattr(self, 'dc'):
            # swapi = SwiftApi(dc=self.dc)
            sfs = File_State(dc=self.dc)
        else:
            # swapi = SwiftApi()
            sfs = File_State()
    
        # swapi.update(self.type, self.name, self.data)
        sfs.update(self.type, self.name, self.data)
        # swapi.close()

    def remove_expired(self):
        # remove expired jobs
        today = datetime.datetime.today()
        lst = self.data['jobs'].copy()
        for job in lst:
            if not job.get('error', None):
                if job.get('status', 'running') != 'running':
                    time_delta = datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                                            "%Y-%m-%dT%H:%M:%SZ") - today
                    if time_delta.days < -1:
                        self.data['jobs'].remove(job)
    
        if hasattr(self, 'dc'):
            swapi = SwiftApi(dc=self.dc)
        else:
            swapi = SwiftApi()
    
        swapi.update(self.type, self.name, self.data)
        swapi.close()
        
    def push_job_to_BigQuery(self,job,mode):
        data = list()
        col = None
        table_name = None
        if mode == 'install':
            col = [
                ("job_id", "INTEGER", "REQUIRED"),
                ("user", "STRING", "REQUIRED"),
                ("start_date", "DATETIME", "REQUIRED"),
                ("Status", "STRING", "REQUIRED"),
                ("DataCenter", "STRING", "REQUIRED"),
                ("SerialNo", "STRING", "REQUIRED"),
                ("Owner", "STRING", "REQUIRED"),
                ("Flavor_Name", "STRING", "REQUIRED"),
                ("Running_Time", "INTEGER", "REQUIRED"),
                ("Price", "INTEGER","REQUIRED")
                

            ]
            table_name = "All_Installation_2"
            data.append((job.get('id', 9999),
             job.get('user', ''),
             datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                        "%Y-%m-%dT%H:%M:%SZ"),
             job.get('installation_status', 'Unknown'),
             job.get('job', {}).get('options', {}).get('DataCenter', ''),
             job.get('job', {}).get('options', {}).get('SerialNo', ''),
             job.get('job', {}).get('options', {}).get('Owner', ''),
             job.get('job', {}).get('options', {}).get('Flavor_name', ''),
             int((datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                                         "%Y-%m-%dT%H:%M:%SZ") - datetime.datetime.strptime(
                             job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ")).seconds / 60),
             int(job.get('job', {}).get('options', {}).get('Price', '0'))
             ))



        elif mode == 'deco':
            table_name = "All_Decommission"
            col = [
                ("job_id", "INTEGER", "REQUIRED"),
                ("user", "STRING", "REQUIRED"),
                ("start_date", "DATETIME", "REQUIRED"),
                ("status", "STRING", "REQUIRED"),
                ("DataCenter", "STRING", "REQUIRED"),
                ("SerialNo", "STRING", "REQUIRED"),
                ("Owner", "STRING", "REQUIRED"),
                ("Hostname", "STRING", "REQUIRED"),
                ("Running_Time", "INTEGER", "REQUIRED"),

            ]
            data.append((job.get('id', 9999),
                         job.get('user', ''),
                         datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                                    "%Y-%m-%dT%H:%M:%SZ"),
                         job.get('deco_status', 'Unknown'),
                         job.get('job', {}).get('options', {}).get('DataCenter', ''),
                         job.get('job', {}).get('options', {}).get('SerialNo', ''),
                         job.get('job', {}).get('options', {}).get('Owner', ''),
                         job.get('job', {}).get('options', {}).get('HostName', ''),
                         int((datetime.datetime.strptime(job.get("date-ended", {}).get('date'),
                                                         "%Y-%m-%dT%H:%M:%SZ") - datetime.datetime.strptime(
                             job.get("date-started", {}).get('date'), "%Y-%m-%dT%H:%M:%SZ")).seconds / 60),

                         ))
        upload_data(data,'cloudops-241008','Unicorn',table_name, col,path='/lp_unicorn/unicorn/files/variables/gcp_creds.json')

        

    def update_jobs_deatils(self):
        # update the jobs status...
        if not hasattr(self,'data'):
            return
        if type(self.data) is not dict:
            return
        if not self.data.get('jobs',None):
            return
        
        lst = self.data['jobs'].copy()

        for i,job in enumerate(lst):
            if not job.get('error',None):
                old_status = job.get('status','running')
                rundeck = Rundeck_api()
                update_job = rundeck.check_job_status(job['id'])
                self.data['jobs'][i] = update_job
                print(update_job.get('id'),update_job.get('status','pp'),old_status)
                # when job status changing from running to somthing else
                if update_job.get('status','running') not in ['running',old_status]:
                    if 'install' in update_job['job']['name'].lower():
                        update_job['installation_status'] = update_job.pop('status')
                        self.push_job_to_BigQuery(update_job,'install')
                    elif 'deco' in update_job['job']['name'].lower():
                        update_job['deco_status'] = update_job.pop('status')
                        self.push_job_to_BigQuery(update_job,'deco')
                        
                    
                    
                    update_job['username'] = update_job.pop('user')
                    if update_job['job']['options'].get('Price',None):
                        update_job['job']['options']['Price'] =int((update_job['job']['options']).pop('Price')) # convert to int for print
                    else:
                        update_job['job']['options']['Price'] = 6500

                    if 'install' in update_job['job']['name'].lower():
                        update_job['job']['options']['Price_int'] = int(update_job['job']['options'].pop('Price'))

                    print(json.dumps(update_job))
                    
                    if 'install' in update_job['job']['name'].lower():
                        update_job['job']['options']['Price'] = update_job['job']['options'].pop('Price_int')
                        
                    if 'install' in update_job['job']['name'].lower():
                        update_job['status'] = update_job.pop('installation_status')
                    elif 'deco' in update_job['job']['name'].lower():
                        update_job['status'] = update_job.pop('deco_status')

                    update_job['user'] = update_job.pop('username')
                    # serial = None
                    # if update_job.get('status','running') in ['aborted','failed']:
                    #     dcim = DcimApi()
                    #     serial = update_job.get('job',{}).get('options',{}).get('SerialNo','')
                    # if serial:
                    #     dcim.update_owner(update_job.get('job',{}).get('options',{}).get('SerialNo',''),'Stock')

            
        if hasattr(self, 'dc'):
            swapi = SwiftApi(dc=self.dc)
        else:
            swapi = SwiftApi()
            
        if self.data['jobs']:
            swapi.update(self.type, self.name, self.data)
        else:
            swapi.delete_file(self.type, self.name)
            return
        
        swapi.update(self.type, self.name, self.data)
        swapi.close()
       
#
def save_state(user,job,type,dc=None):
    s = Save_state(user, type,dc=dc)
    if hasattr(s, 'error'):
        print(s.error)
        return {'Error': s.error}
    s.add_job(job)
    # s.remove_expired()
    # print(json.dumps(s.read_info(), indent=4, sort_keys=True))

def save_flavors(data,dc=None):
    print('start save_flavors fundction ')
    s = Save_state('flavors', 'tools',dc=dc)
    print('create instance Save_state')
    if hasattr(s, 'error'):
        print(s.error)
        return {'Error': s.error}
    print('passed')
    s.flavor_update(data)
    print('create new')
    # s.remove_expired()

