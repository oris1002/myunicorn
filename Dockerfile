FROM centos:7
#FROM lpcr.int.liveperson.net/lp-building-block_release/lp-centos7-base
# Add author name
MAINTAINER Ori Shinsholker
RUN yum -y update
#RUN yum -y install https://centos7.iuscommunity.org/ius-release.rpm
RUN yum -y downgrade glibc glibc-common glibc-devel glibc-headers libstdc++ openldap telnet
RUN yum -y install epel-release ius-release
RUN yum -y install git
RUN yum -y install python36 python36-pip python36-devel openldap-devel gcc gcc-c++ kernel-devel && yum clean all
ADD / /lp_unicorn/unicorn/
WORKDIR /lp_unicorn/unicorn
RUN pip3.6 install --upgrade pip
RUN pip3.6 install -r requirements.txt
RUN bash -c '/bin/echo change file permissions'	+
RUN ["chmod", "+x", "/lp_unicorn/unicorn/lp_unicorn.sh"]
COPY docker/confd/ /etc/confd/
RUN ["chmod", "+x", "/usr/bin/preflight.sh"]
RUN ["chmod", "+x", "/lp_unicorn/unicorn/files/core/lp_unicorn_send_report.sh"]
RUN ["chmod", "+x","/lp_unicorn/unicorn/files/core/reports/BigQuery/save_data.py"]
